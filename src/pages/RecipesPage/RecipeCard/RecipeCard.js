import React from 'react';
import {StyledRecipeCard} from "./styled";
import {StyledCard} from "../../../components/atoms/StyledCard";

export const RecipeCard = props => {
    const {recipe} = props;

    return (
        <StyledRecipeCard>
            <StyledCard classNam='recipe-card'>
                <div className="img-wrap">
                    <img src={recipe.image.cropped && recipe.image.cropped.url} alt='/'/>
                </div>
                <p className="text">{recipe.header}</p>
            </StyledCard>
        </StyledRecipeCard>
    )
};