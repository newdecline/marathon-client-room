import styled from "styled-components/macro";

export const StyledRecipeCard = styled('div')`
    .recipe-card {
        font-size: 20px;
        line-height: 120%;
        font-family: 'Circe-Regular';
        font-weight: 700;
        height: 100%;
    }
    .text {
        padding: 22px 29px 27px 29px;
        margin: 0;
        font-size: 20px;
        line-height: 120%;
        font-weight: 700;
        font-family: 'Circe-Regular';
    }
    .img-wrap {
        border-top-left-radius: inherit;
        border-top-right-radius: inherit;
    }
    img {
        border-top-left-radius: inherit;
        border-top-right-radius: inherit;
        display: block;
        max-width: 100%;
    }
`;