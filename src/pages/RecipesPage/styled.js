import styled from "styled-components/macro";

export const StyledRecipesPage = styled('div')`
    margin: auto 0;
    .container {
        position: relative;
        padding: 0 21px;
    }
    .recipes-list {
        padding: 0;
        margin: 42px 0 44px 0;
        display: grid;
        grid-template-columns: auto;
        grid-template-rows: auto;
        grid-gap: 15px;
        list-style-type: none;
        &__item {
           a {
              height: 100%;
           }
        }
        a {
            text-decoration: none;
            color: inherit;
        }
    }
    @media (min-width: 1024px) {
        .container {
            max-width: 940px;
            margin: 0 auto;
            padding: 0;
        }
        .recipes-list {
            grid-template-columns: repeat(2, 1fr);
        }
        .measurements-popup {
            width: 589px;
            .body-measurements {
                flex: 1;
            }
            .body-measurements__label {
                margin-right: auto;
            }
            .body-upload-file {
                width: 164px;
            }
            .upload-file {
                height: 100%;
                width: 164px;
            }
            .drop-zone {
                height: 100%;
                flex-direction: column;
                justify-content: flex-end;
                padding: 0 13px 12px 13px;
                box-sizing: border-box;
                text-align: center;
                svg {
                    margin-right: 0;
                    margin-bottom: 36px;
                }
            }
            .drop-zone-wrap {
                height: 100%;
            }
            .drop-zone {
                flex-direction: column;
                height: 100%;
                width: 164px;
            }
            .field-wrap {
                width: 164px;
                flex-basis: unset;
            }
        }
    }
    @media (min-width: 1260px) {
        .container {
            position: relative;
            max-width: 1200px;
            margin: 0 auto;
        }
        .recipes-list {
            padding: 0;
            margin: 42px 0 44px 0;
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            grid-template-rows: auto;
            grid-gap: 15px;
            list-style-type: none;
            &__item {
               
            }
            a {
                text-decoration: none;
                color: inherit;
            }
        }
    }
`;