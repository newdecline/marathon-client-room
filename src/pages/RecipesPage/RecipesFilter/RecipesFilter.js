import React from 'react';
import autobind from "autobind";
import {array, func} from "prop-types";
import {observer} from "mobx-react";
import {StyledRecipesFilter} from "./styled";
import {StyledCard} from "../../../components/atoms/StyledCard";

@autobind
@observer
class RecipesFilter extends React.Component {
    render() {
        const {options, onReset} = this.props;

        return (
            <StyledRecipesFilter>
                <StyledCard className='recipes-filter'>
                    <ul className="list-filter">{options.map(this.renderTag)}</ul>

                    <button className="reset-filter" onClick={onReset}>Сбросить все фильтры</button>
                </StyledCard>
            </StyledRecipesFilter>
        );
    }

    renderTag(tag, index) {
        const {selected, onSelect} = this.props;

        const isSelected = selected.includes(tag);

        return (
            <li
                key={index}
                onClick={() => onSelect(tag)}
                className={isSelected ? "list-filter__item selected" : "list-filter__item"}>
                {tag}
            </li>
        );
    }
}

RecipesFilter.propTypes = {
    options: array,
    selected: array,
    onReset: func.isRequired,
    onSelect: func.isRequired,
};

RecipesFilter.defaultProps = {
    options: [],
    selected: [],
};

export {RecipesFilter};