import styled from "styled-components/macro";

export const StyledRecipesFilter = styled('div')`
    .recipes-filter {
        color: #454545;
        font-weight: 700;
        padding: 21px 0;
    }
    .reset-filter {
        display: block;
        padding: 0;
        margin: 0 20px 0 auto;
        border: none;
        border-bottom: 1px solid #454545;
        background-color: transparent;
        font-size: 18px;
        line-height: 120%;
        font-family: 'Circe-Regular';
        font-weight: 700;
        &:hover {
            cursor: pointer;
        }
        &:active, &:focus {
            outline: none;
        }
    }
    .list-filter {
        display: flex;
        justify-content: flex-start;
        flex-wrap: wrap;
        padding: 0;
        margin: 0 0 20px 0;
        list-style-type: none;
        &__item {
            padding: 3px 8px 1px 8px;
            margin: 10px 10px 7px 10px;
            font-size: 18px;
            line-height: 120%;
            background: transparent;
            border-radius: 5px;
            transition: background-color .3s;
            font-family: 'Circe-Regular';
            font-weight: 700;
            &:last-child {
                margin-right: auto;
            }
            &.selected {
                background: #E5E5E5;
            }
            &:hover {
                cursor: pointer;
                background: #E5E5E5;
            }
        }
    }
    @media (min-width: 1024px) {
        
    }
    @media (min-width: 1260px) {
        .reset-filter {
            display: block;
            padding: 0;
            margin: 0 76px 0 auto;
            border: none;
            border-bottom: 1px solid #454545;
            background-color: transparent;
            font-size: 18px;
            line-height: 120%;
            font-family: 'Circe-Regular';
            font-weight: 700;
            &:hover {
                cursor: pointer;
            }
            &:active, &:focus {
                outline: none;
            }
        }
        .list-filter {
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
            padding: 0 58px 12px 53px;
            margin: 0 ;
            list-style-type: none;
            &__item {
                padding: 3px 8px 1px 8px;
                margin: 10px 10px 7px 10px;
                font-size: 18px;
                line-height: 120%;
                background: transparent;
                border-radius: 5px;
                transition: background-color .3s;
                font-family: 'Circe-Regular';
                font-weight: 700;
                &:last-child {
                    margin-right: auto;
                }
                &.selected {
                    background: #E5E5E5;
                }
                &:hover {
                    cursor: pointer;
                    background: #E5E5E5;
                }
            }
        }
    }
`;