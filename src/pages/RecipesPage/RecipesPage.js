import React from 'react';
import {Link} from "react-router-dom";
import {observer} from "mobx-react";
import {RecipesFilter} from "./RecipesFilter/RecipesFilter";
import {RecipeCard} from "./RecipeCard/RecipeCard";
import {StyledRecipesPage} from "./styled";
import autobind from "autobind";
import {observable} from "mobx";
import apiService from "../../apiService";
import {union, intersection} from "lodash";
import {Preloader} from "../../components/Preloader/Preloader";
import commonStore from "../../mobx/commonStore";

@autobind
@observer
class RecipesPage extends React.Component {

    @observable recipes = [];
    @observable selectedTags = [];

    async componentDidMount() {
        const response = await apiService.listRecipes();
        if (response.status === 200) {
            this.recipes = response.body;
        }
    }

    handleTagSelect(tag) {
        if (!this.selectedTags.includes(tag)) {
            this.selectedTags.push(tag);
        } else {
            this.selectedTags = this.selectedTags.filter(selected => selected !== tag);
        }
    }

    resetSelectedTags() {
        this.selectedTags = [];
    }

    render() {
        const tags = union(...this.recipes.map(recipe => JSON.parse(recipe.tags)));
        const filteredRecipes = this.selectedTags.length > 0
            ? this.recipes.filter(recipe => intersection(JSON.parse(recipe.tags), this.selectedTags).length > 0)
            : this.recipes;

        return (
            <StyledRecipesPage>
                {commonStore.loading
                    ? <Preloader/>
                    : <div className="container">
                        <RecipesFilter
                            options={tags}
                            selected={this.selectedTags}
                            onSelect={this.handleTagSelect}
                            onReset={this.resetSelectedTags}
                        />
                        <ul className="recipes-list">
                            {
                                filteredRecipes.map(recipe => {
                                    return <li key={recipe.id} className="recipes-list__item">
                                        <Link to={`/recipes/${recipe.id}`}>
                                            <RecipeCard recipe={recipe}/>
                                        </Link>
                                    </li>
                                })
                            }
                        </ul>
                    </div>}
            </StyledRecipesPage>
        );
    }
}

export {RecipesPage};

