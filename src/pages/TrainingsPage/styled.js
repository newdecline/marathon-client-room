import styled from "styled-components/macro";

export const StyledTrainingPage = styled('div')`
    margin: auto 0;
    .block {
        color: #454545;
        font-size: 20px;
        &__header {
            font-family: 'Bodoni';
            font-weight: 400;
            font-size: 30px;
            color: #454545;
            line-height: 28px;
        }
        p { 
            margin: 0;
        }
        a {
            color: inherit;
            font-weight: bolder;
            font-family: 'Circe-Regular';
        }
        strong {
            font-family: 'Circe-Regular';
        }
        
        &.no-header {
            margin-top: 30px;
        }
    }
    .columns-block {
        &__column {
            &-header {
                font-family: 'Circe-Regular';
                font-size: 20px;
                text-transform: uppercase;
            }
        }
    }

    .container {
        position: relative;
        box-sizing: border-box;
        padding: 0 21px 80px;
    }
    @media (min-width: 1024px) {
        .block {
            &__header {
                font-size: 46px;
            }
            &:first-of-type {
                .block__header {
                    margin-top: 8px;
                }
            }
        }
        .columns-block {
            &__grid {
                display: flex;
            }
            &__column {
                flex: 1;
            }
        }
        .container {
            max-width: 940px;
            margin: 0 auto;
            padding: 0 0 80px;
        }
        .measurements-popup {
            width: 589px;
            .body-measurements {
                flex: 1;
            }
            .body-measurements__label {
                margin-right: auto;
            }
            .body-upload-file {
                width: 164px;
            }
            .upload-file {
                height: 100%;
                width: 164px;
            }
            .drop-zone {
                height: 100%;
                flex-direction: column;
                justify-content: flex-end;
                padding: 0 13px 12px 13px;
                box-sizing: border-box;
                text-align: center;
                svg {
                    margin-right: 0;
                    margin-bottom: 36px;
                }
            }
            .drop-zone-wrap {
                height: 100%;
            }
            .drop-zone {
                flex-direction: column;
                height: 100%;
                width: 164px;
            }
            .field-wrap {
                width: 164px;
                flex-basis: unset;
            }
        }
    }
    @media (min-width: 1260px) {
        .container {
            position: relative;
            max-width: 1200px;
            margin: 0 auto;
        }
    }
`;