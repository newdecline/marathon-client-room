import React from 'react';
import {ReactComponent as VideoPlayIcon} from '../../../svg/play.svg';
import {VideoPlayer} from "./VideoPlayer/VideoPlayer";
import {StyledTrainingVideos} from "./styled";
import {StyledCard} from "../../../components/atoms/StyledCard";

export const TrainingVideos = ({slides}) => {
    return (
        <StyledTrainingVideos>
            {
                slides.map(slide => {
                    return (
                        <StyledCard key={slide.id} className='training-videos-card'>
                            <VideoPlayer
                                iconPlay={<VideoPlayIcon/>}
                                videoURL={slide.url}/>
                            <div className='training-videos-card__label'>{slide.text}</div>
                        </StyledCard>
                    )
                })
            }
        </StyledTrainingVideos>
    )
};

