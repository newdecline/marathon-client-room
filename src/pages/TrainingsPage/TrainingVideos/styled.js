import styled from "styled-components/macro";

export const StyledTrainingVideos = styled('div')`
    display: grid;
    grid-template-columns: auto;
    grid-template-rows: auto;
    grid-gap: 14px;
    margin: 17px 0;
    .video {
        position: relative;
    }
    .video--enabled {
        position: relative;
        padding-bottom: 56.25%;
        iframe {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
    }
    .video__link {
        display: block;
        &::after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.3);   
        }
    }
    .video__button {
        position: absolute;
        top: 50%;
        left: 50%;
        border: none;
        padding: 0;
        background-color: transparent;
        transform: translate(-50%, -50%);
        z-index: 1;
        &:hover {
            cursor: pointer;
        }
    }
    .training-videos-card {
        box-shadow: none;
        overflow: hidden;
        &__label {
            margin: 19px 23px 19px 23px;
            font-size: 20px;
            line-height: 120%;
            white-space: pre-wrap;
        }
    }
    @media (min-width: 1024px) {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        grid-template-rows: auto;
        grid-gap: 14px;
        margin: 50px 0;
        .training-videos-card {
            &__label {
                margin: 29px 23px 19px 23px;
            }
        }
    }
`;