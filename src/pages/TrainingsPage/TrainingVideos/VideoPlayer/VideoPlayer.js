import React, {useEffect, useRef} from 'react';
import {string, object} from 'prop-types';

const regExp = /^.*((m\.)?youtu\.be\/|vi?\/|u\/\w\/|embed\/|\?vi?=|\&vi?=)([^#\&\?]*).*/;

export const VideoPlayer = props => {
    const {videoURL, previewImageAlt, iconPlay} = props;

    const previewImageURL = videoURL.match(regExp)[3];

    const videoRef = useRef(null);
    const videoLinkRef = useRef(null);
    const videoMediaRef = useRef(null);
    const videoButtonRef = useRef(null);

    const setupVideo = (video) => {
        let id = parseMediaURL(videoMediaRef.current);

        video.addEventListener('click', () => {
            let iframe = createIframe(id);

            videoLinkRef.current.remove();
            videoButtonRef.current.remove();
            video.appendChild(iframe);
            videoLinkRef.current.removeAttribute('href');
            video.classList.add('video--enabled');
        });
    };

    const parseMediaURL = (media) => {
        let regexp = /https:\/\/i\.ytimg\.com\/vi\/([a-zA-Z0-9_-]+)\/mqdefault\.jpg/i;
        let url = media.src;
        let match = url.match(regexp);

        return match[1];
    };

    const createIframe = (id) => {
        let iframe = document.createElement('iframe');

        iframe.setAttribute('allowfullscreen', '');
        iframe.setAttribute('allow', 'autoplay');
        iframe.setAttribute('src', generateURL(id));
        iframe.setAttribute('frameborder', 0);
        iframe.classList.add('video__media');

        return iframe;
    };

    const generateURL = (id) => {
        let query = '?rel=0&showinfo=0&autoplay=1';

        return 'https://www.youtube.com/embed/' + id + query;
    };

    useEffect(() => {
        setupVideo(videoRef.current);
    });

    return (
        <div className="wrapper-video">
            <div className="video" ref={videoRef}>
                <a
                    ref={videoLinkRef}
                    className="video__link"
                    href={videoURL}>
                    <img
                        ref={videoMediaRef}
                        className="video__media"
                        src={`https://i.ytimg.com/vi/${previewImageURL}/mqdefault.jpg`}
                        alt={previewImageAlt}/>
                </a>

                <button ref={videoButtonRef}
                        className="video__button"
                        type="button"
                        aria-label="Запустить видео">{iconPlay}</button>
            </div>

        </div>
    )
};

VideoPlayer.propTypes = {
    videoURL: string,
    previewImageAlt: string,
    iconPlay: object
};