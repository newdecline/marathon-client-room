import React from 'react';
import {TrainingVideos} from "./TrainingVideos/TrainingVideos";
import {observer} from "mobx-react";
import {StyledTrainingPage} from "./styled";
import {observable} from "mobx";
import autobind from "autobind";
import apiService from "../../apiService";
import range from "lodash/range";
import {Preloader} from "../../components/Preloader/Preloader";
import commonStore from "../../mobx/commonStore";

@autobind
@observer
class TrainingsPage extends React.Component {

    @observable blocks = [];

    async componentDidMount() {
        const response = await apiService.getPage('trainings');
        if (response.status === 200) {
            this.blocks = response.body.textPageBlocks;
        }
    }

    render() {
        return (
            <StyledTrainingPage>
                {commonStore.loading
                    ? <Preloader/>
                    : <div className="container">
                        {this.blocks.map(this.renderBlock)}
                    </div>}
            </StyledTrainingPage>
        );
    }

    renderBlock(block) {
        const TEXT_PAGE_BLOCK_TYPE_TEXT = 'text';
        const TEXT_PAGE_BLOCK_TYPE_COLUMNS = 'columns';
        const TEXT_PAGE_BLOCK_TYPE_VIDEOS = 'videos';

        switch (block.type) {
            case TEXT_PAGE_BLOCK_TYPE_TEXT:
                return this.renderTextBlock(block);
            case TEXT_PAGE_BLOCK_TYPE_COLUMNS:
                return this.renderColumnsBlock(block);
            case TEXT_PAGE_BLOCK_TYPE_VIDEOS:
                return this.renderVideosBlock(block);
            default:
                return null;
        }
    }

    renderTextBlock(block) {
        const className = `block text-block${block.header ? '' : ' no-header'}`;

        return (
            <div className={className} id={block.id} key={block.id}>
                {block.header && <h2 className="block__header">{block.header}</h2>}
                <div dangerouslySetInnerHTML={{__html: block.text}}/>
            </div>
        );
    }

    renderColumnsBlock(block) {
        const columns = range(1, 4).map(index => ({
            header: block[`column_${index}_header`],
            text: block[`column_${index}_text`],
        }));
        const className = `block columns-block${block.header ? '' : ' no-header'}`;

        return (
            <div className={className} id={block.id} key={block.id}>
                {block.header && <h2 className="block__header">{block.header}</h2>}
                <div className="columns-block__grid">
                    {columns.map((column, index) => (
                        <div className="columns-block__column" key={index}>
                            <h3 className="columns-block__column-header">{column.header}</h3>
                            <div
                                className="columns-block__column-text"
                                dangerouslySetInnerHTML={{__html: column.text}}
                            />
                        </div>
                    ))}
                </div>
            </div>
        );
    }

    renderVideosBlock(block) {
        const className = `block videos-block${block.header ? '' : ' no-header'}`;

        return (
            <div className={className} id={block.id} key={block.id}>
                {block.header && <h2 className="block__header">{block.header}</h2>}
                <TrainingVideos slides={block.gallery.gallerySlides}/>
            </div>
        );
    }
}

export {TrainingsPage};

