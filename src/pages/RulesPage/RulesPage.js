import React from "react";
import {TextPage} from "../TextPage/TextPage";

const RulesPage = () => {
    return (
        <TextPage pageKey='rules' />
    );
};

export {RulesPage};