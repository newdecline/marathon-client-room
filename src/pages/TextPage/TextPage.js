import React from 'react';
import scrollIntoView from 'scroll-into-view-if-needed'
import {FoodGallery} from "../FoodPage/FoodGallery/FoodGallery";
import {observer} from "mobx-react";
import {StyledTextPage} from "./styled";
import {observable} from "mobx";
import autobind from "autobind";
import {Preloader} from "../../components/Preloader/Preloader";
import {StyledCard} from "../../components/atoms/StyledCard";
import commonStore from "../../mobx/commonStore";
import PropTypes from 'prop-types';
import apiService from "../../apiService";

@autobind
@observer
class TextPage extends React.Component {

    @observable blocks = [];

    async componentDidMount() {
        const {pageKey} = this.props;

        const response = await apiService.getPage(pageKey);
        if (response.status === 200) {
            this.blocks = response.body.textPageBlocks;
        }
    }

    handleClickNavigationItem(blockId) {
        const targetScroll = document.getElementById(blockId);

        scrollIntoView(targetScroll, {
            behavior: 'smooth',
            scrollMode: 'if-needed',
            block: 'start'
        });
    }

    render() {
        const blocksWithHeader = this.blocks.filter(block => block.header);

        return (
            <StyledTextPage>
                {
                    commonStore.loading
                        ? <Preloader/>
                        : <div className="container">
                            {blocksWithHeader.length > 1 && this.renderNavigation(blocksWithHeader)}
                            {this.blocks.map(this.renderBlock)}
                        </div>
                }
            </StyledTextPage>
        );
    }

    renderNavigation(blocks) {
        return (
            <StyledCard as={'nav'} className="navigation">
                <div className="navigation-title">Быстрая навигация</div>
                <ul className="navigation-list">
                    {blocks.map(this.renderNavigationItem)}
                </ul>
            </StyledCard>
        );
    }

    renderNavigationItem(block) {
        return (
            <li
                key={block.id}
                className="navigation-list__item"
                onClick={() => this.handleClickNavigationItem(block.id)}
            >
                {block.header}
            </li>
        )
    }

    renderBlock(block) {
        const TEXT_PAGE_BLOCK_TYPE_TEXT = 'text';
        const TEXT_PAGE_BLOCK_TYPE_GALLERY = 'gallery';

        switch (block.type) {
            case TEXT_PAGE_BLOCK_TYPE_TEXT:
                return this.renderTextBlock(block);
            case TEXT_PAGE_BLOCK_TYPE_GALLERY:
                return this.renderGalleryBlock(block);
            default:
                return null;
        }
    }

    renderTextBlock(block) {
        return (
            <StyledCard className='card-item card-text' id={block.id} key={block.id}>
                {block.header && <h2>{block.header}</h2>}
                <div dangerouslySetInnerHTML={{__html: block.text}}/>
            </StyledCard>
        );
    }

    renderGalleryBlock(block) {
        return (
            <StyledCard className='card-item card-gallery' id={block.id} key={block.id}>
                {block.header && <h2>{block.header}</h2>}
                <FoodGallery slides={block.gallery.gallerySlides}/>
            </StyledCard>
        );
    }
}

TextPage.propTypes = {
    pageKey: PropTypes.string.isRequired,
};

export {TextPage};