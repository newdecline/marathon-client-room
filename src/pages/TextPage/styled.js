import styled from "styled-components/macro";

export const StyledTextPage = styled('div')`
    color: #454545;
    margin: auto 0;
    min-height: 100%;
    .container {
        position: relative;
        padding: 0 21px;
    }
    .navigation {
        padding: 29px 22px 24px 22px;
        &-title {
            margin-bottom: 15px;
            font-size: 18px;
            line-height: 120%;
            font-family: 'Circe-Regular';
            font-weight: 700;
        }
        &-list {
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            align-items: flex-start;
            padding: 0;
            margin: 0;
            list-style-type: none;
            &__item {
                margin-bottom: 8px;
                font-size: 20px;
                line-height: 120%;
                &:hover {
                    cursor: pointer;
                }
            }
        }
    }
    .card-item {
        margin: 22px 0;
        padding: 36px 22px 41px 22px;
        box-sizing: border-box;
        font-size: 20px;
        img {
            width: auto;
        }
        h2 {
            font-weight: 400;
            font-size: 30px;
            line-height: 100%;
            font-family: 'Bodoni';
        }
        &.card-gallery {
            padding: 0;
            margin: 0 -21px 0 0;
            border-radius: 0;
            box-shadow: none;
            background: transparent;
        }
        &.card-text {
            h2 {
                margin-top: 0;
                margin-bottom: 24px;
            }
            p {
                margin: 0;
            }
        }
    }
    @media (min-width: 1024px) {
        .navigation {
            padding: 29px 71px 24px 71px;
        }
        .container {
            padding: 0;
            max-width: 940px;
            margin: 0 auto;
        }
        .card-item {
            padding: 36px 39px 41px 71px;
            h2 {
                font-size: 46px;
            }
            &.card-gallery {
                margin: 0;
            }
        }
    }
    @media (min-width: 1260px) {
        .container {
            max-width: 1200px;
            margin: 0 auto;
        }
    }
`;