import React from "react";
import {observer} from 'mobx-react';
import arrowWithLongLineIcon from "../../../svg/arrow-with-long-line.svg";
import commonStore from "../../../mobx/commonStore";
import {StyledStatusCard} from "./styled";
import {StyledCard} from "../../../components/atoms/StyledCard";

export const StatusCard = observer(() => {

    const formatValue = value => {

        if (value >= 0) {
            if (value < 10) {
                return '0' + value.toFixed(1);
            } else {
                return '' + value.toFixed(1);
            }
        } else {
            if (value > -10) {
                return '-0' + (-value).toFixed(1);
            } else {
                return '' + value.toFixed(1);
            }
        }
    };

    const getCardValue = (cardKey, valueKey) => formatValue(commonStore.dashboard.summary[cardKey][valueKey]);

    const cards = [
        {key: 'weight', label: 'Вес', des: 'кг'},
        {key: 'chest', label: 'Грудь', des: 'см'},
        {key: 'waist', label: 'Талия', des: 'см'},
        {key: 'hip', label: 'Бедра', des: 'см'},
    ];
    const cardsData = cards.map(card => ({
        title: card.label,
        headerValue1: getCardValue(card.key, 'totalDiff'),
        headerValue2: getCardValue(card.key, 'lastDiff'),
        firstMeasurement: getCardValue(card.key, 'before'),
        currentMeasurement: getCardValue(card.key, 'after'),
        designationOfMeasure: card.des,
    }));

    const cardsColored = commonStore.dashboard.results.length > 1;

    return (
        <StyledStatusCard>
            {
                cardsData.map((item, index) => {
                    return (
                        <StyledCard
                            key={index}
                            className="item"
                            style={cardsColored ? {
                                backgroundColor: +item.headerValue2 >= 0 ? '#D96868' : '#9AC190',
                            } : {}}>
                            <div className="header">
                                <div className="header__title">{item.title}</div>
                                <div className="header__values">
                                    <div className="values-item">
                                        <span className="values-item-hint">всего</span>
                                        <span
                                            className='values-item-value all'>{item.headerValue1} {item.designationOfMeasure}</span>
                                    </div>
                                    <div className="values-item">
                                        <span className="values-item-hint">за неделю</span>
                                        <span
                                            className='values-item-value'>{item.headerValue2} {item.designationOfMeasure}</span>
                                    </div>
                                </div>
                            </div>
                            <div className="footer">
                                    <span
                                        className='footer__first-measurement'>{item.firstMeasurement} {item.designationOfMeasure}</span>
                                <span
                                    className='footer__current-measurement'>{item.currentMeasurement} {item.designationOfMeasure}</span>
                                <img src={arrowWithLongLineIcon} alt="стрелка"
                                     className='arrow-with-long-line'/>
                            </div>
                        </StyledCard>
                    )
                })
            }
        </StyledStatusCard>
    )
});