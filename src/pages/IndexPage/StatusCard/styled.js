import styled from 'styled-components/macro';

export const StyledStatusCard = styled('div')`
    margin-bottom: -8px;
    grid-area: status-cards;
    .item {
        padding: 13px 18px 9px 15px;
        margin-bottom: 11px;
        background: #C3C3C3;
        color: #fff;
        font-size: 18px;
        box-sizing: border-box;
    }
    .header, .footer {
        display: flex; 
        justify-content: space-between;
    }
    .header {
        margin-bottom: 23px;
        &__title {
            text-transform: uppercase;
        }
        &__values {
            display: flex;
            flex-direction: column;
        }
    }
    .footer {
        position: relative;
        &__first-measurement {
            align-self: flex-end;
            font-family: 'Circe-ExtraBold';
            line-height: 120%;
        }
        &__current-measurement {
            font-family: 'Bodoni';
            font-size: 36px;
            line-height: 100%;
        }
        .arrow-with-long-line {
            position: absolute;
            left: 113px;
            top: 20px;
            display: block;
            width: 22px;
            height: 8px;
        }
    }
    .values-item {
        display: flex;
        &-hint {
            margin-right: 21px;
            width: 64px;
            font-size: 14px;
            line-height: 120%;
            text-align: right;
        }
        &-value {
            font-size: 18px;
            line-height: 120%;
            &.all {
                font-family: "Circe-ExtraBold";
            }
        }
    }
    @media (min-width: 1024px) {
        position: relative;
        margin-bottom: -11px;
        .header {
            margin-bottom: 21px;
        }
        .item {
            padding: 13px 16px 8px 14px;
            margin-bottom: 11px;
        }
        .footer .arrow-with-long-line {
            top: 20px;
            left: 93px;
        }
        .edit-measurements-popup {
            width: 591px;
            height: auto;
            &.no-results {
                height: unset;
            }
        }
    }
    @media (min-width: 1260px) {
        
        display: grid;
        grid-template-columns: 1fr 1fr;
        grid-template-rows: auto;
        grid-gap: 11px 11px;
        margin-bottom: 0;
        .item {
            margin-bottom: 0;
        }
        .footer .arrow-with-long-line {
            left: 95px;
        }
    }
`;