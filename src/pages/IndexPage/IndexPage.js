import React, {useState} from 'react';
import {observer} from 'mobx-react';
import {popupStore} from '../../mobx/popupStore';
import {StyledIndexPage} from "./styled";
import commonStore from "../../mobx/commonStore";
import {GreetingCard} from "./GreetingCard/GreetingCard";
import {StatusCard} from "./StatusCard/StatusCard";
import {CalculateCard} from "./CalculateCard/CalculateCard";
import {DiagramCard} from "./DiagramCard/DiagramCard";

export const IndexPage = observer(() => {
    const [isCalculationError, setIsCalculationError] = useState(false);

    // TODO Рефакторинг: делается одно и то же в IndexPage и CalculateCard
    const getStylePopup = () => {
        if ((window.matchMedia("(min-width: 1260px)").matches)) {
            return {
                left: 0,
                bottom: 222
            }
        } else if (window.matchMedia("(min-width: 1024px)").matches) {
            return {
                top: 746,
                right: 0
            }
        }
    };

    const onOpenPopupCalorieCalculation = () => {
        if (commonStore.dashboard.accessRights.calculateCalories !== false) {
            if (commonStore.dashboard.accessRights.calculateCalories === true) {
                popupStore.setOpenPopup({
                    type: 'calculate-calories',
                    style: getStylePopup()
                });
                setIsCalculationError(false);
            } else {
                setIsCalculationError(true)
            }
        }
    };

    const onClosePopupCalorieCalculation = () => {
        setIsCalculationError(false);
    };

    return (
        <StyledIndexPage>
            <GreetingCard
                onOpenPopupCalorieCalculation={onOpenPopupCalorieCalculation}/>
            <StatusCard/>
            <CalculateCard
                isCalculationError={isCalculationError}
                onOpenPopupCalorieCalculation={onOpenPopupCalorieCalculation}
                onClosePopupCalorieCalculation={onClosePopupCalorieCalculation}/>
            <DiagramCard />
        </StyledIndexPage>
    )
});



