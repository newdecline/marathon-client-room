import styled from 'styled-components/macro';

export const StyledCalculateCard = styled('div')`
    grid-area: calculate;
    .calculate-card {
        display: flex;
        flex-wrap: wrap;
        padding: 12px 17px 21px 15px;
        box-sizing: border-box;
        color: #454545;
    }
    .title {
        display: flex;
        align-items: center;
        width: 100%;
        font-size: 18px;
        line-height: 120%;
        text-transform: uppercase;
        order: 1;
    }
    .btn {
        margin-left: auto;
    }
    .results {
        margin-top: 18px;
        display: flex;
        justify-content: space-between;
        order: 3;
        flex-basis: 100%;
        font-size: 18px;
        line-height: 120%;
    }
    .result-item {
        &__title {
            margin-bottom: 11px;
        }
        &__value {
            font-size: 36px;
            line-height: 100%;
            text-align: right;
            font-family: "Bodoni";
        }
    }
    @media (min-width: 1024px) {
        .calculate-card {
            position: relative;
            padding: 27px 41px 47px 35px;
        }
        .title {
            width: auto;
            align-self: flex-start;
            margin-right: 110px;
            font-size: 18px;
            text-transform: uppercase;
            line-height: 120%;
        }
        .results {
            flex-basis: unset;
            margin-top: 0;
            order: 2;
        }
        .btn {
            position: absolute;
            right: 21px;
            bottom: 24px;
        }
        .result-item {
            margin-right: 82px;
            &:last-child {
                margin-top: 0;
            }
            &__title {
                font-size: 20px;
            }
            &__value {
            }
        }
    }
    @media (min-width: 1260px) {
        .calculate-card {
            flex-wrap: nowrap;
            justify-content: space-between;
            padding: 36px 41px 33px 36px;
        }
        .title {
            align-items: flex-start;
            flex-direction: column;
            margin-right: 0;
            white-space: nowrap;
        }
        .btn {
            position: relative;
            right: unset;
            bottom: unset;
            margin: 14px 0 0 0;
            .hint {
                right: unset;
                left: 0;
            }
        }
        .result-item {
            margin-right: 25px;
            &__title {
                margin-bottom: 28px;
                font-size: 18px;
            }
            &:last-child {
                margin-right: 0;
            }
        }
    }
`;