import React from "react";
import {observer} from 'mobx-react';
import {StyledCalculateCard} from "./styled";
import {ButtonWithHint} from "../../../components/ButtonWithHint/ButtonWithHint";
import commonStore from "../../../mobx/commonStore";
import {StyledCard} from "../../../components/atoms/StyledCard";

export const CalculateCard = observer(props => {
    const {
        isCalculationError,
        onClosePopupCalorieCalculation,
        onOpenPopupCalorieCalculation
    } = props;

    return (
        <StyledCalculateCard>
            <StyledCard className='calculate-card'>
                <div className="title">
                    Норма калорий
                    <ButtonWithHint
                        onOpenPopupCalorieCalculation={onOpenPopupCalorieCalculation}
                        onClosePopupCalorieCalculation={onClosePopupCalorieCalculation}
                        isCalculationError={isCalculationError}
                        className='btn'
                    />
                </div>

                <div className="results">
                    <div className="result-item">
                        <div className="result-item__title">Снижение веса</div>
                        <div className="result-item__value">
                            {commonStore.dashboard.calories.reduction}
                        </div>
                    </div>
                    <div className="result-item">
                        <div className="result-item__title">Поддержание веса</div>
                        <div className="result-item__value">
                            {commonStore.dashboard.calories.maintenance}
                        </div>
                    </div>
                </div>
            </StyledCard>
        </StyledCalculateCard>
    )
});