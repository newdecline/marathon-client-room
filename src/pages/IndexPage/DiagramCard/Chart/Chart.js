import React, {useEffect} from 'react';
import {string, array} from 'prop-types';
import Chartjs from 'chart.js/dist/Chart.bundle.min';
import checkSvg from '../../../../svg/check.svg';
import notVerifiedSvg from '../../../../svg/not-verified.svg';
import {ReactComponent as CircleIcon} from '../../../../svg/circle.svg';
import {StyledChart} from "./styled";

export const Chart = props => {
    const {title, data, name, checked, className} = props;

    useEffect(() => {
        const ctx = document.getElementById(`myChart-${name}`).getContext('2d');
        let arrVerifications = [];

        const checkIcon = new Image();
        checkIcon.src = checkSvg;

        const notVerifiedIcon = new Image();
        notVerifiedIcon.src = notVerifiedSvg;

        Chartjs.defaults.multicolorLine = Chartjs.defaults.line;
        Chartjs.controllers.multicolorLine = Chartjs.controllers.line.extend({
            draw: function (ease) {
                let
                    startIndex = 0,
                    meta = this.getMeta(),
                    points = meta.data || [],
                    colors = this.getDataset().colors,
                    area = this.chart.chartArea,
                    originalDatasets = meta.dataset._children
                        .filter(function (data) {
                            return !isNaN(data._view.y);
                        });

                function _setColor(newColor, meta) {
                    meta.dataset._view.borderColor = newColor;
                }

                if (!colors) {
                    Chart.controllers.line.prototype.draw.call(this, ease);
                    return;
                }

                for (let i = 2; i <= colors.length; i++) {
                    if (colors[i - 1] !== colors[i]) {
                        _setColor(colors[i - 1], meta);
                        meta.dataset._children = originalDatasets.slice(startIndex, i);
                        meta.dataset.draw();
                        startIndex = i - 1;
                    }
                }

                meta.dataset._children = originalDatasets.slice(startIndex);
                meta.dataset.draw();
                meta.dataset._children = originalDatasets;

                points.forEach(function (point) {
                    point.draw(area);
                });
            }
        });

        new Chartjs(ctx, {
            type: 'multicolorLine',
            data: {
                labels: ['1', '2', '3', '4', '5'],
                datasets: [{
                    label: false,
                    data,
                    colors: data.map((item, i) => {
                        if (data[i] >= data[i - 1]) {
                            return '#CA5151'
                        } else {
                            return '#9AC190'
                        }
                        // инициализировать массив с цветами для линий, рисовать линии разными цветами взависимости от направления линии(вверх/вниз)
                    }),
                    pointRadius: 5,
                    pointBorderWidth: 1,
                    pointStyle: function (context) {
                        const index = context.dataIndex;
                        arrVerifications[index] = checked[index];

                        return arrVerifications.map(item => {
                            if (item === null) {
                                return false
                            } else if (item) {
                                return  checkIcon
                            } else {
                                return notVerifiedIcon
                            }
                        })
                        // проставление иконок статуса проверки
                    },
                    borderWidth: 1,
                    lineTension: 0,
                    pointBorderColor: function (context) {
                        const index = context.dataIndex;
                        const value = context.dataset.data[index];
                        if (value === 0) {
                            return '#454545';
                        } else if (value >= context.dataset.data[index - 1]) {
                            return '#CA5151';
                        } else {
                            return '#9AC190';
                        }
                        // рисовать обводку у точек разными цветами взависимости от направления линии(вверх/вниз)
                    },
                    pointBackgroundColor: function (context) {
                        const index = context.dataIndex;
                        const value = context.dataset.data[index];
                        if (value === 0) {
                            return 'transparent';
                        } else if (value >= context.dataset.data[index - 1]) {
                            return '#CA5151';
                        } else {
                            return '#9AC190';
                        }
                        // рисовать точки разными цветами взависимости от направления линии(вверх/вниз)
                    },
                    fill: false,
                }]
            },
            options: {
                animationEnabled: true,
                animation: {
                    duration: 2000,
                    numSteps: 60,
                    easing: 'easeInOutBounce',
                },
                responsive: true,
                legend: {
                    display: false
                },
                layout: {
                    padding: {
                        left: 9,
                        right: 9,
                        top: 9,
                        bottom: 9
                    }
                },
                scales: {
                    xAxes: [{
                        display: false,
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    yAxes: [{
                        display: false,
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                },
                tooltips: false
            }
        });
    }, [data, name, checked]);

    return (
        <StyledChart
            measurements={data.filter(item => !item).length}
            className={className}>
            <div className="line-chart">
                <div className="line-chart__title">{title}</div>
                <div className="chart-container">
                    <div className="empty-circles">
                        {data.filter(item => !item).map((item, index) => (
                            <div
                                style={{
                                    right: `calc(${index / 4 * 100}%)`
                                }}
                                key={index}
                                className='empty-circle'><CircleIcon/> </div>
                        ))}
                    </div>
                    <canvas
                        id={`myChart-${name}`}
                        className='myChart'
                        style={{height: '100%', width: '100%'}}>

                    </canvas>
                </div>
            </div>
        </StyledChart>
    )
};

Chart.propTypes = {
    title: string,
    data: array,
    name: string,
    checked: array,
    className: string
};