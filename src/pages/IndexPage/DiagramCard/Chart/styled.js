import styled from "styled-components/macro";

export const StyledChart = styled('div')`
    .line-chart {
        width: 100%;
        display: grid;
        grid-template-columns: 20% 80%;
        grid-template-rows: 60px;
        padding: 10px 0;
        box-sizing: border-box;
        border-bottom: 1px solid #e5e5e5;
        &__title {
            align-self: center;
            font-size: 14px;
            line-height: 120%;
            text-transform: uppercase;
        }
    }
    .chart-container {
        position: relative;
    }
    .empty-circles {
        position: absolute;
        top: 50%;
        right: 0;
        display: flex;
        justify-content: flex-end;
        width: calc(100% - 8px);
        height: 8px;
        transform: translate(0, -50%);
    }
    .empty-circle {
        position: absolute;
        display: flex;
        &:first-child {
            transform: translate(-5px, 0);
        }
        &:nth-child(2) {
            transform: translate(-4px, 0);
        }
        &:nth-child(3) {
            transform: translate(-4px, 0);
        }
        &:nth-child(3) {
            transform: translate(-2px, 0);
        }
    }
    @media (min-width: 1024px) {
        .line-chart {
            grid-template-columns: auto 89%;
            .myChart {
                max-width: 100%;
            }
            &__title {
                font-size: 18px;
            }
        }
    }
    @media (min-width: 1260px) {
        .line-chart {
            grid-template-columns: auto 82%;
            grid-template-rows: 56px;
        }
    }
`;