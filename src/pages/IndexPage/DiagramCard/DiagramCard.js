import React from "react";
import {StyledDiagramCard} from "./styled";
import commonStore from "../../../mobx/commonStore";
import {Chart} from "./Chart/Chart";
import {observer} from "mobx-react";
import {StyledCard} from "../../../components/atoms/StyledCard";

export const DiagramCard = observer(() => {

    return (
        <StyledDiagramCard>
            <StyledCard className='diagram-card'>
                <div className="header">
                    <div className="title">замеры</div>
                    <div className="week">
                        <div className="week__item">0</div>
                        <div className="week__item">1</div>
                        <div className="week__item">2</div>
                        <div className="week__item">3</div>
                        <div className="week__item">4</div>
                    </div>
                </div>
                {commonStore.chartsData.map((chartItem, i) => {
                    return (
                        <Chart key={commonStore.fakeIdChart + i} {...chartItem} className='line-chart-wrap'/>)
                })}
            </StyledCard>
        </StyledDiagramCard>
    )
});