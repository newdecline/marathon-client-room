import styled from "styled-components/macro";

export const StyledDiagramCard = styled('div')`
    grid-area: diagram;
    .diagram-card {
        position: relative;
        padding: 19px 23px 0 14px;
        display: flex;
        flex-direction: column;
        box-sizing: border-box;
        color: #454545;
    }
    .header {
        display: flex;
        align-items: center;
        padding-bottom: 20px;
        border-bottom: 1px solid #e5e5e5;
        .title {
            width: 20%;
            font-size: 14px;
            line-height: 120%;
        }
        .week {
            display: flex;
            justify-content: space-between;
            width: 80%;
            &__item {
                font-size: 18px;
                line-height: 120%;
                font-family: "Circe-ExtraBold";
                &:last-child {
                    padding-right: 4px;
                }
            }
        }
    }
    .body-item {
        display: flex;
        &__title {
            align-self: center;
            width: 20%;
            text-transform: uppercase;
            font-size: 14px;
            line-height: 120%;
        }
        &__chart {
            width: 80%;
        }
    }
    .line-chart-wrap {
        &:last-child {
            .line-chart {
                border-bottom: none;
            }
        }
    }
    @media (min-width: 1024px) {
        .diagram-card {
            grid-template-rows: auto auto;
            padding: 19px 44px 5px 35px;
        }
        .header {
            .title {
                width: 11%;
                font-size: 18px;
            }
            .week {
                width: 89%;
                &__item {
                    &:last-child {
                        padding-right: 2px;
                    }
                }
            }
        }
        .body-item__title {
            width: 18%;
            font-size: 18px;
        }
        .body-item__chart {
            width: 82%;
        }
        .body-item {
            border-top: 1px solid #e5e5e5;
        }
    }
    @media (min-width: 1260px) {
        .diagram-card {
            grid-template-rows: auto auto;
            padding: 19px 32px 8px 35px;
        }
        .header {
            margin-bottom: 10px;
            .title {
                width: 18%;
                font-size: 18px;
            }
            .week {
                width: 82%;
            }
        }
        .body-item__title {
            width: 18%;
            font-size: 18px;
        }
        .body-item__chart {
            width: 82%;
        }
        .body-item {
            border-top: 1px solid #E5E5E5;
        }
    }
`;