import styled from "styled-components/macro";

export const StyledNotice = styled('div')`
    border-radius: 5px;
    box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.15);
    .greeting-notice {
        position: relative;
        padding: 9px 44px 9px 45px;
        font-size: 20px;
        line-height: 120%;
        &__text {
            font-size: 20px;
            line-height: 120%;
        }
    }
    .icon {
        display: flex;
        position: absolute;
        top: 15px;
        left: 15px;
    }
    @media (min-width: 1024px) {
        .greeting-notice {
            &__text {
                height: 48px;
                overflow: hidden;
            }
        }
    }
`;