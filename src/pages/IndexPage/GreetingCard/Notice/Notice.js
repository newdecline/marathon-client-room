import React from 'react';
import {ReactComponent as NoFillCheckbox} from '../../../../svg/no-fill-checkbox.svg';
import {string, object} from 'prop-types';
import {StyledNotice} from "./styled";

export const Notice = props => {
    const {styles, message} = props;

    return (
        <StyledNotice className='greeting-notice' style={styles}>
            <NoFillCheckbox className='icon'/>
            <div className='greeting-notice__text'>
                {message}
            </div>
        </StyledNotice>
    )
};

Notice.defaultProps = {
    styles: {
        color: '#000',
        backgroundColor: '#fff'
    },
    message: 'Пока нет для тебя сообщений',
};

Notice.propTypes = {
    styles: object.isRequired,
    message: string.isRequired
};

