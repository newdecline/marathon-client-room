import React, {useState} from "react";
import {observer} from 'mobx-react';
import commonStore from "../../../mobx/commonStore";
import Collapsible from "react-collapsible";
import {StyledGreetingCard} from "./styled";
import {StyledTriggerGreetingTitle} from "./styled";
import arrowIcon from "../../../svg/arrow.svg";
import {Link} from "react-router-dom";
import {ReactComponent as Icon1} from "../../../svg/1.svg";
import {ReactComponent as Icon2} from "../../../svg/2.svg";
import {ReactComponent as Icon3} from "../../../svg/3.svg";
import {ReactComponent as Icon4} from "../../../svg/4.svg";
import {Notice} from "./Notice/Notice";
import {StyledCard} from "../../../components/atoms/StyledCard";

export const GreetingCard = observer(props => {
    const {onOpenPopupCalorieCalculation} = props;

    const [isGreetingOpening, setIsGreetingOpening] = useState(false);

    let collapsibleOptions = {
        trigger: (<StyledTriggerGreetingTitle
            isGreetingOpening={isGreetingOpening}>
            <span>Привет!</span>
            <span><img src={arrowIcon} alt="свернуть\развернуть блок"/></span>
        </StyledTriggerGreetingTitle>),
        onOpening: () => {
            setIsGreetingOpening(true)
        },
        onClosing: () => {
            setIsGreetingOpening(false)
        }
    };

    if (window.matchMedia("(min-width: 1024px)").matches) {
        collapsibleOptions = {
            ...collapsibleOptions,
            open: true,
            triggerDisabled: true
        }
    }

    const renderGreetingText = () => {
        switch (commonStore.dashboard.greeting) {
            case 'preStart':
                return (
                    <>
                        <p className="text hidden-with-notice">Рада видеть тебя среди участниц марафона.</p>
                        <p className="text">Марафон
                            начнется: <b>{commonStore.dashboard.nextMarathonStartDate.split(' ')[0]}</b>
                        </p>
                        <p className="text">Я напомню тебе о старте марафона письмом на e-mail.</p>
                        <p className="text">После старта марафона ты сможешь добавить свои первые
                            замеры,
                            рассчитать норму калорий, а так же получишь доступ к разделам Питание, Рецепты и
                            Тренировки.</p>
                        <p className="text">А пока ты можешь заполнить часть информации в своем <Link
                            className='link'
                            to='profile'>профиле</Link> и внимательно изучить <Link className='link' to='rules'>правила</Link>.
                        </p>
                    </>
                );
            case 'start':
                return (
                    <>
                        <p className="text hidden-with-notice">
                            Рада видеть тебя среди участниц марафона.<br/>
                            Сегодня начнется твое преображение.
                        </p>
                        <p className="text text_icon">
                            <Icon1/>
                            <span>Заполни свой <Link className='link' to='profile'>профиль</Link></span>
                        </p>
                        <p className="text text_icon">
                            <Icon2/>
                            <span>Прочитай <Link className='link' to='rules'>правила</Link></span>
                        </p>
                        <p className="text text_icon">
                            <Icon3/>
                            <span>Прочитай информацию <Link className='link' to='food'>о питании</Link></span>
                        </p>
                        <p className="text text_icon">
                            <Icon4/>
                            <span><span className="link" onClick={onOpenPopupCalorieCalculation}>Рассчитай</span> свою
                                норму калорий
                            </span>
                        </p>
                        <p className="text">
                            Не забывай заглядывать в раздел с рецептами и тренировками.
                        </p>
                    </>
                );
            case 'preEnd':
                return (
                    <>
                        <div className="list-header">
                            Выполняй несколько простых правил и результат не заставит себя ждать:
                        </div>
                        <div className="list-item">
                            <span className="list-item__number">1.</span>
                            <span>Вся еда без вредных Е и сахара</span>
                        </div>
                        <div className="list-item">
                            <span className="list-item__number">2.</span>
                            <span>Кардио тренировки минимум 3 раза в неделю</span>
                        </div>
                        <div className="list-item">
                            <span className="list-item__number">3.</span>
                            <span>Не выходи за нормы калорий</span>
                        </div>
                        <div className="list-item">
                            <span className="list-item__number">4.</span>
                            <span>Жиров в рационе должно быть 1 гр на 1 кг твоего веса</span>
                        </div>
                        <div className="list-item">
                            <span className="list-item__number">5.</span>
                            <span>Есть можно в любое время дня разрешенные продукты</span>
                        </div>
                        <div className="list-item">
                            <span className="list-item__number">6.</span>
                            <span>Пить воду 30 мл на 1 кг веса в день</span>
                        </div>
                        <div className="list-item">
                            <span className="list-item__number">7.</span>
                            <span>Спать нужно минимум 6-8 часов в день</span>
                        </div>
                    </>
                );
            case 'end':
                return (
                    <>
                        <p className="text">Марафон закончился. Но у тебя остается доступ в личный
                            кабинет.<br/>Ты можешь
                            смотреть свои результаты, заходить в свой профиль и менять личную информацию.</p>
                        <p className="text">Будет здорово, если ты не остановишься на достугнутом и решишь
                            продолжать.
                            Присоединяйся к следующему марафону!</p>
                        <a href={commonStore.dashboard.layout.whats_app_url} className="get-in-touch">Написать</a>
                    </>
                );
            case 'admin':
                return (
                    <p className="text">
                        Вы администратор.
                    </p>
                );
            default:
                return (
                    <p className="text">
                        Похоже тебя забыли добавить в марафон.
                        <a href={commonStore.dashboard.layout.whats_app_url}>Свяжись</a> с менеджером в WhatsApp.
                    </p>
                );
        }
    };

    //TODO высота поправить высоту блока

    const cardClasses = ['greeting-card'];
    if (commonStore.dashboard.notification) {
        cardClasses.push('greeting-card_with-notice');
    }

    const renderNotice = () => commonStore.dashboard.notification && (
        <Notice {...commonStore.dashboard.notification} />
    );

    return (
        <StyledGreetingCard>
            {renderNotice()}
            <StyledCard className={cardClasses.join(' ')}>
                <Collapsible {...collapsibleOptions}>
                    {renderNotice()}
                    {renderGreetingText()}
                </Collapsible>
            </StyledCard>
        </StyledGreetingCard>
    )
});