import styled from 'styled-components/macro';

export const StyledGreetingCard = styled('div')`
    grid-area: greeting;
    .greeting-notice {
        padding: 13px 44px 9px 60px;
        margin: 0 0 12px 0;
        svg {
            left: 30px;
        }
    }
    .greeting-card {
        padding: 20px 30px 24px 28px;
        color: #454545;
        box-sizing: border-box;
        .greeting-notice {
            display: none;
            margin: 17px 0 23px;
        }
        &_with-notice {
            @media (min-width: 1024px) {
                .hidden-with-notice {
                    display: none;
                }
            }
        }
    }
    .text {
        margin-top: 28px;
        font-size: 20px;
        white-space: pre-wrap;
        &_icon {
            margin: 20px 0;
            display: flex;
            align-items: center;
            svg {
                margin-right: 20px;
                flex-shrink: 0;
            }
        }
    }
    b {
        font-family: "Circe-ExtraBold";
    }
    .link {
        display: inline-block;
        position: relative;
        text-decoration: none;
        color: inherit;
        cursor: pointer;
        &::before {
            content: '';
            width: 100%;
            height: 1px;
            position: absolute;
            left: 0;
            bottom: 3px;
            background-color: #454545;
        }
    }
    .get-in-touch {
        padding: 19px 42px 15px 48px;
        text-decoration: none;
        display: inline-block;
        border: 2px solid #454545;
        font-weight: 700;
        text-transform: uppercase;
        font-size: 20px;
        line-height: 120%;
        letter-spacing: 0.4em;
        color: #454545;
        box-sizing: border-box;
    }
    .list-header {
        font-size: 20px;
        font-family: "Circe-Regular";
        font-weight: bold;
        margin-bottom: 10px;
        margin-top: 23px;
    }
    .list-item {
        font-size: 20px;
        margin-bottom: 9px;
        &__number {
            font-family: "Circe-Regular";
            font-weight: bold;
            margin-right: 5px;
        }
    }
    @media (min-width: 1024px) {
        .greeting-notice {
            display: none;
        }
        .greeting-card {
            padding: 29px 30px 39px 35px;
            height: 100%;
            max-height: 513px;
            .greeting-notice {
                position:relative;
                display: block;
                padding: 13px 44px 9px 45px;
                margin: 17px 0;
                box-shadow: none;
                svg {
                    left: 15px;
                }
            }
        }
        .subtitle {
            margin: 21px 0 45px 0;
        }
        .list-steps {
            &__item {
                margin-bottom: 31px;
                padding-left: 66px;
            }
        }
    }
    @media (min-width: 1260px) {
        .greeting-card {
            padding: 29px 30px 39px 35px;
            max-height: 479px;
        }
        .subtitle {
            margin: 18px 0 31px 0;
        }
        .list-steps {
            &__item {
                margin-bottom: 31px;
            }
        }
    }
`;

export const StyledTriggerGreetingTitle = styled('div')`
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-family: 'Bodoni';
    font-size: 30px;
    color: #454545;
    img {
        display: inline-block;
        transform: ${({isGreetingOpening}) => isGreetingOpening ? 'rotate(180deg)' : 'rotate(0)'};
        transition: transform .5s;
    }
    @media (min-width: 1024px) {
        font-size: 46px;
        img {
            display: none;
        }
    }
`;