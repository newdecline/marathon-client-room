import styled from "styled-components/macro";

export const StyledIndexPage = styled('div')`
    grid-template-columns: 1fr;
    grid-template-rows: 1fr;
    grid-template-areas: 
    "greeting"
    "status-cards"
    "calculate"
    "diagram";
    position: relative;
    display: grid;
    grid-gap: 11px 0;
    margin: 0 20px 36px 22px;
    @media (min-width: 1024px) {
        max-width: 940px;
        width: 100%;
        margin: 0 auto 36px auto;
        grid-gap: 19px 22px;
        grid-template-columns: 1fr 287px;
        grid-template-rows: auto;
        grid-template-areas: 
        "greeting status-cards"
        "calculate calculate"
        "diagram diagram";
        .notation {
            margin: 44px 0 0 0;
            max-width: 440px;
        }
    }
    @media (min-width: 1260px)  {
        max-width: 1200px;
        margin: 0 auto 36px auto;
        grid-gap: 22px;
        grid-template-columns: 1fr 1fr;
        grid-template-rows: auto;
        grid-template-areas: 
        "greeting status-cards"
        "greeting diagram"
        "calculate diagram";  
        .notation {
            margin-top: 43px;
        }
    }
`;