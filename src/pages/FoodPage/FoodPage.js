import React from "react";
import {TextPage} from "../TextPage/TextPage";

const FoodPage = () => {
    return (
        <TextPage pageKey='food' />
    );
};

export {FoodPage};