import styled from "styled-components/macro";

export const StyledFoodGallery = styled('div')`
    margin-bottom: 30px;
    .slick-list {
        
    }
    .slick-slide > div {
        margin-right: 14px;
    }
    .slick-arrow {
        &.slick-next, &.slick-prev {
            &::before {
                content: '';
            }
        }
        &.slick-next {
            display: none;
            right: 0;
            svg {
                transform: rotate(-90deg);
            }
        }
        &.slick-prev {
            display: none;
            left: 0;
            svg {
                transform: rotate(90deg);
            }
        }
    }
    .food-card {
        box-shadow: none;
        overflow: hidden;
        &__label {
            margin: 19px 35px 19px 23px;
            font-size: 20px;
            line-height: 120%;
            white-space: pre-wrap;
            height: 72px;
            overflow: hidden;
        }
    }
    @media (min-width: 1024px) {
        .food-card {
            &__label {
                margin: 29px 35px 19px 23px;
            }
        }
        .slick-list {
            margin: 0 -7px;
            .slick-track {
                margin-left: 0;
            }
        }
        .slick-slide > div {
            margin: 0 7px;
        }
        .slick-arrow {
            &.slick-next, &.slick-prev {
                &::before {
                    content: '';
                }
            }
            &.slick-next {
                display: flex;
                right: -30px;
                svg {
                    transform: rotate(-90deg);
                }
            }
            &.slick-prev {
                display: flex;
                left: -30px;
                svg {
                    transform: rotate(90deg);
                }
            }
        }
    }
    @media (min-width: 1260px) {
        //.slick-list {
        //    margin: 0 -7px;
        //}
        //.slick-slide > div {
        //    margin: 0 7px;
        //}
        
        //.food-card {
        //    background-color: #fff;
        //    border-radius: 5px 5px 5px 5px;
        //    overflow: hidden;
        //    &__label {
        //        margin: 29px 35px 19px 23px;
        //        font-size: 20px;
        //        line-height: 120%;
        //    }
        //}
    }
`;