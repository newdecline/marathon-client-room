import React from 'react';
import Slider from "react-slick";
import uuid from "uuid/v4";
import {ReactComponent as ArrowIcon} from "../../../svg/arrow.svg";
import {StyledFoodGallery} from "./styled";
import {StyledCard} from "../../../components/atoms/StyledCard";

export const FoodGallery = ({slides}) => {
    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        nextArrow: <CustomNextArrow />,
        prevArrow: <CustomPrevArrow />,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1.15
                }
            },
        ]
    };

    const foodGallery = slides.map(slide => ({
        label: slide.text,
        srcImg: slide.image.cropped && slide.image.cropped.url,
    }));

    return (
        <StyledFoodGallery>
            <Slider {...settings}>
                {
                    foodGallery.map(item => (
                        <StyledCard key={uuid()} className='food-card'>
                            <div className="food-card__img-wrap">
                                <img src={item.srcImg} alt="alt"/>
                            </div>
                            <p className="food-card__label">{item.label}</p>
                        </StyledCard>
                    ))
                }
            </Slider>
        </StyledFoodGallery>
    )
};

function CustomNextArrow(props) {
    const { className, onClick } = props;
    return (
        // eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions
        <button
            className={className}
            onClick={onClick}
        ><ArrowIcon/></button>
    );
}

function CustomPrevArrow(props) {
    const { className, onClick } = props;
    return (
        // eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions
        <button
            className={className}
            onClick={onClick}
        ><ArrowIcon/></button>
    );
}