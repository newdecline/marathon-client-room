import styled from "styled-components/macro";

export const StyledRecipePage = styled('div')`
    margin: auto 0;
    .container {
        position: relative;
        padding: 0 21px;
    }
    .name-recipe {
        max-width: 796px;
        margin: 21px 0 35px 0;
        font-family: 'Bodoni';
        font-size: 30px;
        line-height: 100%;
    }
    .card { 
        margin: 0 0 30px 0;
        display: flex;
        flex-direction: column;
        overflow: hidden;
        &__img-wrap {
            width: 100%;
        }
    }
    .ingredients {
        flex: 1;
        padding: 34px 20px 23px 29px;
        box-sizing: border-box;
        &__title {
            text-align: left;
            margin: 0 0 17px 0;
            font-size: 20px;
            line-height: 120%;
            font-family: 'Circe-Regular';
            font-weight: 700;
            text-transform: uppercase;
        }
    }
    .ingredients-table {
        width: 100%;
    }
    .ingredients-list {
        &__item {
            display: flex;
            justify-content: space-between;
            margin: 0 0 12px 0;
            border-bottom: 1px solid #ccc;
            font-size: 20px;
            line-height: 120%;
        }
    }
    .ingredient-name {
        margin-bottom: -8px;
        text-align: left;
        font-weight: 300;
        span {
            padding-right: 10px;
            background-color: #fff;
        }
    }
    .ingredient-value {
        margin-bottom: -8px;
        align-self: flex-end;
        white-space: nowrap;
        font-weight: 300;
        span {
            padding-left: 10px;
            background-color: #fff;
        }
    }
    .ingredient-line {
        display: inline-block;
        box-sizing: border-box;
        border-bottom: 1px solid #cbcbcb;
    }
    .instruction {
        margin: 0 0 54px 0;
        &__title {
            margin: 0 0 20px 0;
            text-transform: uppercase;
            font-family: 'Circe-Regular';
            font-weight: 700;
            font-size: 20px;
            line-height: 120%;
        }
        &__text {
            font-size: 20px;
            p {
                margin: 0;
            }
        }
    }
    @media (min-width: 1024px) {
        .container {
            max-width: 940px;
            margin: 0 auto;
            padding: 0;
        }
        .name-recipe {
            max-width: 796px;
            margin: 20px 0 35px 0;
            font-family: 'Bodoni';
            font-size: 46px;
            line-height: 100%;
        }
        .card { 
            margin: 0 0 41px 0;
            display: flex;
            flex-direction: row;
            border-radius: 5px;
            box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.15);
            background-color: #fff;
            overflow: hidden;
            &__img-wrap {
                width: 459px;
            }
        }
        .ingredients {
            padding: 31px 42px 26px 34px;
            &__title {
                margin: 0 0 11px 0;
            }
        }
        .ingredients-list {
            &__item {
                margin: 0 0 11px 0;
            }
        }
        .instruction-list__item {
            margin: 0 0 25px 0;
        }
        .measurements-popup {
            width: 589px;
            .body-measurements {
                flex: 1;
            }
            .body-measurements__label {
                margin-right: auto;
            }
            .body-upload-file {
                width: 164px;
            }
            .upload-file {
                height: 100%;
                width: 164px;
            }
            .drop-zone {
                height: 100%;
                flex-direction: column;
                justify-content: flex-end;
                padding: 0 13px 12px 13px;
                box-sizing: border-box;
                text-align: center;
                svg {
                    margin-right: 0;
                    margin-bottom: 36px;
                }
            }
            .drop-zone-wrap {
                height: 100%;
            }
            .drop-zone {
                flex-direction: column;
                height: 100%;
                width: 164px;
            }
            .field-wrap {
                width: 164px;
                flex-basis: unset;
            }
        }
    }
    @media (min-width: 1260px) {
        .container {
            position: relative;
            max-width: 1200px;
            margin: 0 auto;
        }
        .name-recipe {
            margin: 2px 0 35px 0;
        }
        .card {
            margin: 0 0 52px 0;
            &__img-wrap {
                width: 589px;
            }
        }
        .ingredients {
            flex: 1;
            padding: 38px 47px 38px 44px;
            box-sizing: border-box;
            &__title {
                margin: 0 0 22px 0;
                font-size: 20px;
                line-height: 120%;
                font-family: 'Circe-Regular';
                font-weight: 700;
                text-transform: uppercase;
            }
        }
        .ingredients-list {
            margin: 0;
            padding: 0;
            list-style-type: none;
            &__item {
                margin: 0 0 4px 0;
                display: flex;
                align-items: baseline;
                font-size: 20px;
                line-height: 150%;
            }
        }
        .ingredient-line {
            flex: 1;
            margin: 0 10px;
            box-sizing: border-box;
            border-bottom: 1px solid #cbcbcb;
        }
        .instruction {
            margin: 0 0 85px 0;
            &__title {
                margin: 0 0 20px 0;
                text-transform: uppercase;
                font-family: 'Circe-Regular';
                font-weight: 700;
                font-size: 20px;
                line-height: 120%;
            }
        }
        .instruction-list {
            margin: 0;
            padding: 0;
            list-style-type: none;
            &__item  {
                margin: 0 0 24px 0;
                font-size: 20px;
                line-height: 120%;
                &:last-child {
                    margin-bottom: 0;
                }
            }
        }
    }
`;