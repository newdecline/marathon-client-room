import React from 'react';
import {observer} from "mobx-react";
import {StyledRecipePage} from "./styled";
import autobind from "autobind";
import {observable} from "mobx";
import apiService from "../../apiService";
import {Preloader} from "../../components/Preloader/Preloader";
import {StyledCard} from "../../components/atoms/StyledCard";
import commonStore from "../../mobx/commonStore";

@autobind
@observer
class RecipePage extends React.Component {

    @observable recipe = null;

    async componentDidMount() {
        const {match} = this.props;
        const {id} = match.params;

        const response = await apiService.getRecipe(id);
        if (response.status === 200) {
            this.recipe = response.body;
        }
    }

    render() {
        return (
            <StyledRecipePage>
                {
                    (commonStore.loading || this.recipe === null)
                        ? <Preloader/>
                        : <div className="container">
                            <div className="name-recipe">{this.recipe.header}</div>
                            <StyledCard className="card">
                                <div className="card__img-wrap">
                                    <img src={this.recipe.image.cropped && this.recipe.image.cropped.url} alt='/'/>
                                </div>
                                <div className="ingredients">
                                    <table className='ingredients-table'>
                                        <caption className="ingredients__title">Ингредиенты</caption>
                                        <tbody>
                                        {
                                            JSON.parse(this.recipe.ingredients).map((ingredient, index) => {
                                                if (ingredient.name) {
                                                    return (
                                                        <tr key={index} className="ingredients-list__item">
                                                            <th className="ingredient-name"><span>{ingredient.name}</span>
                                                            </th>
                                                            <th className="ingredient-value">
                                                                <span>{ingredient.amount}</span>
                                                            </th>
                                                        </tr>
                                                    );
                                                }
                                            })
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </StyledCard>
                            <div className="instruction">
                                <div className="instruction__title">Инструкция приготовления</div>
                                <div
                                    className="instruction__text"
                                    dangerouslySetInnerHTML={{__html: this.recipe.text}}
                                />
                            </div>
                        </div>
                }
            </StyledRecipePage>
        );
    }
}

export {RecipePage};
