import styled from "styled-components/macro";

export const StyledProfilePage = styled('div')`
    margin: 0 20px 33px 22px;
    display: grid;
    grid-template-columns: auto;
    grid-template-rows: auto;
    grid-gap: 17px 0;
    grid-template-areas: 
        "my-profile"
        "first-measurements"
        "calculate"
        "my-results"
        "photo-report"
        "archive";
    color: #454545;
    @media (min-width: 1024px) {
        max-width: 940px;
        margin: 0 auto;
        grid-template-columns: 333px 589px;
        grid-template-areas: 
            "my-profile first-measurements"
            "calculate calculate"
            "my-results my-results"
            "photo-report photo-report"
            "archive archive";
        grid-gap: 18px;
    }
    @media (min-width: 1260px) {
        max-width: 1200px;
        grid-template-columns: 1fr 1fr;
        grid-gap: 22px; 
    }
    
    .archive {
        margin-top: -24px;
        grid-area: archive;
        &__header {
            margin-bottom: 24px;
            font-size: 30px;
            font-family: 'Bodoni';
        }
        &__item {
            padding-bottom: 15px;
            & > div {
                &:first-of-type {
                    margin-bottom: 17px;
                }
            }
        }
    }
    @media (min-width: 1024px) {
        .archive {
            &__header {
                font-size: 46px;
                margin-bottom: 40px;
            }
        }
    }
`;

export const StyledInputField = styled('label')`
    position: relative;
    padding: 13px 35px 12px 14px;
    display: inline-block;
    flex-basis: 56%;
    border: 1px solid #C3C3C3;
    font-size: 20px;
    line-height: 120%;
    color: #c3c3c3;
    box-sizing: border-box;
    input {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        max-width: 100%;
        width: 100%;
        border: none;
        &:active, &:focus {outline: none;}
    }
    .measure {
        position: absolute;
        right: 14px;
        top: 50%;
        transform: translateY(-50%);
    }
`;

export const StyledCheckbox = styled('label')`
    position: relative;
    padding-left: 23px;
    input {
        position: absolute;
        visibility: hidden;
        z-index: -1;
    }
    .fake-checkbox {
        top: 7px;
        left: 0;
        width: 15px;
        height: 15px;        
        position: absolute;
        border: 1px solid #C4C4C4;
        box-sizing: border-box;
        border-radius: 3px;
        background-color: transparent;
        transition: background-color .3s;
    }
    input:checked + .fake-checkbox {
        background-color: #000;
    }
    @media (min-width: 1260px) {
        &:hover {
            cursor: pointer;
        }
    }
`;