import React from "react";
import {observer} from 'mobx-react';
import {StyledViewWithFirstMeasurements} from "./styled";
import commonStore from "../../../mobx/commonStore";
import {ReactComponent as PenIcon} from "../../../svg/pen.svg";
import {popupStore} from "../../../mobx/popupStore";
import {StyledHeaderCard} from "../../../components/atoms/StyledHeaderCard";

const fields = [
    {
        label: 'Вес',
        fieldName: 'weight',
        measure: 'кг'
    },
    {
        label: 'Обхват груди',
        fieldName: 'chest',
        measure: 'см'
    },
    {
        label: 'Обхват талии',
        fieldName: 'waist',
        measure: 'см'
    },
    {
        label: 'Обхват бедер',
        fieldName: 'hip',
        measure: 'см'
    }
];

export const ViewWithFirstMeasurements = observer(props => {
    const {hasStress} = props;

    const handleClickEditMeasurementsBtn = () => {
        commonStore.editingResultsIndex = 0;
        popupStore.setOpenPopup({
            type: 'edit-measurements',
            style: {right: 0}
        });
    };

    const handleClickImage = (imageUrl) => {
        popupStore.setOpenPopup({
            type: 'photo-viewer',
            style: {
                position: 'fixed',
                top: 0,
                left: 0,
            },
            imageUrl: imageUrl
        });
    };

    const hasFirstResults = commonStore.dashboard.results.length > 0;
    let firstData = {};
    if (hasFirstResults) {
        firstData = commonStore.dashboard.results[0];
    }

    // TODO Стили для disabled кнопки (+ то же для профиля)
    return (
        <StyledViewWithFirstMeasurements>
            <StyledHeaderCard className='header-card'>
                <div className={!firstData.isEditableFromClient
                    ? "title disabled"
                    : "title"}>Первые замеры</div>
                {firstData.isEditableFromClient && (
                    <button
                        type='button'
                        onClick={handleClickEditMeasurementsBtn}
                        className="edit-btn"
                    ><span className="edit-btn__text">Редактировать</span><PenIcon/>
                    </button>
                )}
            </StyledHeaderCard>
            <div className="body">
                <ul className="body-measurements">
                    {fields.map(item => (
                        <li key={item.label} className="body-measurements__item">
                                <span
                                    className="body-measurements__item-label">{item.label}</span>
                            <span
                                className="body-measurements__item-measure">{firstData[item.fieldName]} {item.measure}</span>
                        </li>
                    ))}
                </ul>
                <div className="body-photo">
                    <img src={firstData.image && firstData.image.url}
                         alt='/'
                    onClick={() => handleClickImage(firstData.image.url)}/>
                </div>
            </div>
            <div className="footer">
                {!hasStress
                    ? 'Нет факторов стресса'
                    : 'Фактор стресса присутствует'}
            </div>
        </StyledViewWithFirstMeasurements>
    )
});