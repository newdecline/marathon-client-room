import styled from "styled-components/macro";

export const StyledViewWithFirstMeasurements = styled('div')`
    color: #454545;
    .body {
        &-measurements {
            padding: 0;
            list-style-type: none;
            &__item {
                display: flex;
                margin-bottom: 24px;
                font-size: 20px;
                line-height: 120%;
                &:last-child {
                    margin-bottom: 0;
                }
                &-label {
                    width: 127px;
                    margin-right: 34px;
                }
            }
        }
        &-photo {
            position:relative;
            width: 164px;
            height: 199px;
            overflow: hidden;
            &:hover {
                cursor:pointer;
            }
            img {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                width: 100%;
                height: 100%;
                object-fit: cover;
            }
        }
    }
    .footer {
        font-size: 20px;
        line-height: 150%;
    }
    @media (min-width: 1024px) {
        height: 100%;
        display: flex;
        flex-direction: column;
        .body {
            min-height: 258px;
        }
        .footer {
            margin-top: auto;
            min-height: 75px;
            box-sizing: border-box;
        }
    }
`;