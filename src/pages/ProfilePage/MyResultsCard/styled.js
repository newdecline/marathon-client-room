import styled from 'styled-components/macro';

export const StyledMyResults = styled('div')`
    grid-area: my-results;
    .body {
        padding: 22px 17px 11px 22px;
    }
    .mobile-table {
        display: flex;
        padding-bottom: 22px;
    }
    .column {
        font-size: 18px;
        line-height: 120%;
        text-transform: uppercase;
        &:nth-child(3) {
            display:flex;
            flex-direction: column;
            justify-content: space-between;
            margin-left: auto;
        }
        &__title {
            display: inline-block;
            width: 101px;
        }
        &__name {
            width: 60px;
            margin-right: 18px;
        }
        &__value {
            width: 60px;
            text-align: right;
        }
        &__edit {
            &.check {
                opacity: .5;
                pointer-events: none;
            }
        }
    }
    .row {
        display: flex;
        margin-bottom: 14px;
        &:last-child {
            margin-bottom: 0;
        }
    }
    .designation-of-measure {
        text-transform: lowercase;
    }
    .photo-popup {
        &-enter {
            .photo-overlay {
                background-color: rgba(0, 0, 0, 0);
            }
            img {
                transform: scale(0);
            }
        }
        &-enter-active {
            .photo-overlay {
                background-color: rgba(0, 0, 0, .8);
                transition: all .5s;
            }
            img {
                transform: scale(1);
                transition: all .5s;
            }
        }
        &-exit {
             .photo-overlay {
                background-color: rgba(0, 0, 0, 0.6);
            }
            img {
                transform: scale(1);
            }
        }
        &-exit-active {
            .photo-overlay {
                background-color: rgba(0, 0, 0, 0);
                transition: all .5s;
            }
            img {
                transform: scale(0);
                transition: all .5s;
            }
        }
    }
    .photo-overlay {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        display:flex;
        align-items: center;
        justify-content: center;
        z-index: 10;
        background-color: rgba(0, 0, 0, .8);
    }
    .photo-wrap {
        max-width: 70%;
        overflow: hidden;
    }
    .close-popup {
        position: absolute;
        right: 30px;
        top: 30px;
        display: flex;
        padding: 0;
        border: none;
        background-color: transparent;
        z-index: 10;
        &:hover {
            cursor: pointer;
        }
        rect {
            fill: #fff;
        }
    }
    @media (min-width: 1024px) {
        .body {
            padding: 35px 24px 35px 35px;
        }
        .row {
            margin-bottom: 36px;
        }
        .desktop-table {
            font-size: 20px;
            line-height: 120%;
            text-transform: uppercase;
             &__header {
                display: flex;
                margin-bottom: 36px;
             }
        }
        .name-body-part {
            width: 100px;
            margin-right: 30px;
            text-align: right;
            &.empty {
                width: 108px;
                margin-right: 0;
            }
            &.scale {
                text-align: center;
            }
        }
        .row__title {
            width: 108px;
        }
        .row__value {
            width: 100px;
            margin-right: 30px;
            text-align: right;
            &.check {
                text-align: left;
                color: #CBCBCB;
                pointer-events: none;
            }
            &.camera {
                text-align: center;
            }
        }
        .row-value__photo {
            display: inline-flex;
            &:hover {
                cursor:pointer;
            }
        }
        .row__value-edit {
            padding-top: 5px;
            font-size: 14px;
            line-height: 120%;
            font-family: "Circe-ExtraBold";
            display: flex;
            div {
                cursor:pointer;
                &:first-of-type {
                    margin-right: 42px;
                }
                svg {
                    height: 22px;
                }
            }
        }
    }
    @media (min-width: 1260px) {
        .row__title {
            margin-right: 30px;
        }
        .name-body-part {
            margin-right: 67px;
            &.empty {
                margin-right: 30px;
            }
        }
        .row__value {
            margin-right: 67px;
            &.camera {
                margin-right: 45px;
            }
        }
        .row__value-edit {
            div {
                &:first-of-type {
                    margin-right: 38px;
                }
                svg {
                    height: 21px;
                    margin-top: -4px;
                }
            }
        }
    }
`;