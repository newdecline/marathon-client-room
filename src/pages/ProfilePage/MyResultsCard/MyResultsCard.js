import React, {useEffect, useState} from "react";
import {observer} from "mobx-react";
import commonStore from "../../../mobx/commonStore";
import {StyledMyResults} from "./styled";
import {ReactComponent as PenIcon} from "../../../svg/pen.svg";
import {ReactComponent as CameraIcon} from "../../../svg/camera.svg";
import {ReactComponent as Trash2Icon} from "../../../svg/trash2.svg";
import {popupStore} from "../../../mobx/popupStore";
import {StyledCard} from "../../../components/atoms/StyledCard";
import {StyledHeaderCard} from "../../../components/atoms/StyledHeaderCard";

export const MyResultsCard = observer(({marathonResults}) => {
    const handlerClickBody = (e) => {
        if (isOpenPopup) {
            if (!e.target.closest('.photo-wrap') && !e.target.closest('.close-popup')) {
                setIsOpenPopup(!isOpenPopup);
            }
        }
    };

    const [isOpenPopup, setIsOpenPopup] = useState(false);
    useEffect(() => {
        document.body.addEventListener('click', handlerClickBody);
        return () => {
            document.body.removeEventListener('click', handlerClickBody);
        }
    }, [isOpenPopup]);

    const getStyles = () => {
        if (window.matchMedia("(min-width: 1024px)").matches) {
            return {
                position: 'fixed',
                top: '50%',
                left: '50%',
                //TODO если выстаить значения translate3d в процентах то карточка размывается
                transform: 'translate3d(-280px, -223px, 0)'
            }
        }
    };

    const handleClickEditBtn = (index) => {
        commonStore.editingResultsIndex = index;
        popupStore.setOpenPopup({
            type: 'table-edit-measurements',
            style: getStyles()
        });
    };

    const handleClickCameraBtn = (imageUrl) => {
        popupStore.setOpenPopup({
            type: 'photo-viewer',
            style: {
                position: 'fixed',
                top: 0,
                left: 0,
            },
            imageUrl: imageUrl
        });
    };

    const weekTitle = i => i === 0 ? 'Старт' : `${i} неделя`;

    const renderDeleteButton = (id) => {
        return (
            <div className="delete-result-button" onClick={() => {
                commonStore.deletingResultId = id;
                popupStore.setOpenPopup({
                    type: 'delete-results',
                    style: {
                        position: 'fixed',
                        top: 0,
                        left: 0,
                    },
                });
            }}>
                <Trash2Icon />
            </div>
        );
    };

    const renderTable = () => {
        if (window.matchMedia("(min-width: 1024px)").matches) {
            return <div className='desktop-table'>
                <div className="desktop-table__header">
                    <div className="name-body-part empty"/>
                    <div className="name-body-part">Вес</div>
                    <div className="name-body-part">Грудь</div>
                    <div className="name-body-part">Талия</div>
                    <div className="name-body-part">Бедра</div>
                    <div className="name-body-part scale">Весы</div>
                </div>
                {marathonResults.map((result, i) => {
                    let buttonClasses = ['row__value'];
                    if (!result.isEditableFromClient) {
                        buttonClasses.push('check');
                    }

                    return (
                        <div key={i} className="row">
                            <div className="row__title">{weekTitle(i)}</div>
                            <div className="row__value">{result.weight} <span
                                className="designation-of-measure">кг</span></div>
                            <div className="row__value">{result.chest} <span
                                className="designation-of-measure">см</span></div>
                            <div className="row__value">{result.waist} <span
                                className="designation-of-measure">см</span></div>
                            <div className="row__value">{result.hip} <span className="designation-of-measure">см</span>
                            </div>
                            <div className="row__value camera">
                                <div onClick={() => handleClickCameraBtn(result.image && result.image.url)}
                                     className="row-value__photo camera"><CameraIcon/>
                                </div>
                            </div>
                            <div className={buttonClasses.join(' ')}>
                                <div className="row__value-edit">
                                    {result.isEditableFromClient ? (
                                        <>
                                            <div onClick={() => handleClickEditBtn(i)} title="Редактировать">
                                                {window.matchMedia("(min-width: 1260px)").matches ? (
                                                    'Редактировать'
                                                ) : (
                                                    <PenIcon/>
                                                )}
                                            </div>
                                            {renderDeleteButton(result.id)}
                                        </>
                                    ) : (
                                        'Проверено'
                                    )}
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        } else {
            return marathonResults.map((result, i) => {
                let buttonClasses = ['column__edit'];
                if (!result.isEditableFromClient) {
                    buttonClasses.push('check');
                }

                return <div key={i} className='mobile-table'>
                    <div className="column">
                        <span className="column__title">{weekTitle(i)}</span>
                    </div>
                    <div className="column">
                        <div className="row">
                            <div className="column__name">Вес</div>
                            <div className="column__value">{result.weight}
                                <span className="designation-of-measure">кг</span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="column__name">Грудь</div>
                            <div className="column__value">{result.chest}
                                <span className="designation-of-measure">см</span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="column__name">Талия</div>
                            <div className="column__value">{result.waist}
                                <span className="designation-of-measure">см</span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="column__name">Бедра</div>
                            <div className="column__value">{result.hip}
                                <span className="designation-of-measure">см</span>
                            </div>
                        </div>
                    </div>
                    <div className="column">
                        <div onClick={() => handleClickEditBtn(i)} className={buttonClasses.join(' ')}>
                            <PenIcon/>
                        </div>
                        {result.isEditableFromClient && renderDeleteButton(result.id)}
                        <div
                            onClick={() => handleClickCameraBtn(result.image && result.image.url)}
                            className="column__photo"
                        >
                            <CameraIcon/>
                        </div>
                    </div>
                </div>
            })
        }
    };

    return <StyledMyResults>
        <StyledCard className='my-results'>
            <StyledHeaderCard>
                <div className="title">Мои результаты</div>
            </StyledHeaderCard>

            <div className="body">
                {renderTable()}
            </div>
        </StyledCard>
    </StyledMyResults>
});