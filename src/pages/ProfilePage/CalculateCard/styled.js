import styled from "styled-components/macro";

export const StyledCalculateCard = styled('div')`
    grid-area: calculate;
    .calculate-card {
        display: flex;
        flex-wrap: wrap;
        padding: 12px 17px 21px 15px;
        box-sizing: border-box;
        color: #454545;
    }
    .title {
        align-self: center;
        flex-basis: 57%;
        font-size: 18px;
        line-height: 120%;
        text-transform: uppercase;
        order: 1;
    }
    .results {
        margin-top: 18px;
        display: flex;
        justify-content: space-between;
        order: 3;
        flex-basis: 100%;
        font-size: 18px;
        line-height: 120%;
    }
    .result-item {
        &__title {
            margin-bottom: 11px;
        }
        &__value {
            font-size: 36px;
            line-height: 100%;
            text-align: right;
            font-family: "Bodoni";
        }
    }
    .btn {
        flex-basis: auto;
        margin-left: auto;
        order: 2;
    }
    @media (min-width: 1024px) {
        .calculate-card {
            padding: 10px 44px 48px 35px;
            flex-wrap: nowrap;
        }
        .title {
            margin-right: 110px;
            margin-bottom: 24px;
            flex-basis: unset;
            font-size: 18px;
            line-height: 120%;
        }
        .results {
            flex-basis: unset;
            margin-top: 15px;
            order: 2;
        }
        .btn {
            align-self: flex-end;
            margin-top: auto;
            order: 3;
        }
        .result-item {
            margin-right: 78px;
            &:last-child {
                margin-top: 0;
            }
            &__title {
                font-size: 20px;
            }
            &__value {
            }
        }
    }
    @media (min-width: 1260px) {
        .calculate-card {
            padding: 29px 35px 35px 35px;
        }
        .title {
            margin-right: 114px;
            margin-bottom: 24px;
            flex-basis: unset;
            font-size: 46px;
            font-family: 'Bodoni';
            text-transform: unset;
        }
        .results {
            flex-basis: unset;
            margin-top: 15px;
            order: 2;
        }
        .btn {
            align-self: flex-end;
            order: 3;
        }
        .result-item {
            margin-right: 82px;
            &:last-child {
                margin-top: 0;
            }
            &__title {
                font-size: 20px;
            }
        }
    }
`;