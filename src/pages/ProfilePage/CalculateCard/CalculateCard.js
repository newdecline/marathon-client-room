import React, {useState, useRef} from "react";
import {observer} from 'mobx-react';
import commonStore from "../../../mobx/commonStore";
import {ButtonWithHint} from "../../../components/ButtonWithHint/ButtonWithHint";
import {popupStore} from "../../../mobx/popupStore";
import {StyledCalculateCard} from "./styled";
import {offsetElement} from "../../../utilities/offsetElement";
import {StyledCard} from "../../../components/atoms/StyledCard";

export const CalculateCard = observer(() => {
    const [isCalculationError, setIsCalculationError] = useState(false);
    const calculateCardRef = useRef(null);

    // TODO Рефакторинг: делается одно и то же в IndexPage и CalculateCard
    const getStylePopup = () => {
        if (calculateCardRef !== null) {
            if ((window.matchMedia("(min-width: 1260px)").matches)) {
                return {
                    right: 0,
                    top: offsetElement(calculateCardRef.current).top,
                    transform: 'translateY(calc(-100% + 147px))'
                }
            } else if (window.matchMedia("(min-width: 1024px)").matches) {
                return {
                    top: 671,
                    right: 0
                }
            }
        }
    };

    const onOpenPopupCalorieCalculation = () => {
        if (commonStore.dashboard.accessRights.calculateCalories !== false) {
            if (commonStore.dashboard.accessRights.calculateCalories === true) {
                popupStore.setOpenPopup({
                    type: 'calculate-calories',
                    style: getStylePopup()
                });
                setIsCalculationError(false);
            } else {
                setIsCalculationError(true)
            }
        }
    };

    const onClosePopupCalorieCalculation = () => {
        setIsCalculationError(false);
    };

    return (
        <StyledCalculateCard ref={calculateCardRef}>
            <StyledCard className='calculate-card'>
                <div className={commonStore.dashboard.accessRights.calculateCalories === false
                    ? "title disabled"
                    : "title"}>Норма калорий
                </div>
                <div className={commonStore.dashboard.accessRights.calculateCalories === false
                    ? "results disabled"
                    : "results"}>
                    <div className="result-item">
                        <div className="result-item__title">Снижение веса</div>
                        <div className="result-item__value">{commonStore.dashboard.calories.reduction}</div>
                    </div>
                    <div className="result-item">
                        <div className="result-item__title">Поддержание веса</div>
                        <div className="result-item__value">{commonStore.dashboard.calories.maintenance}</div>
                    </div>
                </div>
                <ButtonWithHint
                    onOpenPopupCalorieCalculation={onOpenPopupCalorieCalculation}
                    onClosePopupCalorieCalculation={onClosePopupCalorieCalculation}
                    isCalculationError={isCalculationError}
                    className='btn'
                />
            </StyledCard>
        </StyledCalculateCard>
    )
});

