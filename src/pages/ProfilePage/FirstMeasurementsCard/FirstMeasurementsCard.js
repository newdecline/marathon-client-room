import React from 'react';
import {observer} from 'mobx-react';
import {StyledFirstMeasurementsCard} from "./styled";
import commonStore from "../../../mobx/commonStore";
import {EditMeasurementsPopup} from "../../../components/layouts/MainLayout/OverlayWithPopups/EditMeasurementsPopup/EditMeasurementsPopup";
import {ViewWithFirstMeasurements} from "../ViewWithFirstMeasurements/ViewWithFirstMeasurements";
import {StyledCard} from "../../../components/atoms/StyledCard";

export const FirstMeasurementsCard = observer(() => {

    const hasFirstResults = commonStore.dashboard.results.length > 0;
    let firstData = {};
    if (hasFirstResults) {
        firstData = commonStore.dashboard.results[0];
    }

    return (
        <StyledFirstMeasurementsCard>
            <StyledCard className='first-measurements-card'>
                {hasFirstResults
                    ? <ViewWithFirstMeasurements hasStress={firstData.has_stress}/>
                    : <EditMeasurementsPopup
                        className='first-measurements'
                        type='add-measurements'/>}
            </StyledCard>
        </StyledFirstMeasurementsCard>
    )
});





