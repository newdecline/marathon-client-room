import styled from "styled-components/macro";

export const StyledFirstMeasurementsCard = styled('div')`
    grid-area: first-measurements;
    .first-measurements-card {
        height: 100%;
    }
    .header {
        padding: 23px 20px 25px 22px;
        display: flex;
        justify-content: space-between;
        align-items: baseline;
        border-bottom: 1px solid #e5e5e5;
        &__title {
            font-family: 'Bodoni';
            font-size: 30px;
        }
        &__edit {
            display: flex;
            padding: 0;
            background-color: transparent;
            border: none;
            &:active, &:focus {
                outline: none;
            }
            &-text {
                display: none;
            }
        }
    }
    .body {
        padding: 15px 17px 24px 22px;
        box-sizing: border-box;
        &-measurements {
            display: flex;
            flex-direction: column;
            margin-bottom: 16px;
            &-item {
                display: flex;
                align-items: center;
                &:nth-child(1), &:nth-child(2), &:nth-child(3) {
                    .field-wrap {
                        border-bottom: 0;
                    }
                } 
                &.error {
                    .body-measurements__label, .measure {
                        color: #D96868;
                    }
                }
            }
            &__label {
                flex-basis: 44%;
                font-size: 20px;
                line-height: 120%;
            }
        }
    }
    .footer {
        padding: 0 17px 18px 22px;
        &__checkbox-wrap {
            display: flex;
            align-items: center;
            margin-bottom: 20px;
        }
        .text {
            font-size: 20px;
            line-height: 150%;
        }
        .hint {
            display: inline-block;
            margin-left: auto;
        }
        .btn {
            display: block;
            margin-left:auto;
            &.error {
                background: #D96868;
            }
        }
    }
    .first-measurements {
        position: static;
        .inner {
            width: 100%;
        }
        .close-btn {
            display: none;
        }
    }
    @media (min-width: 1024px) {
        .header {
            padding: 29px 35px 27px 35px;
            align-items: center;
            &__title {
                font-size: 46px;
            }
            &__edit {
                &:hover {
                    cursor: pointer; 
                }
                &-text {
                    display: block;
                    font-size: 14px;
                    line-height: 120%;
                    font-family: "Circe-ExtraBold";
                    text-transform: uppercase;
                }
                &-icon {
                    display: none;
                }
            }
        }
        .body {
            padding: 23px 35px 34px 36px;
            display: flex;
            justify-content: space-between;
            &-measurements {
                margin-right: 22px;
                margin-bottom: 0;
                flex: 1;
                &__label {
                    flex-basis: 127px;
                    margin-right: auto;
                }
            }
        }
        .field-wrap {
            width: 164px;
            flex-basis: unset;
        }
        .footer {
            position: relative;
            display: flex;
            padding: 0 35px 34px 36px;
            &__checkbox-wrap {
                display: inline-flex;
                margin-bottom: 0;
            }
            .btn {
                align-self: center;
            }
            .text {
                margin-right: 8px;
            }
        }
    } 
`;