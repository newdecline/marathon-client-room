import React from "react";
import {observer} from 'mobx-react';
import commonStore from "../../../mobx/commonStore";
import {UploadImages} from "../../../components/UploadImages/UploadImages";
import {ReactComponent as AddFileIcon} from "../../../svg/addFile.svg";
import {StyledPhotoReportCard} from "./styled";
import {StyledCard} from "../../../components/atoms/StyledCard";
import range from "lodash/range";
import {StyledHeaderCard} from "../../../components/atoms/StyledHeaderCard";
import {popupStore} from "../../../mobx/popupStore";

const IMAGE_TITLES = [
    'Фото спереди',
    'Фото сбоку',
    'Фото со спины',
];
const BEFORE_IMAGES = range(1, 4).map(i => [`beforeImage${i}`, `before_result_${i}`]);
const AFTER_IMAGES = range(1, 4).map(i => [`afterImage${i}`, `after_result_${i}`]);

export const PhotoReportCard = observer(({images, archive}) => {

    const handleClickImage = (imageUrl) => {
        popupStore.setOpenPopup({
            type: 'photo-viewer',
            style: {
                position: 'fixed',
                top: 0,
                left: 0,
            },
            imageUrl: imageUrl
        });
    };

    const renderImageUploader = ([key, type], i) => (
        <UploadImages
            key={i}
            image={images[key]}
            type={type}
            title={IMAGE_TITLES[i]}
            subtitle={`Перетащите фото или кликните`}
            icon={<AddFileIcon/>}
            handlerClick={handleClickImage}
            archive={archive}
        />
    );

    const disabled = !archive && !commonStore.dashboard.accessRights.editBeforeAndAfterImages;

    return (
        <StyledPhotoReportCard>
            <StyledCard className='photo-report-card'>
                <StyledHeaderCard border={false} className='header-card'>
                    <div className={disabled
                        ? "title disabled"
                        : "title"}>Фотоотчет</div>
                </StyledHeaderCard>

                <div className={disabled
                    ? "upload-images-before disabled"
                    : "upload-images-before"}>
                    <div className="subtitle-card">Загрузите свои фотографии “до”</div>
                    <div className="upload-images-wrap">
                        {BEFORE_IMAGES.map(renderImageUploader)}
                    </div>
                </div>

                <div className={disabled
                    ? "upload-images-after disabled"
                    : "upload-images-after"}>
                    <div className="subtitle-card">{`Загрузите свои фотографии “после”`}</div>
                    <div className="upload-images-wrap">
                        {AFTER_IMAGES.map(renderImageUploader)}
                    </div>
                </div>
            </StyledCard>
        </StyledPhotoReportCard>
    )
});