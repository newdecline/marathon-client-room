import styled from "styled-components/macro";

export const StyledPhotoReportCard = styled('div')`
    grid-area: photo-report;
    .subtitle-card {
        margin-bottom: 16px;
        text-transform: uppercase;
        font-size: 14px;
        line-height: 120%;
        font-family: 'Circe-ExtraBold';
        white-space: pre-wrap;
    }
    .upload-images-title {
        margin-bottom: 5px;
        font-size: 14px;
        line-height: 120%;
        font-family: "Circe-ExtraBold";
        text-transform: uppercase;
    }
    .upload-images-subtitle {
        font-size: 14px;
        line-height: 120%;
    }
    .upload-images-before {
        margin: 0 22px 14px 20px;
        box-sizing: border-box;
    }
    .upload-images-after {
        margin: 0 22px 26px 20px;
        box-sizing: border-box;
    }
    .upload-images-wrap {
        display: flex;
        flex-wrap: wrap;
    }
    .upload-images {
        width: 100%;
        margin-bottom: 11px;
    }
    .upload-images.uploaded {
        flex: 1;
        min-width: 32.333%;
        padding-right: 11px;
        box-sizing: border-box;
        &:last-child {
            margin-right: 0;
            padding-right: 0;
        }
    }
    .empty {
        padding: 16px 31px 15px 31px;
        svg {
            margin-right: 34px;
        }
    }
    .empty-svg-desktop {
        display: none;
    }
    @media (min-width: 1024px) {
        .photo-report-card {
            margin-bottom: 40px;
            display: flex;
            flex-wrap: wrap;
        }
        .header-card {
            width: 100%;
        }
        .subtitle-card {
            margin-bottom: 21px;
        }
        .upload-images {
            width: 130px;
            margin-right: 17px;
            margin-bottom: 0;
            &:last-of-type {
                margin-right: 0;
            }
        }
        .upload-images.uploaded {
            width: 30.6%;
            flex: unset;
            min-width: unset;
            padding-right: 0;
            margin-right: 12px;
        }
        .upload-images-before {
            flex: 1;
            margin: 0 0 38px 35px;
        }
        .upload-images-after {
            flex: 1;
            margin: 0 25px 38px 0;
        }
        .upload-images-title {
            padding: 0 21px;
            margin-bottom: 12px;
            text-align: center;
        }
        .upload-images-subtitle {
            text-align: center;
        }
        .empty {
            padding: 13px 0 15px 0;
            svg {
                margin-right: 0;
                margin-bottom: 10px;
            }
            &-svg-mobile {
                display: none;
            }
            &-svg-desktop {
                display: flex;
                justify-content: center;
            }
        }
        .thumb {
            height: 291px;
            &:hover {
                cursor: pointer;
            }
        }
    }
    @media (min-width: 1260px) {
        .subtitle-card {
            margin-bottom: 21px;
        }
        .upload-images {
            width: unset;
        }
        .upload-images-wrap {
            flex-wrap: unset;
        }
        .upload-images-title {
            padding: 0 5px;
        }
        .upload-images-title {
            margin-bottom: 42px;
        }
        .empty {
            padding: 25px 0 15px 0;
            width: 170px;
            svg {
                margin-right: 0;
                margin-bottom: 27px;
            }
            &-svg-mobile {
                display: none;
            }
            &-svg-desktop {
                display: flex;
                justify-content: center;
            }
        }
        .thumb {
            height: 291px;
        }
    }
`;