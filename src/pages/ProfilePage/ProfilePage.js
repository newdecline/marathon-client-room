import React from 'react';
import {observer} from 'mobx-react';
import {FirstMeasurementsCard} from "./FirstMeasurementsCard/FirstMeasurementsCard";
import {StyledProfilePage} from "./styled";
import commonStore from "../../mobx/commonStore";
import {MyResultsCard} from "./MyResultsCard/MyResultsCard";
import {MyProfileCard} from "./MyProfileCard/MyProfileCard";
import {CalculateCard} from "./CalculateCard/CalculateCard";
import {PhotoReportCard} from "./PhotoReportCard/PhotoReportCard";

export const ProfilePage = observer(() => {
    return (
        <StyledProfilePage>
            <MyProfileCard/>
            <FirstMeasurementsCard/>
            <CalculateCard/>
            {commonStore.dashboard.results.length > 0 && <MyResultsCard marathonResults={commonStore.dashboard.results} />}
            <PhotoReportCard images={commonStore.dashboard.images} />
            {commonStore.dashboard.archive.length > 0 && (
                <div className="archive">
                    <div className="archive__header">
                        Архив предыдущих марафонов
                    </div>
                    {commonStore.dashboard.archive.map(({results, images}, key) => (
                        <div key={key} className="archive__item">
                            {results.length > 0 && <MyResultsCard marathonResults={results} />}
                            <PhotoReportCard images={images} archive />
                        </div>
                    ))}
                </div>
            )}
        </StyledProfilePage>
    )
});

