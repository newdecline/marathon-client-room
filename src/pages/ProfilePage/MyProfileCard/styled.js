import styled from "styled-components/macro";

export const StyledMyProfileCard = styled('div')`
    grid-area: my-profile;
    .my-profile-card {
        height: 100%;
    }
    .greeting-notice {
        position: relative;
        padding: 13px 44px 9px 60px;
        margin: 0 0 12px 0;
        .icon {
            left: 30px;
        }
    }
    .body {
        padding: 34px 20px 25px 26px;
        &-item {
            margin-bottom: 24px;
            font-size: 20px;
            line-height: 120%;
            &:last-child {
                margin-bottom: 0;
            }
            &__label {
                display: inline-block;
                width: 38%;
            }
            &__label-value {
                display: inline-block;
                width: 62%;  
            }
        }
    }
    @media (min-width: 1024px) {
        .greeting-notice {
            display: none;
        }
        .header-card {
            .title {
                font-size: 30px;
            }
        }
        .body {
            padding: 32px 20px 25px 26px;
            .body-item {
                margin-bottom: 26px;
                &:last-child {
                    margin-bottom: 13px;
                }
            }
            &-item__label {
                width: 38%;
            }
            &-item__value {
                width: 62%;
            }
        }
    }
    @media (min-width: 1260px) {
        .header-card {
            .title {
                font-size: 46px;
            }
        }
        .body {
            padding: 36px 20px 25px 35px;
            .body-item {
                margin-bottom: 26px;
                &:last-child {
                    margin-bottom: 13px;
                }
            }
            &-item__label {
                width: 26%;
            }
            &-item__value {
                width: 74%;
            }
        }
    }
`;