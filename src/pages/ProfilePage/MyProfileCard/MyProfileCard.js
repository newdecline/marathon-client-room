import React from "react";
import {observer} from "mobx-react";
import {ReactComponent as PenIcon} from "../../../svg/pen.svg";
import {popupStore} from "../../../mobx/popupStore";
import commonStore from "../../../mobx/commonStore";
import {StyledMyProfileCard} from "./styled";
import {StyledCard} from "../../../components/atoms/StyledCard";
import {Notice} from "../../IndexPage/GreetingCard/Notice/Notice";
import {StyledHeaderCard} from "../../../components/atoms/StyledHeaderCard";

export const MyProfileCard = observer(() => {
    const profileData = [
        {
            label: 'Имя',
            value: commonStore.dashboard.profile.name,
        },
        {
            label: 'Фамилия',
            value: commonStore.dashboard.profile.surname,
        },
        {
            label: 'Телефон',
            value: commonStore.dashboard.profile.phone,
        },
        {
            label: 'E-mail',
            value: commonStore.dashboard.profile.email,
        },
        {
            label: 'Возраст',
            value: commonStore.dashboard.profile.age,
        },
        {
            label: 'Рост',
            value: commonStore.dashboard.profile.height,
        },
    ];

    const handleClickEditProfileBtn = () => {
        popupStore.setOpenPopup({
            type: 'edit-profile',
            style: {top: 'unset'}
        })
    };

    const renderNotice = () => commonStore.dashboard.notification && (
        <Notice {...commonStore.dashboard.notification} />
    );

    return (
        <StyledMyProfileCard>
            {renderNotice()}
            <StyledCard className='my-profile-card'>
                <StyledHeaderCard className='header-card'>
                    <div className="title">Мой профиль</div>
                    {commonStore.dashboard.accessRights.editProfile && (
                        <button
                            onClick={handleClickEditProfileBtn}
                            type='button'
                            className="edit-btn">
                            <span className="edit-btn__text">Редактировать</span><PenIcon/>
                        </button>
                    )}
                </StyledHeaderCard>
                <div className="body">
                    {
                        profileData.map(item => {
                            return (
                                <div key={item.label} className="body-item">
                                    <span className='body-item__label'>{item.label}</span>
                                    <span className='body-item__label-value'>{item.value}</span>
                                </div>
                            )
                        })
                    }
                </div>
            </StyledCard>
        </StyledMyProfileCard>
    )
});