import styled from "styled-components/macro";

export const StyledInputFieldGroup = styled('div')`
    label:not(:last-of-type) {
        input {
            border-bottom: none;
        }
    }
`;