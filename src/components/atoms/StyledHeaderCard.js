import styled from 'styled-components/macro';

export const StyledHeaderCard = styled('div')`
    padding: 23px 20px 25px 22px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-bottom: ${({border = true}) => !border ? 'none' : '1px solid #e5e5e5'};
    .title {
        font-family: 'Bodoni';
        font-size: 30px;
        text-transform: unset;
    }
    .close-btn {
        display: flex;
        border: 0;
        background-color: transparent;
        padding: 0;
        margin: 0;
        outline: none;
    }
    .edit-btn {
        display: flex;
        border: 0;
        background-color: transparent;
        padding: 0;
        margin: 0;
        outline: none;
        &__text {
            display: none;
        }
    }
    @media (min-width: 1024px) {
        padding: 29px 35px 27px 35px;
        .title {
            font-size: 46px;
        }
        .close-btn, .edit-btn {
            &:hover {
                cursor: pointer;
            }
        }
    }
    @media (min-width: 1260px) {
        .edit-btn {
            &__text {
                padding-top: 6px;
                display: block;
                font-size: 14px;
                line-height: 120%;
                font-family: "Circe-ExtraBold";
                text-transform: uppercase;
            }
            svg {
                display: none;
            }
        }
    }
`;