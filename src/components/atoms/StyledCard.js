import styled from "styled-components/macro";

export const StyledCard = styled('div')`
    border-radius: 5px;
    background: #fff;
    box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.15);
`;