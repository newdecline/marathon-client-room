import styled from "styled-components/macro";

export const StyledPreloader = styled('div')`
    .react-spinner-material {
        margin: auto;
    }
`;