import React from "react";
import {StyledPreloader} from "./styled";
import Spinner from 'react-spinner-material';

export const Preloader = () => {

    return (
        <StyledPreloader>
            <Spinner size={60} spinnerColor={"#9AC190"} spinnerWidth={4} visible={true} />
        </StyledPreloader>
    )
};