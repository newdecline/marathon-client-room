import styled, {keyframes} from "styled-components/macro";

const translation = keyframes`
    0% {
        left: -70%;
        transform: scale(0.5, 1);
    }
    
    90% {
        transform: scale(7, 1);
    }
    
    100% {
        left: 200%;
        transform: scale(0.5, 1);
    }
`;

export const StyledButton = styled('button')`
    position: relative;
    line-height: 1;
    text-transform: uppercase;
    outline: none;
    overflow: hidden;
    padding: 0 12px;
    cursor: pointer;
    
    height: ${({variant}) => variant === 'default' ? '40px' : '48px'};
    @media (min-width: 1024px) {
        height: ${({variant}) => variant === 'default' ? '40px' : '60px'};
    }
    min-width: ${({variant}) => variant === 'default' ? '126px' : '270px'};
    background: ${({outlined, error}) => outlined ? 'transparent' : (error ? '#d96868' : '#454545')};
    color: ${({outlined, error}) => outlined ? (error ? '#d96868' : '#454545') : 'white'};
    border: ${({variant, outlined, error}) => outlined ? `2px solid ${error ? '#d96868' : '#454545'}` : 'none'};
    font-size: ${({variant}) => variant === 'default' ? '14px' : '20px'};
    border-radius: ${({variant}) => variant === 'default' ? '5px' : '0'};
    letter-spacing: ${({variant}) => variant === 'default' ? 'unset' : '.4em'};
    font-family: ${({variant}) => variant === 'default' ? 'Circe-ExtraBold' : 'Circe-Regular'};
    font-weight: ${({outlined}) => outlined ? 'bold' : 'unset'};
    width: ${({fullWidth}) => fullWidth ? '100%' : 'unset'};
    
    &[disabled] {
        opacity: .4;
        cursor: default !important;
    }
    .btn-text {
        display: block;
        position: relative;
        z-index: 1;
        padding-top: ${({variant}) => variant === 'default' ? '2px' : '4px'};
        padding-left: ${({variant}) => variant === 'default' ? '0' : '9px'};
    }
    .progress-container {
        position:absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        display: block;
        background: inherit;
        &__active {
            position:absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 20%;
            display: block;
            background: ${({outlined}) => outlined ? 'transparent' : 'black'};
            animation: ${translation} 1s ease-in infinite;
        }
    }
`;