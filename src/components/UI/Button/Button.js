import React from 'react';
import {StyledButton} from "./styled";
import commonStore from "../../../mobx/commonStore";
import PropTypes from "prop-types";
import {observer} from 'mobx-react';

export const Button = observer(props => {
    const {
        label,
        variant,
        outlined,
        loadingLabel,
        disabled,
        error,
        loading,
        type,
        onClick,
        className,
        fullWidth,
    } = props;

    return (
        <StyledButton
            variant={variant}
            outlined={outlined}
            disabled={disabled || commonStore.loading}
            error={error}
            type={type}
            onClick={onClick}
            className={className}
            fullWidth={fullWidth}
        >
            <span className="btn-text">
                {loading ? (loadingLabel || label) : label}
            </span>
            {loading && (
                <span className="progress-container">
                    <span className="progress-container__active"/>
                </span>
            )}
        </StyledButton>
    )
});

Button.propTypes = {
    label: PropTypes.string.isRequired,
    variant: PropTypes.string,
    outlined: PropTypes.bool,
    loadingLabel: PropTypes.string,
    disabled: PropTypes.bool,
    error: PropTypes.bool,
    loading: PropTypes.bool,
    type: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
    fullWidth: PropTypes.bool,
};

Button.defaultProps = {
    variant: 'default',
};
