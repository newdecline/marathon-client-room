import React from "react";
import {string, object} from "prop-types";
import {StyledInputField} from "./styled";
import {isEmptyObj} from "../../../utilities/isEmptyObj";
import commonStore from "../../../mobx/commonStore";
import {observer} from 'mobx-react';

export const InputField = observer(props => {
    const {
        text,
        className,
        inputLabel,
        inputProps,
        error
    } = props;

    return <StyledInputField className={className} error={!isEmptyObj(error)}>
        <span className="text">{text}</span>
        <span className="input-wrap">
            <input className="input-wrap__input" {...inputProps} disabled={inputProps.disabled || commonStore.loading} />
            {inputLabel && <span className="input-wrap__label">{inputLabel}</span>}
        </span>
    </StyledInputField>
});

InputField.defaultProps = {
    text: null,
    className: null,
    inputProps: {},
    inputLabel: null,
    error: {}
};

InputField.propTypes = {
    text: string,
    className: string,
    inputProps: object,
    inputLabel: string,
    error: object
};