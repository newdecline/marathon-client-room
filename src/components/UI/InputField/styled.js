import styled from 'styled-components/macro';

export const StyledInputField = styled('label')`
    display: flex;
    align-items: center;
    .text {
        margin-right: 5px;
        width: 127px;
        font-size: 20px;
        line-height: 120%;
        color: ${({error}) => error ? '#D96868' : '#000'}
    }
    .input-wrap {
        flex: 1;
        position: relative;
        &__input {
            outline: none;
            padding: 11px 32px 11px 15px;
            width: 100%;
            border: 1px solid #c3c3c3;
            font-size: 20px;
            line-height: 120%;
            box-sizing: border-box;
            &[disabled] {
                background-color: unset;
            }
        }
        &__label {
            position: absolute;
            top: 12px;
            right: 10px;
            font-size: 20px;
            line-height: 120%;
            color: ${({error}) => error ? '#D96868' : '#c3c3c3'};
        }
    }
    @media (min-width: 1024px) {
        
    }
`;