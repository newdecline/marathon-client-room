import React, {useEffect} from 'react';
import {observer} from 'mobx-react';
import {ReactComponent as AvatarPosterIcon} from '../../svg/avatar-poster.svg';
import {Link} from 'react-router-dom';
import commonStore from '../../mobx/commonStore';
import {StyledProfilePopup} from "./styled";

export const ProfilePopup = observer(() => {
    const togglePopup = (isOpen) => {
        commonStore.setOpenProfilePopup(isOpen);
    };

    const handlerClickBody = (e) => {
        if (commonStore.isOpenProfilePopup) {
            if (!e.target.closest(".profile-popup")) {
                togglePopup(false);
            }
        }
    };

    const handleClickLogoutBtn = (e) => {
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
        togglePopup(false);

        commonStore.logout();
    };

    useEffect(() => {
        document.body.addEventListener('click', handlerClickBody);
        return () => {
            document.body.removeEventListener('click', handlerClickBody);
        }
    }, []);

    return (
        <StyledProfilePopup
            isOpen={commonStore.isOpenProfilePopup}
            className='profile-popup'>
            <div className='label' onClick={() => togglePopup()}>
                <span className="name">{commonStore.fullName}</span>
                {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                <span className='label__avatar'><AvatarPosterIcon/></span>
            </div>
            <div className="label-expand">
                <div className="label-expand__name">{commonStore.fullName}</div>
                <a className="label-expand__email">{commonStore.dashboard.profile.email}</a>
                <div className="label-expand-footer">
                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                    <Link onClick={() => togglePopup(false)} to='/profile' className='label-expand-footer__profile'>Профиль</Link>
                    <button
                        disabled={commonStore.dashboard.role === 'admin'}
                        onClick={handleClickLogoutBtn}
                        className='label-expand-footer__logout'
                    >
                        Выйти
                    </button>
                </div>
            </div>
        </StyledProfilePopup>
    )
});

