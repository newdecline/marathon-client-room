import styled from "styled-components/macro";

export const StyledProfilePopup = styled('div')`
    position: relative;
    z-index: 2;
    .label {
        display: flex;
        align-items: center;
        z-index: 1;
        cursor: pointer;
        .name {
            display: none;
        }
        &__avatar {
            display: flex;
            position: relative;
            z-index: 2;
        }
        
    }
    .label-expand {
        position: absolute;
        top: -22px;
        right: -36px;
        padding: 28px 36px 18px 31px;
        display: flex;
        flex-direction: column;
        background-color: #454545;
        border-radius: 5px;
        box-sizing: border-box;
        z-index: 1;
        opacity: ${({isOpen}) => isOpen ? 1 : 0};
        transform: ${({isOpen}) => isOpen ? 'scale3d(1, 1, 1)' : 'scale3d(1, 0, 1)'};
        transform-origin: top right;
        transition: opacity .3s, transform .3s;
        &__name {
            margin-bottom: 8px;
            font-size: 18px;
            line-height: 120%;
            text-transform: uppercase;
            white-space: nowrap;
        }
        &__email {
            margin-bottom: 38px;
            font-size: 14px;
            line-height: 120%;
            text-decoration: none;
            color: #fff;
        }
    }
    .label-expand-footer {
        display: flex;
        justify-content: space-between;
        &__profile, &__logout {
            font-size: 18px;
            line-height: 120%;
            font-family: "Circe-ExtraBold";
            text-decoration: none;
            color: #fff;
            text-transform: uppercase;
            padding: 0;
            margin: 0;
            border: none;
            background-color: transparent;
        }
        &__profile {
           margin-right: 47px; 
        }
        button[disabled] {
            opacity: .5;
        }
    }

    @media (min-width: 1024px) {
        margin-left: unset;
        &::after {
            display: none;
        }
        .label {
            padding: 0 36px 0 37px;
            background: ${({isOpen}) => isOpen ? '#454545' : 'transparent'};
            border-radius: 5px 5px 0 0;
            transition: background-color .3s;
            &__avatar {
                &:hover {
                    cursor: pointer;
                }
            }
            .name {
                min-width: 160px;
                transform: translate(0, 3px);
                margin-right: 25px;
                display: unset;
                font-size: 18px;
                line-height: 120%;
                font-family: "Circe-Regular";
                text-transform: uppercase;
                z-index: 2;
            }
        }
        .label-expand {
            padding: 60px 36px 18px 37px;
            width: 100%;
            top: -25px;
            right: unset;
            left: 0;
            border-radius: 5px;
            transform-origin: center top;
            &__name {
                display: none;
            }
        }
        .label-expand-footer {
            &__logout {
                &:hover {
                    cursor: pointer;
                }
            }
        }
    }
`;