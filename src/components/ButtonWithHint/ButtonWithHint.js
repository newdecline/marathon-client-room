import React from 'react';
import {string, func, bool} from 'prop-types';
import {observer} from 'mobx-react';
import {StyledButtonWithHint} from "./styled";
import {ReactComponent as CrossIcon} from "../../svg/cross-rotated.svg";
import {Button} from "../UI/Button/Button";
import commonStore from "../../mobx/commonStore";

export const ButtonWithHint = observer((
    {
        onOpenPopupCalorieCalculation,
        onClosePopupCalorieCalculation,
        isCalculationError,
        className
    }) => {

    const errorMessage = commonStore.dashboard.accessRights.calculateCalories === 'first_results_denied'
        ? 'Ваши первые замеры отклонены. Отредактируйте их, и вы сможете рассчитать норму калорий.'
        : 'Для расчета нормы калорий сначала заполните в профиле рост, возраст и введите первые замеры.';

    return (
        <StyledButtonWithHint
            className={className}
            isCalculationError={isCalculationError}>
            <div className="hint">
                {errorMessage}
                <button onClick={onClosePopupCalorieCalculation} className="close-hint"><CrossIcon/></button>
            </div>
            <Button
                type='button'
                label='рассчитать'
                disabled={commonStore.dashboard.accessRights.calculateCalories === false}
                className='btn'
                onClick={onOpenPopupCalorieCalculation}
            />
        </StyledButtonWithHint>
    )
});

ButtonWithHint.propTypes = {
    text: string,
    onOpenPopupCalorieCalculation: func,
    onClosePopupCalorieCalculation: func,
    isCalculationError: bool,
    className: string
};