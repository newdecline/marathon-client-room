import styled from 'styled-components/macro';

export const StyledButtonWithHint = styled('div')`
    position: relative;
    .hint {
        display: ${({isCalculationError}) => isCalculationError ? 'block' : 'none'};
        position: absolute;
        bottom: 60px;
        right: 0;
        width: 316px;
        padding: 30px 45px 20px 25px;
        background: #D96868;
        box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.15);
        border-radius: 5px;
        font-size: 20px;
        line-height: 120%;
        color: #fff;
        text-transform: none;
        box-sizing: border-box;
        white-space: pre-wrap;
        z-index: 1;
    }
    .close-hint {
        display: flex;
        position: absolute;
        top: 15px;
        right: 15px;
        padding: 0;
        margin: 0;
        border: none;
        background-color: transparent;
        rect {
            fill: #fff;
        }
    }
    .btn {
        background: ${({isCalculationError}) => isCalculationError ? '#d96868' : '#454545'};
    }
    @media (min-width: 1024px) {
        button {
            &:hover {
                cursor: pointer;
            }
        }
    }
`;