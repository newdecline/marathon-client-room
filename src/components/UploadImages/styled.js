import styled, {keyframes} from "styled-components/macro";

const translation = keyframes`
    0% {
        left: -70%;
        transform: scale(0.5, 1);
    }
    
    90% {
        transform: scale(7, 1);
    }
    
    100% {
        left: 200%;
        transform: scale(0.5, 1);
    }
`;

export const StyledUploadImages = styled('div')`
    .wrap-empty {
        outline: none;
    }
    .loading-text {
        margin-top: 15px;
        text-align: center;
    }
    .empty {
        padding: 22px 16px 25px 23px;
        display: flex;
        align-items: center;
        justify-content: center;
        border: ${({error}) => error ? '1px dashed #D96868' : '1px dashed #C3C3C3'};
        box-sizing: border-box;
        background-color: ${({isDragActive}) => isDragActive ? 'rgba(0, 0, 0, 0.1)' : 'transparent'};
        transition: background-color .3s;
        svg {
            margin-right: 22px;
        }
        &.loading {
            padding: 0;
            height: 90px;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
    }
    .preview {
        display: flex;
        flex-direction: row;
        box-sizing: border-box;
        &.loading {
            flex-direction: column;
            justify-content: center;
            align-items: center;
            width: 100%;
            height: 90px;
            border: 1px dashed #C3C3C3;
            .container-loading__active {
                background: #757575;
            }
        }
    }
    .thumb {
        position: relative;
        display: inline-flex;
        max-width: 164px;
        width: 100%;
        height: 199px;
        box-sizing: border-box;
        overflow: hidden;
        img {
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            width:auto;
            height: 100%;
            object-fit: cover;
        }
    }
    .subtitle, .title {
        white-space: pre-wrap;
    }
    .subtitle {
        color: ${({error}) => error ? '#D96868' : '#454545'};
    }
    .trash {
        position: absolute;
        top: 15px;
        right: 15px;
        background-color: transparent;
        padding: 0;
        margin: 0;
        border: none;
        z-index: 1;
        outline: none;
    }
    .empty-svg-mobile {
        rect {
            fill: ${({error}) => error ? '#D96868' : '#454545'};
        }
        circle {
            stroke: ${({error}) => error ? '#D96868' : '#454545'};
        }
    }
    .container-loading {
        position: relative;
        width: 72%;
        height: 4px;
        background-color: #c3c3c3;
        overflow: hidden;
        &__active {
            position:absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 20%;
            display: block;
            background: #9ac190;
            animation: ${translation} 1s ease-in infinite;
        }
    }
    @media (min-width: 1024px) {
        .empty {
            padding: 0;
            &.loading {
                height: 152px;
            }
        }
        .preview {
            &.loading {
                height: 152px;
            }
        }
    }
    @media (min-width: 1260px) {
        .empty {
            &:hover {
                cursor: pointer;
            }
            &.loading {
                height: 199px;
                width: 170px;
            }
        }
        .preview {
            &.loading {
                height: 199px;
                width: 170px;
            }
        }
        .trash {
            &:hover {
                cursor: pointer;
            }
        }
    }
`;