import React, {forwardRef, useState} from 'react';
import {string, object, func, any} from 'prop-types';
import {useDropzone} from 'react-dropzone';
import {ReactComponent as TrashIcon} from '../../svg/trash.svg';
import {StyledUploadImages} from "./styled";
import apiService from "../../apiService";
import commonStore from "../../mobx/commonStore";
import notifierStore from "../../mobx/notifierStore";

export const UploadImages = forwardRef((props, ref) => {

    const {
        title,
        subtitle,
        icon,
        handlerClick,
        image,
        type,
        archive,
    } = props;

    const cls = {
        'upload-images': ['upload-images']
    };

    const [loading, setLoading] = useState([false, '']);

    const handleClickTrashBtn = async () => {
        setLoading([true, 'deleteImage']);
        const response = await apiService.deleteBeforeOrAfterImage(type);
        setLoading([false, '']);

        if (response.status === 200) {
            notifierStore.enqueueSuccess('Фотография удалена');
        }
    };

    const onUpdateFile = async file => {
        setLoading([true, 'uploadImage']);
        const response = await apiService.createImage(file);
        setLoading([false, '']);

        if (response.status === 200) {
            setLoading([true, 'uploadImage']);
            const resp = await apiService.createBeforeOrAfterImage(type, response.body.id);
            setLoading([false, '']);

            if (resp.status === 200) {
                notifierStore.enqueueSuccess('Фотография добавлена');
            }
        }
    };

    const {getRootProps, getInputProps, isDragActive} = useDropzone({
        accept: 'image/*',
        onDrop: acceptedFiles => {
            onUpdateFile(acceptedFiles[0]);
        }
    });

    if (image) {
        cls['upload-images'].push('uploaded');
    }

    return (
        <StyledUploadImages
            isDragActive={isDragActive}
            className={cls['upload-images'].join(' ')}>
            {!image
                ? <div {...getRootProps()} className='wrap-empty'>
                    <label>
                        <input {...getInputProps({ref, disabled: loading[0] || !commonStore.dashboard.accessRights.editBeforeAndAfterImages || archive})} />
                        <div className={loading[0] ? 'empty loading' : 'empty'}>
                            {loading[0]
                                ? <>
                                    <div className="container-loading">
                                        <span className='container-loading__active'/>
                                    </div>
                                    <div className="loading-text">Изображение<br/>загружается</div>
                                </>
                                : <>
                                    <div className="empty-svg-mobile">{icon}</div>
                                    <div>
                                        <div className="upload-images-title">{title}</div>
                                        <div className="empty-svg-desktop">{icon}</div>
                                        <div className="upload-images-subtitle">{subtitle}</div>
                                    </div>
                                </>}

                        </div>
                    </label>
                </div>

                : <div className={loading[0] ? 'preview loading' : 'preview'}>
                    {
                        loading[0]
                            ? <>
                                <div className="container-loading">
                                    <span className='container-loading__active'/>
                                </div>
                                <div className="loading-text">Изображение<br/>удаляется</div>
                            </>
                            : <div className='thumb'>
                                {!archive && commonStore.dashboard.accessRights.editBeforeAndAfterImages && (
                                    <button
                                        disabled={loading[0]}
                                        onClick={handleClickTrashBtn}
                                        className='trash'><TrashIcon/>
                                    </button>
                                )}
                                <img src={image.url} alt='/' onClick={() => handlerClick(image.url)}/>
                            </div>
                    }
                </div>}
        </StyledUploadImages>
    )
});


UploadImages.propTypes = {
    title: string,
    subtitle: string,
    icon: object,
    onUpdateFile: func,
    error: object,
    inputName: string,
    image: any,
    type: any
};