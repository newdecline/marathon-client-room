const navbar = [
    {
        route: '/rules',
        label: 'Правила',
    },
    {
        route: '/food',
        label: 'Питание',
    },
    {
        route: '/recipes',
        label: 'Рецепты',
    },
    {
        route: '/trainings',
        label: 'Тренировки',
    },
];

export default navbar;