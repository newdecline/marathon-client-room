import React, {useEffect} from 'react';
import {observer} from 'mobx-react';
import {StyledBurgerMenu, StyledHeader, StyledMobileMenu, StyledTopMenu} from "./styled";
import {ProfilePopup} from "../ProfilePopup/ProfilePopup";
import {CSSTransition} from "react-transition-group";
import {ReactComponent as HomeIcon} from "../../svg/home.svg";
import {Link, NavLink} from "react-router-dom";
import commonStore from "../../mobx/commonStore";
import {disablePageScroll, enablePageScroll} from "scroll-lock";
import NavBar from "./NavBar/NavBar";
import navbar from './navbar';
import routes from  '../../routes';
import classnames from 'classnames';
import get from 'lodash/get';

// TODO Рефакторинг: меню закрывается при клике на задизейбленную ссылку
export const Header = observer(() => {
    useEffect(() => {
        if (commonStore.isOpenMenu) {
            disablePageScroll();
        } else {
            enablePageScroll();
        }
    }, [commonStore.isOpenMenu]);

    const toggleMenu = isOpen => {
        commonStore.setOpenMenu(isOpen);
    };

    return (
        <StyledHeader>
            <StyledTopMenu>
                <div className="container">
                    <StyledBurgerMenu
                        isOpenMenu={commonStore.isOpenMenu}
                        onClick={() => toggleMenu()}>
                        <span className="item item-1"/>
                        <span className="item item-2"/>
                        <span className="item item-3"/>
                    </StyledBurgerMenu>
                    <a
                        href='/'
                        rel='noreferrer noopener'
                        className='return-to-website'>Вернуться на сайт</a>

                    <ProfilePopup/>
                </div>
            </StyledTopMenu>
            <NavBar/>
            <StyledMobileMenu>
                <CSSTransition
                    in={commonStore.isOpenMenu}
                    timeout={300}
                    classNames="mobile-menu"
                    unmountOnExit
                >
                    <div className="overlay">
                        <div className="inner">
                            <ul className="menu-list">
                                <li
                                    onClick={() => toggleMenu(false)} className="menu-list__item">
                                    <Link to={'/'} className='link'><HomeIcon/></Link>
                                </li>
                                {navbar.map(({route, label}) => (
                                    <li
                                        key={route}
                                        onClick={() => toggleMenu(false)} className="menu-list__item">
                                        <NavLink
                                            to={route}
                                            className={
                                                classnames('link', {
                                                    disabled: !get(commonStore.dashboard.accessRights, routes.find(({path}) => path === route).accessKey)
                                                })
                                            }
                                            activeClassName="active"
                                        >
                                            {label}
                                        </NavLink>
                                    </li>
                                ))}
                            </ul>
                            <a
                                onClick={() => toggleMenu(false)}
                                href='/'
                                rel='noreferrer noopener'
                                className='return-to-website'>Вернуться на сайт</a>
                        </div>
                    </div>
                </CSSTransition>
            </StyledMobileMenu>
        </StyledHeader>
    )
});


