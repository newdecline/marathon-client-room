import styled from "styled-components/macro";

export const StyledHeader = styled('header')`
    margin-bottom: 11px;
    position: relative;
    z-index: 2;
    @media (min-width: 1260px) {
        margin-bottom: 40px;
    }
`;

export const StyledTopMenu = styled('nav')`
    padding: 37px 42px 39px 35px;
    background: rgba(0, 0, 0, 0.8);
    color: #fff;
    .container {
        display: flex;
        align-items: center;
    }
    .marathon-info, .return-to-website {
        display: none;
    }
    .profile-popup {
        margin-left: auto;
    }
    @media (min-width: 1024px) {
        padding: 37px 0 37px 0;
        .profile-popup {
            margin: 0 -30px 0 auto;
        }
        .container {
            max-width: 947px;
            margin: 0 auto;
        }
        .marathon-info, .return-to-website {
            display: unset;
            margin: 4px 0 0 0;
        } 
        .marathon-info {
            margin-right: 27px;
            font-size: 18px;
            font-family: "Circe-ExtraBold";
            color: #C3C3C3;
            text-transform: uppercase;
        }
        .return-to-website {
            color: #fff;
            font-size: 18px;
            line-height: 120%;
            text-transform: uppercase;
            text-decoration: none;
        }
    }
    @media (min-width: 1260px) {
        padding: 34px 0 40px 0;
        .container {
            max-width: 1200px;
            margin: 0 auto;
        }
        .profile-popup {
            margin: 0 -15px 0 auto;
        }
        .marathon-info {
            margin-right: 27px;
            font-size: 18px;
            font-family: "Circe-ExtraBold";
            color: #C3C3C3;
            text-transform: uppercase;
        }
        .return-to-website {
            color: #fff;
            font-size: 18px;
            line-height: 120%;
            text-transform: uppercase;
            text-decoration: none;
        }
    }
`;

export const StyledBurgerMenu = styled('button')`
    position: relative;
    display: inline-block;
    width: 38px;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    background-color: transparent;
    border: none;
    z-index: 2;
    &:active, &:focus {
        outline: none;
    }
    .item {
        display: block;
        height: 4px;
        background-color: #e5e5e5;
        transform-origin: center;
        transition: transform .3s;
        &-1 {
            transform: ${({isOpenMenu}) => isOpenMenu ? 'translate3d(0, 10px, 0) rotate(45deg)' : 'translate3d(0, 0, 0) rotate(0)'};
        }
        &-2 {
            margin: 6px 0;
            opacity: ${({isOpenMenu}) => isOpenMenu ? 0 : 1};
        }
        &-3 {
            transform: ${({isOpenMenu}) => isOpenMenu ? 'translate3d(0, -10px, 0) rotate(-45deg)' : 'translate3d(0, 0, 0) rotate(0)'};
        }
    }
    @media (min-width: 1024px) {
        display: none;
    }
`;

export const StyledMobileMenu = styled('div')`
    .mobile-menu {
        &-enter {
            background-color: rgba(0, 0, 0, 0);
            .inner {
                transform: translateX(-100%);
            }
        }
        &-enter-active {
            background-color: rgba(0, 0, 0, .6);
            transition: background-color .3s;
            .inner {
                transform: translateX(0);
                transition: transform .3s;
            }
        }
        &-exit {
            background-color: rgba(0, 0, 0, .6);
            transition: background-color .3s;
            .inner {
                transform: translateX(0);
            }  
        }
        &-exit-active {
            background-color: rgba(0, 0, 0, 0);
            transition: background-color .3s;
            .inner {
                transform: translateX(-100%);
                transition: transform .3s;
            }
        }
        &-enter-done {
            background-color: rgba(0, 0, 0, .6);
        }
    }
    .overlay {
        position: fixed;
        top: 0;
        left: 0;
        height: 100vh;
        width: 100%;
    }
    .inner {
        display: flex;
        padding: 102px 42px 39px 35px;
        flex-direction: column;
        height: 100%;
        background: #000;
        color: #fff;
        box-sizing: border-box;
    }
    .menu-list {
        margin: 0;
        padding: 0;
        list-style-type: none;
        display: flex;
        flex-direction: column;
        &__item {
            margin-bottom: 42px;
            &:last-child {
                margin-bottom: 57px;
            }
        }
    }
    .link {
        font-size: 18px;
        line-height: 120%;
        text-transform: uppercase;
        font-family: 'Circe-ExtraBold';
        color: #fff;
        text-decoration:none;
        path {
            fill: #fff;
        }
    }
    .return-to-website {
        font-size: 18px;
        line-height: 120%;
        color: #fff;
        text-transform:uppercase;
        text-decoration:none;
    }
`;