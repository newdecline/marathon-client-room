import styled from "styled-components/macro";

export const StyledNavBar = styled('nav')`
    margin: 17px 20px 0 22px;
    .link {
        display: none;
        color: #454545;
    }
    .add-measurements {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 12px 37px 12px 22px;
        margin: 0;
        background: #000;
        color: #fff;
        border-radius: 5px;
        border: none;
        text-transform: uppercase;
        font-size: 18px;
        font-family: 'Circe-ExtraBold';
        box-sizing: border-box;
        svg {
            display: block;
            margin-right: 27px;
        }
    }
    @media (min-width: 1024px) {
        max-width: 940px;
        margin: 32px auto 20px auto;
        display: flex;
        align-items: center;
        .link {
            padding: 7px 10px 4px 10px;
            margin-right: 14px;
            display: unset;
            font-size: 18px;
            line-height: 120%;
            font-family: "Circe-ExtraBold";
            text-transform: uppercase;
            text-decoration: none;
            &.active {
                background: #fff;
                border-radius: 5px;
            }
            &:first-child {
                display: inline-flex;
                padding: 0 9px 4px 0;
                margin-right: 15px;
            }
            &:last-child {
                margin-right: 0;
            }
        }
        .add-measurements {
            width: auto;
            margin-left: auto;
            padding: 12px 14px 12px 13px;
            svg {
                margin-right: 19px;
            }
            &:hover {
                cursor: pointer;
            }
        }
    }
    @media (min-width: 1260px) {
        max-width: 1200px;
        margin: 39px auto 0 auto;
        display: flex;
        align-items: center;
        .link {
            padding: 7px 10px 4px 10px;
            margin-right: 24px;
            display: unset;
            font-size: 18px;
            line-height: 120%;
            font-family: "Circe-ExtraBold";
            text-transform: uppercase;
            text-decoration: none;
            &.active {
                background: #fff;
                border-radius: 5px;
            }
            &:first-child {
                display: inline-flex;
                padding: 0 10px 4px 0;
                margin-right: 15px;
            }
            &:last-child {
                margin-right: 0;
            }
        }
        .add-measurements {
            width: auto;
            margin-left: auto;
            padding: 12px 14px 12px 13px;
            svg {
                margin-right: 19px;
            }
            &:hover {
                cursor: pointer;
            }
        }
    }
`;