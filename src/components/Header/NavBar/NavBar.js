import React from 'react';
import {observer} from 'mobx-react';
import {popupStore} from '../../../mobx/popupStore';
import {Link, NavLink, withRouter} from 'react-router-dom';
import {ReactComponent as CrossIcon} from "../../../svg/cross.svg";
import {ReactComponent as HomeIcon} from "../../../svg/home.svg";
import commonStore from "../../../mobx/commonStore";
import {StyledNavBar} from "./styled";
import navbar from "../navbar";
import classnames from "classnames";
import get from "lodash/get";
import routes from "../../../routes";

const NavBar = observer(() => {
    const handleClickAddMeasurementsBtn = () => {
        popupStore.setOpenPopup({
            type: 'add-measurements',
            style: {
                right: 0
            }
        });
    };

    return (
        <StyledNavBar>
            <Link to={'/'} className='link'>
                <HomeIcon/>
            </Link>
            {navbar.map(({route, label}) => (
                <NavLink
                    key={route}
                    to={route}
                    className={
                        classnames('link', {
                            disabled: !get(commonStore.dashboard.accessRights, routes.find(({path}) => path === route).accessKey)
                        })
                    }
                    activeClassName="active"
                >
                    {label}
                </NavLink>
            ))}
            <button
                onClick={handleClickAddMeasurementsBtn}
                className={
                    !commonStore.dashboard.accessRights.addResults
                        ? 'add-measurements disabled'
                        : 'add-measurements'
                }
            >
                <CrossIcon/>
                Добавить измерения
            </button>
        </StyledNavBar>
    )
});

export default withRouter(NavBar);