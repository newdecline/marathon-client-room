import React, {useState} from "react";
import {StyledLoginLayout} from "./styled";
import {observer} from "mobx-react";
import {useForm} from 'react-hook-form';
import commonStore from "../../../mobx/commonStore";
import {Preloader} from "../../Preloader/Preloader";
import {Button} from "../../UI/Button/Button";
import classnames from 'classnames';
import {ReactComponent as CrossRotated} from "../../../svg/cross-rotated.svg";
import notifierStore from "../../../mobx/notifierStore";
import apiService from "../../../apiService";

const LoginLayout = observer(() => {
    const {register, handleSubmit} = useForm();

    const [loading, setLoading] = useState(false);
    const [errors, setErrors] = useState([]);

    const onSubmit = async data => {
        setErrors([]);

        setLoading(true);

        const response = await commonStore.login(data);
        if (response.status === 422) {
            setErrors(response.body);
        }

        setLoading(false);
    };

    const onResetFormSubmit = async data => {

        setErrors([]);

        setLoading(true);

        const response = await apiService.resetPassword(data);
        if (response.status === 200) {
            notifierStore.enqueueSuccess('Ссылка для восстановления отправлена на ваш e-mail');

            commonStore.resetPassword = false;
            localStorage.removeItem('resetPassword');
        } else if (response.status === 422) {
            setErrors(response.body);
        }

        setLoading(false);
    };
    
    const cancelPasswordReset = () => {
        setErrors([]);
        commonStore.resetPassword = false;
        localStorage.removeItem('resetPassword');
    };

    const fieldHasError = name => errors.find(({field}) => field === name);

    const renderResetPasswordForm = () => (
        <form
            className={classnames('login-form', {error: errors.length > 0})}
            onSubmit={handleSubmit(onResetFormSubmit)}
        >
            <div className="login-form__close" onClick={cancelPasswordReset}>
                <CrossRotated />
            </div>
            
            <div className="login-form__text">
                Вы запрашиваете восстановление пароля. Введите e-mail и мы направим вам новый пароль.
            </div>
            
            <label className={classnames('login-form__text-label', {error: fieldHasError('email')})}>
                <input type="text" name="email" disabled={loading} ref={register()} />
                <span>E-mail</span>
            </label>
            
            <Button
                label='Отправить'
                variant='alternate'
                disabled={loading}
                error={errors.length > 0}
                loading={loading}
                type="submit"
                fullWidth
            />
            <div className="login-form__errors">
                {errors.length > 0 && errors[errors.length - 1].message}
            </div>
        </form>
    );

    const renderLoginForm = () => (
        <form
            className={classnames('login-form', {error: errors.length > 0})}
            onSubmit={handleSubmit(onSubmit)}
        >
            <label className={classnames('login-form__text-label', {error: fieldHasError('email')})}>
                <input type="text" name="email" disabled={loading} ref={register()} />
                <span>E-mail</span>
            </label>
            <label className={classnames('login-form__text-label', {error: fieldHasError('password')})}>
                <input type="password" name="password" disabled={loading} ref={register()} />
                <span>Пароль</span>
            </label>
            <Button
                label='Войти'
                variant='alternate'
                disabled={loading}
                error={errors.length > 0}
                loading={loading}
                type="submit"
                fullWidth
            />
            <div className="login-form__errors">
                {errors.length > 0 && errors[errors.length - 1].message}
            </div>
            <div className="login-form__bottom">
                <label className="login-form__checkbox-label">
                    <input
                        type="checkbox"
                        name="rememberMe"
                        defaultChecked
                        disabled={loading}
                        ref={register()}
                    />
                    <span className="login-form__checkbox-label-square" />
                    <span className="login-form__checkbox-label-text" >Запомнить меня</span>
                </label>
                <div
                    className="login-form__reset-password"
                    onClick={() => {
                        setErrors([]);
                        commonStore.resetPassword = true;
                    }}
                    title="Восстановить пароль"
                >
                    Забыли пароль?
                </div>
            </div>
        </form>
    );

    return (
        <StyledLoginLayout>
            {commonStore.loading ? (
                <Preloader />
            ) : (commonStore.resetPassword ? renderResetPasswordForm() : renderLoginForm())}
        </StyledLoginLayout>
    );
});

export default LoginLayout;