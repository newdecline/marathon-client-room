import styled from "styled-components/macro";

export const StyledLoginLayout = styled('div')`
    min-height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 22px;
    box-sizing: border-box;
    .login-form {
        position: relative;
        background: #CBCBCB;
        box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.15);
        border-radius: 5px;
        padding: 20px;
        max-width: 310px;
        box-sizing: border-box;
        input:disabled {
            opacity: .4;
            pointer-events: none;
        }
        &__text-label {
            position: relative;
            display: block;
            width: 100%;
            margin-bottom: 28px;
            input {
                display: block;
                width: 100%;
                color: rgb(69, 69, 69);
                background-color: transparent;
                box-sizing: border-box;
                font-size: 18px;
                font-weight: 600;
                padding: 16px 14px 16px 19px;
                border-width: 2px;
                border-style: solid;
                border-color: rgb(69, 69, 69);
                border-image: initial;
                transition: border 0.3s ease 0s;
                outline: none;
            }
            span {
                position: absolute;
                top: -10px;
                left: 14px;
                display: inline-block;
                font-size: 18px;
                line-height: 120%;
                font-weight: 600;
                padding: 0 5px;
                background: rgb(203, 203, 203);
                transition: color 0.3s ease 0s;
            }
            &.error {
                input {
                    border-color: #d96868;
                }
                span {
                    color: #d96868;
                }
            }
        }
        &__checkbox-label {
            position: relative;
            padding-left: 28px;
            display: flex;
            -webkit-box-align: center;
            align-items: center;
            input {
                position: absolute;
                z-index: -1;
                visibility: hidden;
                &:checked + .login-form__checkbox-label-square {
                    background-color: rgb(69, 69, 69);
                }
            }
            &-square {
                position: absolute;
                top: 1px;
                left: 0px;
                width: 15px;
                height: 15px;
                box-sizing: border-box;
                border-width: 1px;
                border-style: solid;
                border-color: rgb(69, 69, 69);
                border-image: initial;
                border-radius: 3px;
                transition: background-color 0.3s ease 0s;
            }
            &-text {
                font-size: 18px;
                line-height: 120%;
                font-weight: 600;
            }
        }
        &__errors {
            font-size: 20px;
            font-weight: bold;
            color: #D96868;
            margin-bottom: 26px;
            white-space: pre-wrap;
        }
        &__text {
            font-size: 20px;
            font-weight: bold;
            margin-bottom: 50px;
            white-space: pre-wrap;
            margin-top: 39px;
        }
        &__close {
            position: absolute;
            top: 19px;
            right: 19px;
            cursor: pointer;
        }
        &.error {
            button {
                margin-bottom: 26px;
            }
        }
        &__reset-password {
            font-size: 18px;
            font-weight: 600;
            margin-top: 20px;
            cursor: pointer;
            text-decoration: underline;
        }
    }
    @media (min-width: 1024px) {
        .login-form {
            padding: 86px 69px 44px 69px;
            max-width: 100%;
            width: 507px;
            &__text-label input {
                padding: 18px 14px 18px 19px;
                box-sizing: border-box;
            }
             &__checkbox-label {
                &:hover {
                    cursor: pointer;
                }
             }
             &__bottom {
                display: flex;
                justify-content: space-between;
             }
             &__text {
                margin-top: 0;
             }
             &__close {
                top: 28px;
                right: 28px;
             }
             &__reset-password {
                margin-top: 0;
             }
        }
    }
`;