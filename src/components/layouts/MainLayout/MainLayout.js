import React from "react";
import {object} from 'prop-types'
import {Header} from "../../Header/Header";
import {Footer} from "../../Footer/Footer";
import {OverlayWithPopups} from "./OverlayWithPopups/OverlayWithPopups";

export const MainLayout = props => {
    const {children} = props;

    return (
        <>
            <Header/>
            {children}
            <Footer/>
            <OverlayWithPopups/>
        </>
    )
};

MainLayout.propTypes = {
    children: object
};