import React, {useState} from "react";
import {StyledDeleteResultsPopup} from "./styled";
import {observer} from "mobx-react";
import {popupStore} from "../../../../../mobx/popupStore";
import {Button} from "../../../../UI/Button/Button";
import notifierStore from "../../../../../mobx/notifierStore";
import apiService from "../../../../../apiService";
import commonStore from "../../../../../mobx/commonStore";

export const DeleteResultsPopup = observer(props => {
    const {type, className, style} = props;

    const handleClickClosePopup = () => {
        popupStore.setOpenPopup({type})
    };

    const [deleting, setDeleting] = useState(false);

    const deleteResult = async () => {
        setDeleting(true);
        const response = await apiService.deleteResult(commonStore.deletingResultId);
        setDeleting(false);

        if (response.status === 204) {
            notifierStore.enqueueSuccess('Замеры удалены');
            commonStore.deletingResultId = null;
            popupStore.setOpenPopup({type})
        }
    };

    return <StyledDeleteResultsPopup
        style={style}
        className={className}>
        <div className="inner">
            <div className="delete-result-popup">
                <div className="delete-result-popup__message">
                    Вы действительно хотите<br/>
                    удалить замеры?<br/>
                    Отменить это действие будет невозможно.
                </div>
                <div className="delete-result-popup__buttons">
                    <Button
                        label='Да, удалить'
                        loading={deleting}
                        onClick={deleteResult}
                    />
                    <Button
                        label='Нет'
                        onClick={handleClickClosePopup}
                    />
                </div>
            </div>
        </div>
    </StyledDeleteResultsPopup>
});

// PhotoViewerPopup.defaultProps = {
//
// };
//
// PhotoViewerPopup.propTypes = {
//
// };