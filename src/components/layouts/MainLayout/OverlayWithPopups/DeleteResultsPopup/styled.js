import styled from 'styled-components/macro';

export const StyledDeleteResultsPopup = styled('div')`
    height: 100vh;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    .delete-result-popup {
        width: 310px;
        padding: 26px 22px;
        background: #D96868;
        box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.15);
        border-radius: 5px;
        font-size: 20px;
        line-height: 120%;
        color: #fff;
        text-transform: none;
        box-sizing: border-box;
        white-space: pre-wrap;
        &__buttons {
            display: flex;
            justify-content: space-between;
            margin-top: 21px;
        }
    }
`;