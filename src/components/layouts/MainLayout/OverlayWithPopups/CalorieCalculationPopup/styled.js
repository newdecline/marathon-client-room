import styled from "styled-components/macro";

export const StyledCalorieCalculationPopup = styled('div')`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #454545;
    .inner {
        max-height: 100vh;
        overflow: auto;
        width: 333px;
        background: #fff;
        box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.15);
        border-radius: 5px;
    }
    .list {
        padding: 33px 33px 26px 22px;
        margin: 0;
        list-style-type: none;
        border-bottom: 1px solid #E5E5E5;
        &__item {
            display: flex;
            margin-bottom: 23px;
            font-size: 20px;
            &:last-child {
                margin-bottom: 0;
            }
        }
    }
    .measurements {
        width: 58%;
    }
    .measurements-value {
        width: 42%;
    }
    .footer {
        padding: 33px 33px 26px 22px;
        &__title {
            margin-bottom: 17px;
            font-size: 20px;
            line-height: 120%;
        }
    }
    .footer-calculated {
        padding: 33px 33px 68px 22px;
        &-item {
            display: flex;
            align-items: center;
            margin-bottom: 19px;
            &:last-child {
                margin-bottom: 0;
            }
            &-text {
                font-size: 18px;
                line-height: 120%;
            }
            &-value {
                margin-left: auto;
                font-size: 36px;
                text-align: right;
                font-family: 'Bodoni';
            }
        }
    }
    .label {
        position: relative;
        display: flex;
        align-items: center;
        padding-left: 30px;
        margin-bottom: 15px;
        &:last-child {
            bottom: 0;
        }
        &__fake-radio {
            position: absolute;
            left: 3px;
            top: 0;
            width: 15px;
            height: 15px;
            border-radius: 50%;
            border: 1px solid #C3C3C3;
            transition: background-color .3s;
        }
        &__input {
            position: absolute;
            z-index: -1;
            visibility: hidden;
            &:checked + .label__fake-radio {
                background-color: #454545;
            }
        }
    }
    .btn-calculate {
        display: inline-block;
        margin: 0 24px 23px 0;
        float: right;
    }
    .btn-count-calories {
        margin: 0 0 23px 24px;
        float: left;
        background-color: transparent;
        color: #454545;
    }
    .btn-save {
        display: inline-block;
        margin: 0 24px 23px 0;
        float: right;
    }
    @media (min-width: 1024px) {
        position: absolute;
        width: auto;
        height: auto;
        right: 0;
        left: unset;
        .inner {
            width: 100%;
        }
        .list {
            padding: 25px 65px 5px 40px;
            display: flex;
            justify-content: space-between;
            &__item {
                margin-right: 50px;
                font-size: 20px;
            }
        }
        .label {
            &:hover {
                cursor: pointer;
            }
            &__text {
                font-size: 20px;
            }
        }
        .measurements {
            width: auto;
            margin-right: 30px;
        }
        .measurements-value {
            width: auto;
        }
        .footer {
            padding: 22px 33px 26px 39px;
        }
        .footer-calculated {
            padding: 36px 33px 26px 39px;
            display: flex;
            &-item {
                flex-direction: column;
                align-items: unset;
                margin-right: 46px;
                &:last-child {
                    margin-right: 0;
                }
                &-text {
                    margin: 0 0 8px 0;
                }
                &-value {
                    margin-left: unset;
                    text-align-last: left;
                    font-size: 48px;
                }
            }
        }
        .btn-calculate {
            margin: 0 24px 33px 36px;
            float: unset;
            &:hover {
                cursor: pointer;
            }
        }
        .radio-buttons-list {
            width: 460px;
        }
        .btn-count-calories, .btn-save {
            &:hover {
                cursor: pointer;
            }
        }
    }
    @media (min-width: 1260px) {
        width: 589px;
        .measurements {
            width: auto;
            margin-right: 17px;
        }
    }
`;