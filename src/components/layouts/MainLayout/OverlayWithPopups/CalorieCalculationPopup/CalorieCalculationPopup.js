import React, {useState} from 'react';
import {ReactComponent as CrossRotatedIcon} from '../../../../../svg/cross-rotated.svg';
import {Button} from "../../../../UI/Button/Button";
import {observer} from 'mobx-react';
import {popupStore} from "../../../../../mobx/popupStore";
import {StyledCalorieCalculationPopup} from "./styled";
import commonStore from "../../../../../mobx/commonStore";
import apiService from "../../../../../apiService";
import {StyledHeaderCard} from "../../../../atoms/StyledHeaderCard";
import notifierStore from "../../../../../mobx/notifierStore";

export const CalorieCalculationPopup = observer(props => {
    const {type, style, className} = props;

    const [isCalculated, setIsCalculated] = useState(false);
    const [activity, setActivity] = useState('min');
    const [loading, setLoading] = useState(false);

    const handleClickCloseBtn = () => {
        popupStore.setOpenPopup({type});
    };

    const handleClickReCalculate = () => {
        setIsCalculated(false);
    };

    const handleClickBtnCalculate = async () => {
        setLoading(true);
        const response = await apiService.calculateCalories(activity);
        setLoading(false);

        if (response.status === 200) {
            setIsCalculated(true);

            notifierStore.enqueueSuccess('Норма калорий обновлена');
        }
    };

    const firstResults = commonStore.dashboard.results[0];

    return (
        <StyledCalorieCalculationPopup style={style} className={className}>
            <div className="inner">
                <StyledHeaderCard>
                    <div className="title">Расчет калорий</div>
                    <button
                        onClick={handleClickCloseBtn}
                        className="close-btn"><CrossRotatedIcon/></button>
                </StyledHeaderCard>
                <ul className="list">
                    <li className="list__item">
                        <span className="measurements">Вес</span>
                        <span className="measurements-value">{firstResults.weight}</span>
                    </li>
                    <li className="list__item">
                        <span className="measurements">Рост</span>
                        <span className="measurements-value">{commonStore.dashboard.profile.height}</span>
                    </li>
                    <li className="list__item">
                        <span className="measurements">Возраст</span>
                        <span className="measurements-value">{commonStore.dashboard.profile.age}</span>
                    </li>
                </ul>
                {renderFooter(isCalculated, activity, setActivity)}
                {renderButtons(isCalculated, handleClickBtnCalculate, handleClickReCalculate, loading, type)}
            </div>
        </StyledCalorieCalculationPopup>
    )
});

const renderFooter = (isCalculated, activity, setActivity) => {
    if (isCalculated) {
        return (
            <div className="footer-calculated">
                <div className="footer-calculated-item">
                    <div className="footer-calculated-item-text">Снижение веса</div>
                    <div className="footer-calculated-item-value">
                        {commonStore.dashboard.calories.reduction}
                    </div>
                </div>
                <div className="footer-calculated-item">
                    <div className="footer-calculated-item-text">Поддержание веса</div>
                    <div className="footer-calculated-item-value">
                        {commonStore.dashboard.calories.maintenance}
                    </div>
                </div>
            </div>
        )
    } else {
        return (
            <div className="footer">
                <div className="footer__title">Укажите вашу активность:</div>
                <div className="radio-buttons-list">
                    <label className="label">
                        <input type="radio" className="label__input" name="action" defaultChecked={activity === 'min'}
                               onClick={() => setActivity('min')}/>
                        <div className="label__fake-radio"/>
                        <div className="label__text">минимум или отсутствие физической нагрузки (только при наличии
                            противопоказаний!)
                        </div>
                    </label>
                    <label className="label">
                        <input type="radio" className="label__input" name="action" defaultChecked={activity === 'sport'}
                               onClick={() => setActivity('sport')}/>
                        <div className="label__fake-radio"/>
                        <div className="label__text">занятия спортом 3 и более раз в неделю</div>
                    </label>
                </div>
            </div>
        )
    }
};

const renderButtons = (isCalculated, handleClickBtnCalculate, handleClickReCalculate, loading, type) => {
    if (isCalculated) {
        return (
            <>
                <Button
                    onClick={handleClickReCalculate}
                    label='пересчитать'
                    className='btn-count-calories'
                />
                <Button
                    onClick={() => popupStore.setOpenPopup({type})}
                    label='сохранить'
                    className='btn-save'
                />
            </>
        )
    } else {
        return (
            <Button
                type='submit'
                loadingLabel='Рассчитывается'
                label='Рассчитать'
                loading={loading}
                className='btn-calculate'
                onClick={handleClickBtnCalculate}
            />
        )
    }
};