import React, {useState} from 'react';
import {useForm} from 'react-hook-form';
import {Button} from "../../../../UI/Button/Button";
import {observer} from 'mobx-react';
import {popupStore} from "../../../../../mobx/popupStore";
import {ReactComponent as CrossRotated} from "../../../../../svg/cross-rotated.svg";
import commonStore from "../../../../../mobx/commonStore";
import apiService from "../../../../../apiService";
import {StyledEditProfilePopup} from "./styled";
import {InputField} from "../../../../UI/InputField/InputField";
import {StyledInputFieldGroup} from "../../../../atoms/StyledInputFieldGroup";
import {StyledHeaderCard} from "../../../../atoms/StyledHeaderCard";
import notifierStore from "../../../../../mobx/notifierStore";

export const EditProfilePopup = observer(props => {
    const {type, style, className} = props;

    const {register, handleSubmit, errors, setError} = useForm();

    const [loading, setLoading] = useState(false);

    const onSubmit = async data => {
        setLoading(true);
        const response = await apiService.updateProfile(data);
        setLoading(false);

        if (response.status === 200) {
            popupStore.setOpenPopup({type});
            notifierStore.enqueueSuccess('Изменения сохранены');
        } else if (response.status === 422) {
            response.body.forEach(({field}) => setError(field));
        }
    };

    const onClosePopup = () => {
        popupStore.setOpenPopup({type});
    };

    let initialValues = {
        name: commonStore.dashboard.profile.name,
        surname: commonStore.dashboard.profile.surname,
        phone: commonStore.dashboard.profile.phone,
    };
    let fields = [
        {
            label: 'Имя',
            fieldName: 'name'
        },
        {
            label: 'Фамилия',
            fieldName: 'surname'
        },
        {
            label: 'Телефон',
            fieldName: 'phone'
        },
    ];

    if (commonStore.dashboard.accessRights.editAgeAndHeight) {
        initialValues = {
            ...initialValues,
            age: commonStore.dashboard.profile.age,
            height: commonStore.dashboard.profile.height,
        };
        fields = [
            ...fields,
            {
                label: 'Возраст',
                fieldName: 'age'
            },
            {
                label: 'Рост',
                fieldName: 'height'
            },
        ];
    }

    return (
        <StyledEditProfilePopup style={style} className={className}>
            <div className="inner">
                <StyledHeaderCard className='header-card'>
                    <div className="title">Мой профиль</div>
                    <button onClick={onClosePopup} className="close-btn"><CrossRotated/></button>
                </StyledHeaderCard>
                <form onSubmit={handleSubmit(onSubmit)} className='form'>
                    <StyledInputFieldGroup>
                        {
                            fields.map(field => {
                                return (
                                    <InputField
                                        className='input-field'
                                        text={field.label}
                                        error={errors[field.fieldName]}
                                        inputProps={{
                                            name: field.fieldName,
                                            defaultValue: initialValues[field.fieldName] || '',
                                            ref: register(),
                                            type: "text",
                                        }}
                                        key={field.fieldName}/>
                                )
                            })
                        }
                    </StyledInputFieldGroup>
                    <Button
                        type='submit'
                        label='Сохранить'
                        loadingLabel='Сохраняется'
                        className='submit'
                        loading={loading}
                        error={Object.keys(errors).length > 0}
                    />
                </form>
            </div>
        </StyledEditProfilePopup>
    )
});