import styled from "styled-components/macro";

export const StyledEditProfilePopup = styled('div')`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #454545;
    .inner {
        max-height: 100vh;
        overflow: auto;
        width: 333px;
        background: #fff;
        box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.15);
        border-radius: 5px;
        box-sizing: border-box;
    }
    .form {
        padding: 34px 20px 25px 26px;
    }
    .submit {
        margin: 20px 0 0 auto;
        display: block;
    }
    @media (min-width: 1024px) {
        position: absolute;
        width: unset;
        height: 442px;
        align-items: stretch;
        .inner {
            max-height: unset;
        }
        .header-card {
            .title {
                font-size: 30px;
            }
        }
        .form {
            padding: 20px 20px 25px 26px;
        }
        .input-field {
            .text {
                width: 38%;
            }
        }
        .input-wrap__input {
            padding: 12px 32px 12px 15px;
        }
    }
    @media (min-width: 1260px) {
        width: 589px;
        min-height: 457px;
        .inner {
            width: 100%;
        }
        .header-card {
            .title {
                font-size: 46px;
            }
        }
        .form {
            padding: 31px 29px 31px 36px;
        }
        .submit {
            margin: 35px 0 0 auto;
        }
        .input-wrap__input {
            padding: 8px 32px 8px 15px;
        }
        .input-field {
            .text {
                width: 28%;
            }
        }
    }
`;