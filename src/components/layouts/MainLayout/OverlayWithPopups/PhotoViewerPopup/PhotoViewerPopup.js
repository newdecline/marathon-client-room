import React from "react";
import {StyledPhotoViewerPopup} from "./styled";
import {observer} from "mobx-react";
import {ReactComponent as CloseIcon} from '../../../../../svg/cross-rotated.svg';
import {popupStore} from "../../../../../mobx/popupStore";
// import {} from "prop-types";

export const PhotoViewerPopup = observer(props => {
    const {type, className, style} = props;

    const handleClickClosePopup = () => {
        popupStore.setOpenPopup({type})
    };
    return <StyledPhotoViewerPopup
        style={style}
        className={className}>
        <div className="inner">
            <button className='close-popup' onClick={handleClickClosePopup}>
                <CloseIcon/>
            </button>
            <img src={popupStore.photoViewer.imageUrl} alt="alt"/>
        </div>
    </StyledPhotoViewerPopup>
});

// PhotoViewerPopup.defaultProps = {
//
// };
//
// PhotoViewerPopup.propTypes = {
//
// };