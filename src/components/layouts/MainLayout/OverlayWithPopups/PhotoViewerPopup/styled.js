import styled from 'styled-components/macro';

export const StyledPhotoViewerPopup = styled('div')`
    height: 100vh;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    .close-popup {
        position: absolute;
        right: 15px;
        top: 15px;
        display: flex;
        padding: 0;
        background-color: transparent;
        border: none;
        z-index: 1;
        outline: none;
        &:hover {
            cursor: pointer;
        }
        svg {
            rect {
                fill: #fff;
            }
        }
    }
    img {
        padding: 30px;
        width: auto;
        max-height: 100vh;
        box-sizing: border-box;
    }
`;