import React, {useEffect} from "react";
import {StyledOverlay} from "./styled";
import {observer} from 'mobx-react';
import {CSSTransition} from "react-transition-group";
import {popupStore} from "../../../../mobx/popupStore";
import {EditMeasurementsPopup} from "./EditMeasurementsPopup/EditMeasurementsPopup";
import {EditProfilePopup} from "./EditProfilePopup/EditProfilePopup";
import {CalorieCalculationPopup} from "./CalorieCalculationPopup/CalorieCalculationPopup";
import {PhotoViewerPopup} from "./PhotoViewerPopup/PhotoViewerPopup.js";
import {DeleteResultsPopup} from "./DeleteResultsPopup/DeleteResultsPopup";

const durationOverlayAnimation = 500;

export const OverlayWithPopups = observer(() => {
    const isShowOverlay = popupStore.showOverlay;

    const handlerClickBody = (e) => {
        if (isShowOverlay) {
            if (!e.target.closest('.inner')) {
                popupStore.setOpenPopup({type: popupStore.openNow});
            }
        }
    };

    useEffect(() => {
        document.body.addEventListener('click', handlerClickBody);
        return () => {
            document.body.removeEventListener('click', handlerClickBody);
        }
    }, [isShowOverlay]);

    return (
        <StyledOverlay
            isShowOverlay={isShowOverlay}
            durationOverlayAnimation={durationOverlayAnimation}>
            <CSSTransition
                in={isShowOverlay}
                timeout={durationOverlayAnimation}
                classNames="transition-overlay"
                unmountOnExit>
                <div>
                    <div className="overlay">
                        <div className="popup-container">
                            {popupStore.addMeasurements.isOpen && (
                                <EditMeasurementsPopup
                                    className='popup'
                                    style={popupStore.addMeasurements.style}
                                    type='add-measurements'/>
                            )}
                            {popupStore.editMeasurements.isOpen && (
                                <EditMeasurementsPopup
                                    className='popup'
                                    style={popupStore.editMeasurements.style}
                                    type='edit-measurements'/>
                            )}
                            {popupStore.editProfile.isOpen && (
                                <EditProfilePopup
                                    className='popup'
                                    style={popupStore.editProfile.style}
                                    type='edit-profile'/>
                            )}
                            {popupStore.calorieCalculation.isOpen && (
                                <CalorieCalculationPopup
                                    className='popup'
                                    style={popupStore.calorieCalculation.style}
                                    type='calculate-calories'/>
                            )}
                            {popupStore.tableEditMeasurements.isOpen && (
                                <EditMeasurementsPopup
                                    className='popup'
                                    style={popupStore.tableEditMeasurements.style}
                                    type='table-edit-measurements'/>
                            )}
                            {popupStore.photoViewer.isOpen && (
                                <PhotoViewerPopup
                                    className='popup'
                                    style={popupStore.photoViewer.style}
                                    type='photo-viewer'/>
                            )}
                            {popupStore.deleteResults.isOpen && (
                                <DeleteResultsPopup
                                    className='popup'
                                    style={popupStore.deleteResults.style}
                                    type='delete-results'/>
                            )}
                        </div>
                    </div>
                </div>
            </CSSTransition>
        </StyledOverlay>
    )
});