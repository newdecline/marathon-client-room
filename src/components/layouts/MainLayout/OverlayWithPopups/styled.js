import styled from 'styled-components/macro';

export const StyledOverlay = styled('div')`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 99;
    pointer-events: ${({isShowOverlay}) => isShowOverlay ? 'all' : 'none'};
    .transition-overlay {
        &-enter {
            height: 100%;
            .overlay {
                background-color: rgba(0, 0, 0, 0);
            }
            .popup-container {
                opacity: 0;
            }
        }
        &-enter-active {
            .overlay {
                background-color: rgba(0, 0, 0, .6);
                transition: ${({durationOverlayAnimation}) => `background-color ${durationOverlayAnimation}ms`};
            }
            .popup-container {
                opacity: 1;
                transition: ${({durationOverlayAnimation}) => `opacity ${durationOverlayAnimation}ms`};
            }
        }
        &-exit {
            height: 100%;
            .overlay {
                background-color: rgba(0, 0, 0, 0.6);
            }
            .popup-container {
                opacity: 1;
            }
        }
        &-exit-active {
            .overlay {
                background-color: rgba(0, 0, 0, 0);
                transition: ${({durationOverlayAnimation}) => `background-color ${durationOverlayAnimation}ms`};
            }
            .popup-container {
                opacity: 0;
                transition: ${({durationOverlayAnimation}) => `opacity ${durationOverlayAnimation}ms`};
            }
        }
        &-enter-done {
            height: 100%;
        }
    }
    .overlay {
        position: relative;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .6);
    }
    @media (min-width: 1024px) {
         .popup-container {
            position:relative;
            margin: 0 auto;
            padding: 213px 0 233px 0;
            max-width: 940px;
            width: 100%;
            height: 100%;
            box-sizing: border-box;
        }
    }
    @media (min-width: 1260px) {
        .popup-container {
            padding: 229px 0 222px 0;
            max-width: 1200px;
        }
    }
`;