import styled from "styled-components/macro";

export const StyledCheckbox = styled('label')`
    position: relative;
    padding-left: 23px;
    input {
        position: absolute;
        visibility: hidden;
        z-index: -1;
    }
    .fake-checkbox {
        top: 7px;
        left: 0;
        width: 15px;
        height: 15px;        
        position: absolute;
        border: 1px solid #C4C4C4;
        box-sizing: border-box;
        border-radius: 3px;
        background-color: transparent;
        transition: background-color .3s;
    }
    input:checked + .fake-checkbox {
        background-color: #000;
    }
    @media (min-width: 1260px) {
        &:hover {
            cursor: pointer;
        }
    }
`;