import React from 'react';
import {string} from "prop-types";
import {StyledCheckbox} from "./styled";

export const Checkbox = props => {
    const {text, name, register, defaultChecked, disabled} = props;

    return (
        <StyledCheckbox>
            <input type="checkbox" defaultChecked={defaultChecked} name={name} ref={register()} disabled={disabled} />
            <div className='fake-checkbox'/>
            <span className="text">{text}</span>
        </StyledCheckbox>
    )
};

Checkbox.propTypes = {
    text: string
};