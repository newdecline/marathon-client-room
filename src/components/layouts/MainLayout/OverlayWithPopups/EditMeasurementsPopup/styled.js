import styled from "styled-components/macro";

export const StyledEditMeasurementsPopup = styled('div')`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #454545;
    .inner {
        max-height: 100vh;
        overflow: auto;
        width: 333px;
        background: #fff;
        box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.15);
        border-radius: 5px;
        .body {
            padding: 15px 17px 24px 22px;
            box-sizing: border-box;
            &-measurements {
                display: flex;
                flex: 1;
                flex-direction: column;
                margin-bottom: 16px;
            }
        }
        .footer {
            position: relative;
            padding: 0 17px 18px 22px;
            justify-content: unset;
            flex-direction: column;
            z-index: 1;
            &__checkbox-wrap {
                display: flex;
                margin-bottom: 20px;
            }
            .text {
                font-size: 20px;
                line-height: 150%;
            }
            .hint {
                display: inline-block;
                margin-left: auto;
            }
            .btn {
                display: inline-block;
                margin-left:auto;
            }
        }
    }
    @media (min-width: 1024px) {
        position: absolute;
        width: 589px;
        height: 442px;
        top: unset;
        left: unset;
        right: unset;
        bottom: unset;
        .form {
            height: 100%;
            display: flex;
            flex-direction: column;
        }
        .inner {
            width: 100%;
            height: 100%;
            .body {
                padding: 23px 35px 34px 36px;
                display: flex;
                justify-content: space-between;
                &-measurements {
                    margin-bottom: 0;
                    margin-right: 22px;
                }
                .input-wrap {
                    max-width: 164px;
                }
                .text {
                    margin-right: auto;
                }
            }
            .footer {
                display: flex;
                flex-direction: row;
                align-items: center;
                margin-top: auto;
                padding: 0 35px 35px 36px;
                &__checkbox-wrap {
                    display: inline-flex;
                    margin-bottom: 0;
                }
                .btn {
                    align-self: center;
                }
                .text {
                    margin-right: 8px;
                }
                .hint-text {
                    bottom: 90px;
                }
            }
        }
    }
    @media (min-width: 1260px) {
        min-height: 457px;
        .inner {
            min-height: 457px;
        }
    }
`;