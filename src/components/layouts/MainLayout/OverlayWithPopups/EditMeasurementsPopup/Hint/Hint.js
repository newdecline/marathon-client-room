import React, {useState} from 'react';
import {object} from "prop-types";
import {StyledHint} from "./styled";

export const Hint = props => {
    const {icon} = props;

    const [isMouseOver, setIsMouseOver] = useState(false);

    const onMouseEnter = () => {
        setIsMouseOver(true);
    };
    const onMouseLeave = () => {
        setIsMouseOver(false);
    };

    return (
        <StyledHint
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}>
            {icon}
            {isMouseOver
            && <div className="hint-text">
                <div className="hint-text__title">Факторы стресса:</div>
                <ul className="list">
                    <li>- простуда, ОРВИ и грипп</li>
                    <li>- критические дни</li>
                    <li>- период реабилитации после операции и болезни</li>
                    <li>- режим сна (дефицит)</li>
                    <li>- режим воды (дефицит или большой профицит)</li>
                    <li>- режим соли (избыток)</li>
                    <li>- новые лекарственные препараты</li>
                </ul>
            </div>}
        </StyledHint>
    )
};

Hint.propTypes = {
    icon: object
};