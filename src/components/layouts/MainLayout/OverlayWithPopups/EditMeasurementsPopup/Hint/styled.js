import styled from "styled-components/macro";

export const StyledHint = styled('div')`
    position: relative;
    display: flex;
    .hint-text {
        width: 300px;
        padding: 15px;
        position: absolute;
        right: 0;
        bottom: 40px;
        border: 1px solid #ccc;
        box-sizing: border-box;
        white-space: pre-wrap;
        background: #C4C4C4;
        border-radius: 8px;
        &__title {
            margin-bottom: 15px;
            font-size: 14px;
        }
        &__subtitle {
            margin: 0;
            font-size: 12px;
        }
        .list {
            padding: 0;
            margin: 0;
            list-style-type: none;
            font-size: 12px;
        }
    }
    @media (min-width: 1260px) {
        position: unset;
        .hint-text {
            padding: 16px 22px;
            right: 34px;
            bottom: 95px;
        }
    }
`;