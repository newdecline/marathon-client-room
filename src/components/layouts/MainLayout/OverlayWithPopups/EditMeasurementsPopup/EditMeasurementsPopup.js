import React, {useEffect, useState} from 'react';
import {observer} from 'mobx-react';
import {useForm} from 'react-hook-form';
import {popupStore} from "../../../../../mobx/popupStore";
import {StyledEditMeasurementsPopup} from "./styled";
import {Checkbox} from "./Checkbox/Checkbox";
import {Hint} from "./Hint/Hint";
import {Button} from "../../../../UI/Button/Button";
import {ReactComponent as AddFileIcon} from "../../../../../svg/addFile.svg";
import {ReactComponent as QuestionIcon} from "../../../../../svg/question.svg";
import {ReactComponent as CrossRotatedIcon} from "../../../../../svg/cross-rotated.svg";
import {AddMeasurementsUploadImages} from "../../../../AddMeasurementsUploadImages/AddMeasurementsUploadImages";
import apiService from "../../../../../apiService";
import commonStore from "../../../../../mobx/commonStore";
import {InputField} from "../../../../UI/InputField/InputField";
import {StyledInputFieldGroup} from "../../../../atoms/StyledInputFieldGroup";
import {StyledHeaderCard} from "../../../../atoms/StyledHeaderCard";
import notifierStore from "../../../../../mobx/notifierStore";

const fields = [
    {
        label: 'Вес',
        fieldName: 'weight',
        measure: 'кг'
    },
    {
        label: 'Обхват груди',
        fieldName: 'chest',
        measure: 'см'
    },
    {
        label: 'Обхват талии',
        fieldName: 'waist',
        measure: 'см'
    },
    {
        label: 'Обхват бедер',
        fieldName: 'hip',
        measure: 'см'
    }
];

export const EditMeasurementsPopup = observer(props => {
    const {type, className, style} = props;
    const {register, handleSubmit, errors, setValue, setError, clearError} = useForm();

    const editing = commonStore.editingResultsIndex !== null;

    let initialData = {
        weight: '',
        waist: '',
        chest: '',
        hip: '',
        has_stress: false,
        image: undefined,
    };
    if (editing) {
        initialData = commonStore.dashboard.results[commonStore.editingResultsIndex];
    }

    const [imageLoading, setImageLoading] = useState(false);
    const [image, setImage] = useState(initialData.image);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        register({name: 'imageId'});
        if (initialData.image) {
            setValue('imageId', initialData.image.id);
        }
    }, []);

    const handleClickCloseBtn = () => {
        popupStore.setOpenPopup({type});
        commonStore.editingResultsIndex = null;
    };

    // TODO Рефакторинг.
    const onUpdateFile = async (file) => {
        if (!file) {
            setValue('imageId', null);
            setImage(null);
        } else {
            clearError('imageId');

            setImageLoading(true);
            const response = await apiService.createImage(file);
            setImageLoading(false);

            if (response.status === 200) {
                setImage(response.body);
                setValue('imageId', response.body.id);
            } else {
                setImage(null);
                setError('imageId');
                setValue('imageId', null);
            }
        }
    };

    let modalHeader;
    if (editing) {
        modalHeader = commonStore.editingResultsIndex === 0 ? 'Первые замеры' : `Неделя ${commonStore.editingResultsIndex}`;
    } else {
        const resultsCount = commonStore.dashboard.results.length;
        modalHeader = resultsCount === 0 ? 'Первые замеры' : `Неделя ${resultsCount}`;
    }

    const onSubmit = async values => {
        setLoading(true);

        let response;

        if (editing) {
            response = await apiService.updateResult(initialData.id, values);
        } else {
            response = await apiService.createResult(values);
        }

        if (response.status === 200 || response.status === 201) {
            commonStore.editingResultsIndex = null;

            if (editing) {
                notifierStore.enqueueSuccess('Изменения сохранены');
            } else {
                notifierStore.enqueueSuccess('Замеры добавлены');
            }

            popupStore.setOpenPopup({type, isOpen: false});
        } else if (response.status === 422) {
            response.body.forEach(({field}) => setError(field));
        }

        setLoading(false);
    };

    const canEdit = editing
        ? commonStore.dashboard.results[commonStore.editingResultsIndex].isEditableFromClient
        : commonStore.dashboard.accessRights.addResults;

    return (
        <StyledEditMeasurementsPopup
            style={style}
            className={className}>
            <div className={!canEdit ? "inner disabled" : "inner"}>
                <form onSubmit={handleSubmit(onSubmit)} className='form'>
                    <StyledHeaderCard>
                        <div className="title">{modalHeader}</div>
                        <button
                            type='button'
                            onClick={handleClickCloseBtn}
                            className="close-btn"><CrossRotatedIcon/></button>
                    </StyledHeaderCard>
                    <div className="body">
                        <StyledInputFieldGroup className="body-measurements">
                            {
                                fields.map(field => {
                                    return (
                                        <InputField
                                            text={field.label}
                                            inputLabel={field.measure}
                                            error={errors[field.fieldName]}
                                            inputProps={{
                                                name: field.fieldName,
                                                defaultValue: initialData[field.fieldName] || '',
                                                ref: register(),
                                                type: "text",
                                                disabled: !canEdit,
                                            }}
                                            key={field.fieldName}/>
                                    )
                                })
                            }
                        </StyledInputFieldGroup>
                        <div className="body-upload-file">
                            <AddMeasurementsUploadImages
                                image={image}
                                error={errors.imageId}
                                inputName='imageId'
                                onUpdateFile={onUpdateFile}
                                title={`Перетащите сюда\nфото весов или кликните`}
                                icon={<AddFileIcon/>}
                                loading={imageLoading}
                            />
                        </div>
                    </div>
                    <div className="footer">
                        <div className="footer__checkbox-wrap">
                            <Checkbox
                                disabled={!canEdit}
                                name="has_stress"
                                register={register}
                                defaultChecked={initialData.has_stress}
                                text='наличие факторов стресса'
                            />
                            <div className="hint">
                                <Hint
                                    icon={<QuestionIcon/>}/>
                            </div>
                        </div>
                        <Button
                            type='submit'
                            label='Сохранить'
                            loadingLabel='Сохраняется'
                            disabled={!canEdit}
                            loading={loading}
                            className='btn'
                            error={Object.keys(errors).length > 0}
                        />
                    </div>
                </form>
            </div>
        </StyledEditMeasurementsPopup>
    )
});