import styled from "styled-components/macro";

export const StyledFooter = styled('footer')`
    margin: 0;
    padding: 38px 30px 35px 41px;
    display: flex;
    flex-direction: column;
    background: #454545;
    color: #fff;
    font-family: "Circe-Light";
    .navigation-list {
        display: none;
        &__item {
            a {
                text-decoration: none;
                color: #fff;
            }
        }
    }
    .additional-menu {
        padding: 0;
        &__item {
            display: none;
        }
    }
    .social-list {
        display: flex;
        align-items: center;
        margin-bottom: 35px;
        padding: 0;
        &__item {
            display: flex;
            margin-right: 25px;
            &:nth-child(2) {
                width: 27px;
                height: 27px;
                margin-right: 0;
                .social-list__link {
                    width: 27px;
                    height: 27px;
                }
                svg path{
                    fill: #fff;
                }
            }
        }
        &__link {
            display: inline-flex;
            width: 25px;
            height: 25px;
            svg {
                width: 100%;
                height: 100%;
            }
        }
    }
    .copyright {
        margin-bottom: 8px;
        &__author {
            font-size: 20px;
            line-height: 140%;
        }
        &__text {
            font-size: 20px;
            line-height: 140%;
        }
        &__author {
            white-space: pre-wrap;
        }
    }
    .developed-by {
        text-decoration: none;
        color: #fff;
        &__text {
            margin-bottom: 15px;
            font-size: 20px;
            line-height: 120%;
        }
        &__agency-name {
            width: 105px;
            height: 37px;
            svg {
                width: 100%;
                height: 100%;
            }
        }
    }
    @media (min-width: 1024px) {
        padding: 0;
        .container {
            max-width: 940px;
            width: 100%;
            margin: 0 auto;
            padding: 54px 23px 35px 24px;
            display: flex;
            box-sizing: border-box;
        }
        .date {
            display: block;
        }
        .developed-by {
            display: flex;
            flex-direction: column;
            margin-left: 0;
            &__text {
                padding-left: 132px;
            }
            &__agency-name {
                align-self: flex-end;
            }
        }
        .navigation-list {
            display: block;
            margin: 0;
            padding: 0;
            list-style-type: none;
            &__item {
                margin-right: 0;
                text-transform: unset;
                line-height: 138%;
                font-size: 20px;
            }
        }
        .additional-menu {
            margin: 0 0 0 10px;
            &__item {
                display: block;
                font-size: 20px;
                line-height: 138%;
                a {
                    color: #fff;
                    text-decoration: none;
                }
            }
        }
        .developed-by {
            display: inline-block;
            margin-left: 89px;
            &__agency-name {
                width: 139px;
                height: 51px;
            }
        }
        .social-list {
            margin-top: 23px;
            margin-bottom: 0;
        }
        .col {
            width: calc(28% - 36px);
            &:last-child {
                display: flex;
                margin-left: auto;
            }
        }
        .developed-by {
            display: inline-flex;
            margin: 0 0 0 auto;
            &__text {
                padding-left: 0;
            }
        }
    }
    @media (min-width: 1260px) {
        padding: 0;
        .container {
            max-width: 1200px;
            width: 100%;
            margin: 0 auto;
            padding: 44px 0 35px 0;
            display: flex;
            box-sizing: border-box;
        }
        .date {
            display: block;
        }
        .developed-by {
            display: flex;
            flex-direction: column;
            margin-left: 0;
            &__text {
                padding-left: 132px;
            }
            &__agency-name {
                align-self: flex-end;
            }
        }
        .navigation-list {
            display: block;
            margin: 0;
            padding: 0;
            list-style-type: none;
            &__item {
                margin-right: 0;
                text-transform: unset;
                line-height: 138%;
                font-size: 20px;
            }
        }
        .additional-menu {
            margin: 0 0 0 10px;
            &__item {
                display: block;
                font-size: 20px;
                line-height: 138%;
                a {
                    color: #fff;
                    text-decoration: none;
                }
            }
        }
        .developed-by {
            display: inline-block;
            margin-left: 89px;
            &__agency-name {
                width: 139px;
                height: 51px;
            }
        }
        .social-list {
            margin-top: 23px;
            margin-bottom: 0;
        }
        .col {
            width: calc(28% - 36px);
            &:last-child {
                display: flex;
                margin-left: auto;
            }
        }
        .developed-by {
            display: inline-flex;
            margin: 0 0 0 auto;
            &__text {
                padding-left: 0;
            }
        }
    }
`;