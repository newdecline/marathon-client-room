import React from 'react';
import {ReactComponent as InstagramIcon} from "../../svg/instagram.svg";
import {ReactComponent as WhatsappIcon} from "../../svg/whatsapp.svg";
import {ReactComponent as PinkchilliIcon} from "../../svg/pinkchilli-logo.svg";
import {observer} from "mobx-react";
import commonStore from "../../mobx/commonStore";
import {StyledFooter} from "./styled";

const navigation = [
    {
        href: '/',
        label: 'Главная'
    },
    {
        href: '/#about',
        label: 'О проекте'
    },
    {
        href: '/#participation-results',
        label: 'Результаты участниц'
    },
    {
        href: '/#feedback',
        label: 'Отзывы'
    }
];

export const Footer = observer(() => {
    return (
        <StyledFooter>
            <div className="container">
                <div className="col">
                    <div className="copyright">
                        <div className="copyright__author">{commonStore.dashboard.layout.copyright}</div>
                        <div className="copyright__text">
                            <span className='date'>
                                {new Date().getFullYear()}
                            </span>
                        </div>
                    </div>
                </div>

                <div className="col">
                    <ul className='navigation-list'>
                        {
                            navigation.map((item, index) => (
                                <li
                                    key={index}
                                    className='navigation-list__item'>
                                    <a href={item.href} target="_blank"
                                       rel='noreferrer noopener'>{item.label}</a>
                                </li>))
                        }
                    </ul>
                </div>

                <div className="col">
                    <ul className='additional-menu'>
                        <li className='additional-menu__item'>
                            <a href='/client' rel='noopener noreferrer'>Личный кабинет</a>
                        </li>
                        <li className='additional-menu__item'>
                            <a target='_blank' rel='noopener noreferrer' href={commonStore.dashboard.layout.whats_app_url}>Оплатить
                                участие</a>
                        </li>

                        <ul className='social-list'>
                            <li className="social-list__item">
                                <a href={commonStore.dashboard.layout.instagram_url} target='_blank' rel='noopener noreferrer'
                                   className='social-list__link'><InstagramIcon/></a>
                            </li>
                            <li className="social-list__item">
                                <a target='_blank' rel='noopener noreferrer' href={commonStore.dashboard.layout.whats_app_url}
                                   className='social-list__link'><WhatsappIcon/></a>
                            </li>
                        </ul>
                    </ul>
                </div>

                <div className="col">
                    <a href='https://pinkchilli.agency' target='_blank' rel='noopener noreferrer'
                       className="developed-by">
                        <div className="developed-by__text">Разработка</div>
                        <div className="developed-by__agency-name"><PinkchilliIcon/></div>
                    </a>
                </div>
            </div>
        </StyledFooter>

    )
});