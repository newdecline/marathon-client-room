import React from 'react';
import {string, object, func, bool, any} from 'prop-types';
import {useDropzone} from 'react-dropzone';
import {ReactComponent as TrashIcon} from '../../svg/trash.svg';
import {StyledAddMeasurementsUploadImages} from "./styled";
import commonStore from "../../mobx/commonStore";

export const AddMeasurementsUploadImages = props => {

    const {
        image,
        title,
        icon,
        error,
        onUpdateFile = () => {
        },
        loading,
    } = props;

    const handleClickTrashBtn = () => {
        onUpdateFile(undefined);
    };

    const {getRootProps, getInputProps, isDragActive} = useDropzone({
        accept: 'image/*',
        onDrop: acceptedFiles => {
            onUpdateFile(acceptedFiles[0]);
        },
        onFileDialogCancel: () => {
            !image && onUpdateFile(undefined);
        },
    });

    const renderDropZone = () => {
        return (
            <div {...getRootProps()} className='drop-zone-wrap'>
                <input {...getInputProps({disabled: loading || commonStore.isAdmin})}/>
                <div className={loading ? 'drop-zone loading' : 'drop-zone'}>
                    {
                        loading
                            ? <>
                                <div className="container-loading">
                                    <span className="container-loading__active"/>
                                </div>
                                <div className='loading-message'>Изображение загружается</div>
                            </>
                            : <>
                                <div className="plus-icon">{icon}</div>
                                <div className="title">{title}</div>
                            </>
                    }
                </div>
            </div>
        );
    };

    const renderThumbnail = () => {
        return (
            <div className='preview'>
                <div className='thumb'>
                    <button
                        onClick={handleClickTrashBtn}
                        className='trash'><TrashIcon/>
                    </button>
                    <img src={image && image.url} alt='alt'/>
                </div>
            </div>
        );
    };

    return (
        <StyledAddMeasurementsUploadImages
            error={error}
            isDragActive={isDragActive}
            className='upload-file'>
            {image ? renderThumbnail() : renderDropZone()}
        </StyledAddMeasurementsUploadImages>
    )
};


AddMeasurementsUploadImages.propTypes = {
    title: string,
    subtitle: string,
    icon: object,
    onUpdateFile: func,
    error: object,
    inputName: string,
    image: any,
    loading: bool
};