import styled, {keyframes} from "styled-components/macro";

const translation = keyframes`
    0% {
        left: -70%;
        transform: scale(0.5, 1);
    }
    
    90% {
        transform: scale(7, 1);
    }
    
    100% {
        left: 200%;
        transform: scale(0.5, 1);
    }
`;

export const StyledAddMeasurementsUploadImages = styled('div')`
    .drop-zone-wrap {
        outline: none;
    }
    .drop-zone {
        padding: 22px 16px 25px 23px;
        display: flex;
        align-items: center;
        justify-content: center;
        border: ${({error}) => error ? '1px dashed #D96868' : '1px dashed #C3C3C3'};
        box-sizing: border-box;
        background-color: ${({isDragActive}) => isDragActive ? 'rgba(0, 0, 0, 0.1)' : 'transparent'};
        transition: background-color .3s;
        svg {
            margin-right: 22px;
        }
        rect {
            fill: ${({error}) => error ? '#D96868' : '#454545'};
        }
        circle {
            stroke: ${({error}) => error ? '#D96868' : '#454545'};
        }
        &.loading {
            flex-direction: column;
            justify-content: center;
            padding: 25px 16px 25px 23px;
        }
    }
    .preview {
        display: flex;
        flex-direction: row;
    }
    .thumb {
        position: relative;
        display: inline-flex;
        max-width: 164px;
        width: 100%;
        height: 199px;
        box-sizing: border-box;
        overflow: hidden;
        img {
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            width:auto;
            height: 100%;
            object-fit: cover;
        }
    }
    .title {
        white-space: pre-wrap;
        color: ${({error}) => error ? '#D96868' : '#454545'};
    }
    .plus-icon {
        display: flex;
    }
    .trash {
        position: absolute;
        top: 15px;
        right: 15px;
        background-color: transparent;
        padding: 0;
        margin: 0;
        border: none;
        z-index: 1;
        outline: none;
    }
    .container-loading {
        position: relative;
        width: 72%;
        height: 4px;
        background-color: #c3c3c3;
        overflow: hidden;
        &__active {
            position:absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 20%;
            display: block;
            background: #9ac190;
            animation: ${translation} 1s ease-in infinite;
        }
    }
    .loading-message {
        margin-top: 10px;
    }
    @media (min-width: 1024px) {
        height: 100%;
        width: 164px;
        .drop-zone {
            height: 100%;
            flex-direction: column;
            justify-content: flex-end;
            padding: 0 13px 12px 13px;
            box-sizing: border-box;
            text-align: center;
            &:hover {
                cursor: pointer;
            }
            svg {
                margin-right: 0;
                margin-bottom: 36px;
            }
            &.loading {
                justify-content: center;
                padding: 0;
            }
        }
        .trash {
            &:hover {
                cursor: pointer;
            }
        }
        .drop-zone-wrap {
            height: 100%;
        }
    }
`;