import {observable, action} from "mobx/lib/mobx";

class NotifierStore {

    @observable notifications = [];

    @action enqueueSuccess(message = 'Успешно') {
        this.enqueueSnackbar({
            message,
            theme: 'success',
        })
    }

    @action enqueueError(message = 'Ошибка') {
        this.enqueueSnackbar({
            message,
            theme: 'error',
        })
    }

    @action enqueueWarning(message) {
        this.enqueueSnackbar({
            message,
            theme: 'warning',
        })
    }

    @action enqueueSnackbar(note) {
        this.notifications.push({
            key: new Date().getTime() + Math.random(),
            ...note,
        });
    }

    @action removeSnackbar(note) {
        this.notifications.remove(note);
    }
}

export default new NotifierStore();