import {observable, action, computed} from 'mobx';

const initialStyle = {
    top: 'unset',
    bottom: 'unset',
    left: 'unset',
    right: 'unset',
    transform: 'unset'
};

export const popupStore = observable({
    openNow: null,
    calorieCalculation: {
        isOpen: false,
        style: initialStyle
    },
    editProfile: {
        isOpen: false,
        style: initialStyle
    },
    addMeasurements: {
        isOpen: false,
        style: initialStyle
    },
    editMeasurements: {
        isOpen: false,
        style: initialStyle
    },
    tableEditMeasurements: {
        isOpen: false,
        style: initialStyle
    },
    photoViewer: {
        isOpen: false,
        style: initialStyle,
        imageUrl: null
    },
    deleteResults: {
        isOpen: false,
        style: initialStyle,
    },
    setOpenPopup(payload) {
        this.openNow = payload.type;

        switch (payload.type) {
            case 'calculate-calories':
                return this.calorieCalculation = {
                    isOpen: !this.calorieCalculation.isOpen,
                    style: {
                        ...this.calorieCalculation.style,
                        ...payload.style || initialStyle
                    }
                };
            case 'edit-profile':
                return this.editProfile = {
                    isOpen: !this.editProfile.isOpen,
                    style: {
                        ...this.editProfile.style,
                        ...payload.style || initialStyle
                    }
                };
            case 'add-measurements':
                return this.addMeasurements = {
                    isOpen: payload.isOpen === undefined
                        ? !this.addMeasurements.isOpen
                        : payload.isOpen,
                    style: {
                        ...this.addMeasurements.style,
                        ...payload.style || initialStyle
                    }
                };
            case 'edit-measurements':
                return this.editMeasurements = {
                    isOpen: !this.editMeasurements.isOpen,
                    style: {
                        ...this.editMeasurements.style,
                        ...payload.style || initialStyle
                    }
                };
            case 'table-edit-measurements':
                return this.tableEditMeasurements = {
                    isOpen: !this.tableEditMeasurements.isOpen,
                    style: {
                        ...this.tableEditMeasurements.style,
                        ...payload.style || initialStyle
                    }
                };
            case 'photo-viewer':
                return this.photoViewer = {
                    isOpen: !this.photoViewer.isOpen,
                    style: {
                        ...this.photoViewer.style,
                        ...payload.style || initialStyle
                    },
                    imageUrl: payload.imageUrl || null
                };
            case 'delete-results':
                return this.deleteResults = {
                    isOpen: !this.deleteResults.isOpen,
                    style: {
                        ...this.deleteResults.style,
                        ...payload.style || initialStyle,
                    },
                };
            default:
                return this.openNow = null;
        }
    },
    get showOverlay() {
        const isShow = this.addMeasurements.isOpen
            || this.editMeasurements.isOpen
            || this.editProfile.isOpen
            || this.calorieCalculation.isOpen
            || this.tableEditMeasurements.isOpen
            || this.photoViewer.isOpen
            || this.deleteResults.isOpen;

        if (!isShow) {
            this.openNow = null
        }

        return isShow;
    }
}, {
    setOpenPopup: action,
    showOverlay: computed,
});