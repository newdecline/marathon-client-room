import {observable, action} from 'mobx';
import uuid from 'uuid/v4';
import apiService from '../apiService';

class CommonStore {

    @observable loading = true;
    @observable token;
    @observable dashboard = null;

    @observable editingResultsIndex = null;
    @observable deletingResultId = null;
    @observable isOpenMenu = false;
    @observable isOpenProfilePopup = false;

    // TODO Что это?
    @observable fakeIdChart = uuid();

    @observable resetPassword = false;

    constructor() {
        this.token = localStorage.getItem('token');
        this.resetPassword = localStorage.getItem('resetPassword') !== null;
    }

    @action async login(data) {
        const response = await apiService.login(data);
        if (response.status === 200) {
            this.token = response.body.value;
            localStorage.setItem('token', response.body.value);
            this.loadDashboard();
        }

        return response;
    }

    // TODO Вызывать apiService.logout? (учесть, что вызывается при получении 401 с любого запроса)
    @action logout() {
        this.token = undefined;
        localStorage.removeItem('token');
        this.dashboard = null;
    }

    // loading выставляется вручную, чтобы избежать лага с показыванием формы входа при первой загрузке
    @action async loadDashboard() {
        this.loading = true;
        const response = await apiService.getDashboard();
        if (response.status === 200) {
            this.dashboard = response.body;
        } else {
            this.dashboard = null;
        }
        this.loading = false;
    }

    @action setOpenMenu(isOpen) {
        this.isOpenMenu = isOpen === undefined ? !this.isOpenMenu : isOpen;
    }

    @action setOpenProfilePopup(isOpen) {
        if (this.isOpenMenu) {
            this.setOpenMenu(false)
        }
        this.isOpenProfilePopup = isOpen === undefined ? !this.isOpenProfilePopup : isOpen;
    }

    @action updateChart() {
        this.fakeIdChart = uuid();
    }

    // TODO Рефакторинг
    get chartsData() {
        let charts = [
            {
                name: 'weight',
                title: 'Вес',
                data: [undefined, undefined, undefined, undefined, undefined],
                checked: [null, null, null, null, null]
            },
            {
                name: 'breast',
                title: 'Грудь',
                data: [undefined, undefined, undefined, undefined, undefined],
                checked: [null, null, null, null, null]
            },
            {
                name: 'waist',
                title: 'Талия',
                data: [undefined, undefined, undefined, undefined, undefined],
                checked: [null, null, null, null, null]
            },
            {
                name: 'hips',
                title: 'Бедра',
                data: [undefined, undefined, undefined, undefined, undefined],
                checked: [null, null, null, null, null]
            },
        ];
        for (let i = 0; i < this.dashboard.results.length; i++) {
            charts[0].data[i] = this.dashboard.results[i].weight;
            charts[1].data[i] = this.dashboard.results[i].chest;
            charts[2].data[i] = this.dashboard.results[i].waist;
            charts[3].data[i] = this.dashboard.results[i].hip;

            const checked = this.dashboard.results[i].isChecked ? true : null;

            charts[0].checked[i] = checked;
            charts[1].checked[i] = checked;
            charts[2].checked[i] = checked;
            charts[3].checked[i] = checked;
        }

        return charts;
    }

    get fullName() {
        return [
            this.dashboard.profile.name,
            this.dashboard.profile.surname,
        ].join(' ');
    }
}

export default new CommonStore();