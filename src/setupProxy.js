const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(proxy(['/api', '/images'], {
        target: process.env.REACT_APP_PROXY,
        changeOrigin: true,
    }));
};