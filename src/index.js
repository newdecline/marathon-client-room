import React from 'react';
import {render} from 'react-dom';
import {App} from './App';
import * as serviceWorker from './serviceWorker';
import { SnackbarProvider } from 'react-snackbar-alert';

render(
    <SnackbarProvider>
        <App />
    </SnackbarProvider>,
    document.getElementById('root')
);

serviceWorker.unregister();
