import React from "react";
import { observer } from "mobx-react";
import { autorun } from 'mobx';
import notifierStore from "./mobx/notifierStore";
import {wrapComponent} from "react-snackbar-alert";
import omit from "lodash/omit";

@observer
class Notifier extends React.Component {
    displayed = [];

    storeDisplayed = (id) => {
        this.displayed = [...this.displayed, id];
    };

    componentDidMount() {
        autorun(() => {
            const { notifications = [] } = notifierStore;
            
            notifications.forEach((notification) => {
                // Do nothing if snackbar is already displayed
                if (this.displayed.includes(notification.key)) return;
                // Display snackbar using notistack
                this.props.createSnackbar(omit(notification, 'key'));
                // Keep track of snackbars that we've displayed
                this.storeDisplayed(notification.key);
                // Dispatch action to remove snackbar from mobx store
                notifierStore.removeSnackbar(notification.key);
            });
        });
    }

    render() {
        return null;
    }
}

export default wrapComponent(Notifier);