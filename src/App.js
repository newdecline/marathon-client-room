import React, {useEffect} from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {observer} from 'mobx-react';
import './css/normalize.css';
import './css/index.css';
import commonStore from './mobx/commonStore';
import LoginLayout from "./components/layouts/LoginLayout/LoginLayout";
import Notifier from "./Notifier";
import routes from "./routes";
import get from "lodash/get";
import {MainLayout} from "./components/layouts/MainLayout/MainLayout";

const App = observer(() => {
    useEffect(() => {
        commonStore.loadDashboard();
    }, []);
    
    return (
        <>
            <div className='app'>
                {!commonStore.dashboard ? (
                    <LoginLayout />
                ) : (
                    <BrowserRouter basename={'/client'}>
                        <Switch>
                            {routes.map(({exact, path, page: Page, accessKey}) =>
                                (accessKey && get(commonStore.dashboard.accessRights, accessKey) || !accessKey) && (
                                    <Route
                                        key={path}
                                        exact={exact}
                                        path={path}
                                        render={props => (
                                            <MainLayout>
                                                <Page {...props} />
                                            </MainLayout>
                                        )}
                                    />
                                ))}
                        </Switch>
                    </BrowserRouter>
                )}
            </div>
            <Notifier />
        </>

    );
});

export {App};
