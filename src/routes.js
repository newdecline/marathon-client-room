import {IndexPage} from "./pages/IndexPage/IndexPage";
import {RulesPage} from "./pages/RulesPage/RulesPage";
import {ProfilePage} from "./pages/ProfilePage/ProfilePage";
import {FoodPage} from "./pages/FoodPage/FoodPage";
import {RecipesPage} from "./pages/RecipesPage/RecipesPage";
import {RecipePage} from "./pages/RecipePage/RecipePage";
import {TrainingsPage} from "./pages/TrainingsPage/TrainingsPage";
import {ErrorPage} from "./pages/ErrorPage";

const routes = [
    {
        exact: true,
        path: '/',
        page: IndexPage,
    },
    {
        path: '/profile',
        page: ProfilePage,
    },
    {
        path: '/rules',
        page: RulesPage,
        accessKey: 'pages.rules',
    },
    {
        path: '/food',
        page: FoodPage,
        accessKey: 'pages.food',
    },
    {
        exact: true,
        path: '/recipes',
        page: RecipesPage,
        accessKey: 'pages.recipes',
    },
    {
        path: '/recipes/:id',
        page: RecipePage,
        accessKey: 'pages.recipes',
    },
    {
        path: '/trainings',
        page: TrainingsPage,
        accessKey: 'pages.trainings',
    },
    {
        path: '*',
        page: ErrorPage,
    },
];

export default routes;