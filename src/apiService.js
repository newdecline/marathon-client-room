import HttpService from "./HttpService";

const AUTH_ROUTE = '/auth';
const IMAGES_ROUTE = '/images';
const CLIENT_ROUTE = '/client';

const DASHBOARD_ROUTE = `${CLIENT_ROUTE}/dashboard`;
const PAGES_ROUTE = `${CLIENT_ROUTE}/pages`;
const RECIPES_ROUTE = `${CLIENT_ROUTE}/recipes`;
const PROFILE_ROUTE = `${CLIENT_ROUTE}/profile`;
const CALORIES_ROUTE = `${CLIENT_ROUTE}/calories`;
const BEFORE_AND_AFTER_IMAGES_ROUTE = `${CLIENT_ROUTE}/images`;
const RESULTS_ROUTE = `${CLIENT_ROUTE}/results`;

class ApiService extends HttpService {

    // --- Auth ---

    async login(data) {
        return await this.post(`${AUTH_ROUTE}/login`, data, {
            snackbar: false,
            setLoading: false,
            updateDashboard: false,
        });
    }

    async logout() {
        return await this.post(`${AUTH_ROUTE}/logout`, {}, {updateDashboard: false});
    }

    async resetPassword(data) {
        return await this.post(`${AUTH_ROUTE}/reset-password`, data, {
            setLoading: false,
            updateDashboard: false,
        });
    }

    // --- Images ---

    async createImage(file) {
        let formData = new FormData();
        formData.append('file', file);

        return await this.post(IMAGES_ROUTE, formData, {contentType: false, updateDashboard: false});
    }

    // --- Client ---

    async getDashboard() {
        let options = {updateDashboard: false, setLoading: false, updateDashboardOnError: false};
        const customerId = localStorage.getItem('customerId');
        if (customerId) {
            options.query = {customerId};
        }

        return await this.get(DASHBOARD_ROUTE, options);
    }

    async getPage(key) {
        return await this.get(`${PAGES_ROUTE}/${key}`, {updateDashboard: false});
    }

    async listRecipes() {
        return await this.get(RECIPES_ROUTE, {updateDashboard: false});
    }

    async getRecipe(id) {
        return await this.get(`${RECIPES_ROUTE}/${id}`, {updateDashboard: false});
    }

    async updateProfile(data) {
        return await this.put(PROFILE_ROUTE, data);
    }

    async calculateCalories(activity) {
        return await this.post(`${CALORIES_ROUTE}/calculate`, {activity});
    }

    async createBeforeOrAfterImage(key, imageId) {
        return await this.post(`${BEFORE_AND_AFTER_IMAGES_ROUTE}/${key}`, {imageId});
    }

    async deleteBeforeOrAfterImage(key) {
        return await this.delete(`${BEFORE_AND_AFTER_IMAGES_ROUTE}/${key}`);
    }

    async createResult(data) {
        return await this.post(RESULTS_ROUTE, data);
    }

    async updateResult(id, data) {
        return await this.put(`${RESULTS_ROUTE}/${id}`, data);
    }

    async deleteResult(id) {
        return await this.delete(`${RESULTS_ROUTE}/${id}`);
    }
}

export default new ApiService('/api');