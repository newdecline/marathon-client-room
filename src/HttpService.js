import commonStore from "./mobx/commonStore";
import notifierStore from "./mobx/notifierStore";

const REQUEST_DEFAULT_OPTIONS = {
    contentType: 'application/json',
    method: 'get',
    body: {},
    query: {},
    snackbar: true,
    setLoading: true,
    updateDashboard: true,
    updateDashboardOnError: true,
};

class HttpService {

    apiBase;

    constructor(apiBase) {
        this.apiBase = apiBase;
    }

    static log(type, status, data) {
        console.log(`[HTTP] ${type} (${status})`, data);
    }

    async makeRequest(route, options = {}) {
        const {
            contentType,
            method,
            body,
            query,
            snackbar,
            setLoading,
            updateDashboard,
            updateDashboardOnError,
        } = {...REQUEST_DEFAULT_OPTIONS, ...options};
        
        let fetchConfig = {
            headers: {
                'Authorization': `Bearer ${commonStore.token}`,
            },
            method,
        };
        if (contentType) {
            fetchConfig.headers['Content-Type'] = contentType;
        }

        if (method !== 'get' && method !== 'head') {
            fetchConfig.body = contentType ==='application/json' ? JSON.stringify(body) : body;
        }

        const urlObject = new URL(`${this.apiBase}${route}`, window.location.origin);
        for (const key in query) {
            urlObject.searchParams.append(key, query[key]);
        }

        setLoading && (commonStore.loading = true);
        const response = await fetch(urlObject, fetchConfig);
        setLoading && (commonStore.loading = false);

        let responseBody;
        try {
            responseBody = await response.json();
        } catch (e) {
            responseBody = null;
        }

        HttpService.log('response', response.status, responseBody);

        if (200 <= response.status && response.status < 300) {
            updateDashboard && commonStore.loadDashboard();
        } else if (response.status === 401) {
            commonStore.logout();
        } else if (response.status === 422) {
            snackbar && notifierStore.enqueueError('Возникла ошибка при заполнении формы');
        } else if (response.status > 400) {
            const message = responseBody && responseBody.message
                ? responseBody.message
                : 'Ошибка';
            notifierStore.enqueueError(message);

            if (updateDashboardOnError) {
                commonStore.loadDashboard();
            }
        }

        return {
            status: response.status,
            body: responseBody,
        };
    }

    async get(route, options = {}) {
        return await this.makeRequest(route, options);
    }

    async post(route, body, options = {}) {
        return await this.makeRequest(route, {...options, method: 'post', body});
    }

    async put(route, body, options = {}) {
        return await this.makeRequest(route, {...options, method: 'put', body});
    }

    async delete(route, options = {}) {
        return await this.makeRequest(route, {...options, method: 'delete'});
    }
}

export default HttpService;